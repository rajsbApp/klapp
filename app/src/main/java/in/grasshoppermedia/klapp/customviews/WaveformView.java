package in.grasshoppermedia.klapp.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

/**
 * WaveformView.
 *
 * @author kang
 * @since 14/9/24.
 */
public class WaveformView extends View {
	private static final float MIN_AMPLITUDE = 0.0575f;
	private float mPrimaryWidth = 5.0f;
//	private float mSecondaryWidth = 2.0f;
	private float mAmplitude = MIN_AMPLITUDE;
	private int mWaveColor = Color.DKGRAY;
	private int mDensity = 1;
	private int mWaveCount = 1;

	private float wFrequency = 0.5f;
	private float wPhaseShift = -0.2f;
	private float wPhase = wPhaseShift;

	private Paint wPrimaryPaint;
//	private Paint mSecondaryPaint;

//	private Paint rPaint;
//	private float rWidth = 3.0f;
//	private float rFrequency = 0.8f;
//	private float rPhaseShift = -0.2f;
//	private float rPhase = rPhaseShift;

//	private Paint bPaint;
//	private float bWidth = 3.0f;
//	private float bFrequency = 1.0f;
//	private float bPhaseShift = -0.4f;
//	private float bPhase = bPhaseShift;

//	private Paint gPaint;
//	private float gWidth = 3.0f;
//	private float gFrequency = 0.6f;
//	private float gPhaseShift = -0.6f;
//	private float gPhase = gPhaseShift;

	private Path wPath;
//	private Path rPath;
//	private Path bPath;
//	private Path gPath;

	private float mLastX;
	private float mLastY;

	public WaveformView(Context context) {
		this(context, null);
	}

	public WaveformView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public WaveformView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initialize();
	}

	private void initialize() {
		wPrimaryPaint = new Paint();
		initpaint(wPrimaryPaint, mPrimaryWidth, Color.WHITE);

//		mSecondaryPaint = new Paint();
//		initpaint(mSecondaryPaint, mSecondaryWidth, Color.WHITE);
//
//		rPaint = new Paint();
//		initpaint(rPaint, rWidth, Color.RED);
//
//		bPaint = new Paint();
//		initpaint(bPaint, bWidth, Color.BLUE);
//
//		gPaint = new Paint();
//		initpaint(gPaint, gWidth, Color.GREEN);

		wPath = new Path();
//		rPath = new Path();
//		bPath = new Path();
//		gPath = new Path();
	}

	private void initpaint(Paint myPaint, float width, int color) {

		myPaint.setStrokeWidth(width);
		myPaint.setAntiAlias(true);
		myPaint.setStyle(Paint.Style.STROKE);
		myPaint.setColor(color);
	}

	public void updateAmplitude(float amplitude) {


		if (amplitude < 0.6)
			mAmplitude = Math.max(amplitude, MIN_AMPLITUDE);
		else
			mAmplitude = 0.6f;

	}

	@Override
	protected void onDraw(Canvas canvas) {
		int width = getWidth();
		int height = getHeight();

		for (int l = 0; l < mWaveCount; ++l) {
			float midH = height / 2.0f;
			float midW = width / 2.0f;

			float maxAmplitude = midH / 2f - 4.0f;
			float progress = 1.0f - l * 1.0f / mWaveCount;
			float normalAmplitude = (1.5f * progress - 0.5f) * mAmplitude;

			float multiplier = (float) Math.min(1.0, (progress / 3.0f * 2.0f)
					+ (1.0f / 3.0f));

//			if (l != 0) {
//				mSecondaryPaint.setAlpha((int) (multiplier * 255));
//			}

			wPath.reset();
//			rPath.reset();
//			bPath.reset();
//			gPath.reset();
			for (int x = 0; x < (width + mDensity); x += mDensity) {
				float scaling = 1f - (float) Math.pow(1 / midW * (x - midW), 2);
				float y = scaling
						* maxAmplitude
						* normalAmplitude
						* (float) Math.sin(180 * x * wFrequency
								/ (width * Math.PI) + wPhase) + midH;
//				float ry = scaling
//						* maxAmplitude
//						* normalAmplitude
//						* (float) Math.sin(180 * x * rFrequency
//								/ (width * Math.PI) + rPhase) + midH;
//				float by = scaling
//						* maxAmplitude
//						* normalAmplitude
//						* (float) Math.sin(180 * x * bFrequency
//								/ (width * Math.PI) + bPhase) + midH;
//				float gy = scaling
//						* maxAmplitude
//						* normalAmplitude
//						* (float) Math.sin(180 * x * gFrequency
//								/ (width * Math.PI) + gPhase) + midH;

				if (x == 0) {
					wPath.moveTo(x, y);
//					rPath.moveTo(x, ry);
//					bPath.moveTo(x, by);
//					gPath.moveTo(x, gy);
				} else {
					wPath.lineTo(x, y);
//					rPath.lineTo(x, ry);
//					bPath.lineTo(x, by);
//					gPath.lineTo(x, gy);
				}

				mLastX = x;
				mLastY = y;
			}

            canvas.drawPath(wPath, wPrimaryPaint);
//			if (l == 0) {
//				canvas.drawPath(rPath, rPaint);
//				canvas.drawPath(bPath, bPaint);
//				canvas.drawPath(gPath, gPaint);
//				canvas.drawPath(wPath, wPrimaryPaint);
//			} else {
//				canvas.drawPath(wPath, mSecondaryPaint);
//			}
		}

		wPhase += wPhaseShift;
//		rPhase += rPhaseShift;
//		bPhase += bPhaseShift;
//		gPhase += gPhaseShift;
		invalidate();
	}

}
