package in.grasshoppermedia.klapp.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by katrina on 17/08/16.
 */
public class CustomTextBold extends TextView {

    public CustomTextBold(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public CustomTextBold(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public CustomTextBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface face=Typeface.createFromAsset(context.getAssets(), "bold_font.ttf");
        this.setTypeface(face);
    }
}
