package in.grasshoppermedia.klapp.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by katrina on 16/08/16.
 */

public class CustomText extends TextView {

    public CustomText(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public CustomText(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public CustomText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface face=Typeface.createFromAsset(context.getAssets(), "font.ttf");
        this.setTypeface(face);
    }
}


