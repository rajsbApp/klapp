package in.grasshoppermedia.klapp.background_process;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import in.grasshoppermedia.klapp.Utils.Session;
import in.grasshoppermedia.klapp.Utils.Utils;

/**
 * Created by katrina on 19/07/16.
 */
public class OnBoot extends BroadcastReceiver{

    Session session;

    @Override
    public void onReceive(Context context, Intent intent) {

        session = Session.getSession(context);
        if (session.getIsPlaying().equalsIgnoreCase(Utils.TRUE)) {
            context.startService(new Intent(context, MusicService.class));
        }

    }
}
