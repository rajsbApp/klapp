package in.grasshoppermedia.klapp.background_process;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import in.grasshoppermedia.klapp.Utils.Session;

public class LocationUpdater extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private String latitude = "";
    private String longitude = "";
    Session session;
    Context context;

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;

    public LocationUpdater() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        context = LocationUpdater.this;
        session = Session.getSession(context);
        try{
            int off = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
            if(off==0){
//                Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                onGPS.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(onGPS);
                stopSelf();
            } else {
                buildGoogleApiClient();
                mGoogleApiClient.connect();
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }


        return super.onStartCommand(intent, flags, startId);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {

            latitude = ""+String.valueOf(mLastLocation.getLatitude());
            longitude = ""+String.valueOf(mLastLocation.getLongitude());

        }

        session.setLatitude(latitude);
        session.setLongitude(longitude);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
