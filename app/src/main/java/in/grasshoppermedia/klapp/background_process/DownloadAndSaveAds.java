package in.grasshoppermedia.klapp.background_process;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import in.grasshoppermedia.klapp.AsyncExtractPackage;
import in.grasshoppermedia.klapp.DownloadFileAsync;
import in.grasshoppermedia.klapp.Events.NewdAdEvent;
import in.grasshoppermedia.klapp.Utils.InteractionParser;
import in.grasshoppermedia.klapp.Utils.KlapPrefs;
import in.grasshoppermedia.klapp.Utils.Session;
import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.activities.HomeActivity;
import in.grasshoppermedia.klapp.db.DatabaseHelper;
import in.grasshoppermedia.klapp.model.AdModel;
import in.grasshoppermedia.klapp.model.FeaturesModel;
import in.grasshoppermedia.klapp.model.ScheduleModel;
import in.grasshoppermedia.klapp.services.CallPost;

public class DownloadAndSaveAds extends Service {

    ArrayList<AdModel> adModels;
    DatabaseHelper db;
int i=0;

    public DownloadAndSaveAds() {

    }



    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        db = DatabaseHelper.getDb(DownloadAndSaveAds.this);
        DownLoadAndSave();
        return START_REDELIVER_INTENT;
    }

    private void DownLoadAndSave() {

        synchronized (this) {
            Utils.makeRootFolder();
        }


        String url = Utils.BASE_URL+Utils.GET_INTERACTIONS;

        final List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id",  Session.getSession(DownloadAndSaveAds.this).getUser_id()));
        params.add(new BasicNameValuePair("gender", ""));
        params.add(new BasicNameValuePair("income_id", ""));
        params.add(new BasicNameValuePair("country_id", ""));
        params.add(new BasicNameValuePair("state_id", ""));
        params.add(new BasicNameValuePair("city_id", ""));//
        params.add(new BasicNameValuePair("application_id", Utils.APP_ID));

        Log.d("ankit", url);
        CallPost service = new CallPost(this, url, params, false, new CallPost.OnPostCall() {
        @Override
        public void OnPostCall(String response) {
            Log.d("ankit","interaction response ::::"+response);
            try{
                KlapPrefs.putString(getApplicationContext(),Utils.INTRACTIONS,response);
                 adModels = new ArrayList<>();
                InteractionParser parser = new InteractionParser(response);
                ArrayList<AdModel> list=parser.JSON_PARSER();
                if(list!=null){

                    adModels.addAll(list);


                    AsyncTask getAdnSave = new AsyncTask() {
                        @Override
                        protected Object doInBackground(Object[] params) {
                            for (AdModel model : adModels) {

                                downLoadFile(model);
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Object o) {
                            super.onPostExecute(o);
                            Intent RTReturn = new Intent(HomeActivity.RECEIVE_JSON);
                            RTReturn.putExtra("isDownloaded", true);
                            LocalBroadcastManager.getInstance(DownloadAndSaveAds.this).sendBroadcast(RTReturn);
                        }
                    };


                    if (Build.VERSION.SDK_INT >= 11) {
                        // --post GB use serial executor by default --
                        getAdnSave.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        // --GB uses ThreadPoolExecutor by default--
                        getAdnSave.execute();
                    }


                }




            } catch (Exception ex){
                ex.printStackTrace();
            }
        }
    });


        if (Build.VERSION.SDK_INT >= 11) {
            // --post GB use serial executor by default --
            service.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            // --GB uses ThreadPoolExecutor by default--
            service.execute();
        }
    }


    private synchronized void downLoadFile(final AdModel admodel) {
       //add check for update

        //if(!admodel.getVersion().equalsIgnoreCase(db.getIntractionVersion(admodel.getInteraction_id()).getVersion())){
            AsyncExtractPackage download = new AsyncExtractPackage( admodel.getFile(),
                    Utils.ROOT_DIR+"/"+admodel.getFile_name(),
                    Utils.ROOT_DIR+"/Klapp/"+admodel.getFile_name().substring(0,admodel.getFile_name().length()-4),new DownloadFileAsync.PostDownload() {
                @Override
                public void downloadDone(boolean unziped) {
                    Log.i("downloaded", "file download completed");

                    SaveToDb(admodel);
                    Log.i("unziped", "file unzip completed");


                }
            });




            if (Build.VERSION.SDK_INT >= 11) {
                // --post GB use serial executor by default --
                download.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                // --GB uses ThreadPoolExecutor by default--
                download.execute();
            }
        }


   // }



    private void SaveToDb(final AdModel adModel) {
        long id = 0;
        synchronized (this) {
            id = db.saveIntractionDetails(adModel);
            if (id > 0) {
                ArrayList<ScheduleModel> scheduleModels = adModel.getScheduleModels();
                for (int i = 0; i < scheduleModels.size(); i++) {
                    db.saveScheduleData("" + adModel.getInteraction_id(), scheduleModels.get(i));
                }


                ArrayList<FeaturesModel> featurelist = adModel.getFeaturesModels();
                for (int i = 0; i < featurelist.size(); i++) {
                    db.saveFeaturesData(adModel.getInteraction_id(), featurelist.get(i));
                }
            }
        }
        EventBus.getDefault().post(new NewdAdEvent(adModel.getInteraction_id()));



        stopSelf();
    }


public interface DataDownloaded{
    public void onDataDownLoad();
}
}