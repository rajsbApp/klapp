package in.grasshoppermedia.klapp.background_process;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import in.grasshoppermedia.klapp.Utils.Session;
import in.grasshoppermedia.klapp.Utils.Utils;

/**
 * Created by katrina on 19/07/16.
 */
public class RestartService extends BroadcastReceiver{

    Session session;
    boolean isAlive = false;

    @Override
    public void onReceive(Context context, Intent intent) {

        session = Session.getSession(context);

        Utils.setServiceWatchdogTimer(context, true, Utils.WATCHDOGTIMER);

        isAlive = Utils.isMyServiceRunning(MusicService.class, context);

        try {
            if (session.getIsPlaying().equalsIgnoreCase(Utils.TRUE)) {

                // now check if service running or not

                if (!isAlive)
                    context.startService(new Intent(context, MusicService.class));
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }


}
