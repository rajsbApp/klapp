package in.grasshoppermedia.klapp.background_process;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.os.IBinder;

import in.grasshoppermedia.klapp.Utils.Session;
import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.db.DatabaseHelper;
import in.grasshoppermedia.klapp.decoder.GetDecoder;


public class MusicService extends Service {

	GetDecoder decoder;
    private AudioManager am;
	Session session;
	DatabaseHelper db;

//    CountDownTimer timer;
//    boolean isHearing = false;

    @Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub

        am = (AudioManager) getSystemService(AUDIO_SERVICE);
		session = Session.getSession(MusicService.this);
		db = DatabaseHelper.getDb(MusicService.this);
        Utils.setServiceWatchdogTimer(this, true, Utils.WATCHDOGTIMER);

        int requestResult = am.requestAudioFocus(
                mAudioFocusListener, AudioManager.STREAM_MUSIC,
                //AudioManager.AUDIOFOCUS_GAIN);
                //AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK);

//        timer = new CountDownTimer(500,100) {
//            @Override
//            public void onTick(long millisUntilFinished) {
//
//            }
//
//            @Override
//            public void onFinish() {
//
//
//                if(isHearing)
//                    timer.start();
//
//            }
//        };
//
//        timer.start();

        if (requestResult == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            try {
                synchronized (this){
                    decoder = new GetDecoder(MusicService.this,db);
                    decoder.startDecoding();
//                    isHearing = true;
                    session.setIsPlaying(Utils.TRUE);
//                    createVisualizer();
                }
            } catch (Exception e) {
                // TODO: handle exception

                e.printStackTrace();
            }
        } else {
            stopSelf();
        }
		return START_STICKY;
	}



	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		synchronized (this) {
			if (decoder != null)
				decoder.stopDecoding();
//                isHearing = false;
                session.setIsPlaying(Utils.FALSE);
		}
	}


	private AudioManager.OnAudioFocusChangeListener mAudioFocusListener = new AudioManager.OnAudioFocusChangeListener() {
		public void onAudioFocusChange(int focusChange) {
			// AudioFocus is a new feature: focus updates are made verbose on
			// purpose
			switch (focusChange) {
				case AudioManager.AUDIOFOCUS_LOSS:

					break;

				case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:

//					calling when another app is requesting
                    synchronized (this) {
                        if (decoder != null)
                            decoder.stopDecoding();
                    }
					break;

				case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:

					break;

				case AudioManager.AUDIOFOCUS_GAIN:

//					calling when another app has lost request
					 try {

                        decoder = new GetDecoder(MusicService.this,db);
                        decoder.startDecoding();

                    } catch (Exception e) {
                        // TODO: handle exception

                        e.printStackTrace();
                    }
					break;

				default:

			}

		}
	};

}
