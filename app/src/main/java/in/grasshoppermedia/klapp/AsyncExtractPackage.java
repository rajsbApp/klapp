package in.grasshoppermedia.klapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by wildcoder on 15/02/16.
 */
public class AsyncExtractPackage extends AsyncTask<File, Integer, File> {
    private String _error = "";
    private ProgressDialog _progress_dialog;
    private Activity _activity;

    //private AppPreferences appPreferences;


    private static final String TAG ="DOWNLOADFILE";
    String fileUrl, StorezipFileLocation, DirectoryName;
    DownloadFileAsync.PostDownload onFileDownload;
    public AsyncExtractPackage(String url,String storageLoc,String unzipDir,DownloadFileAsync.PostDownload onFileDownload) {
        this.fileUrl=url;
        this.StorezipFileLocation=storageLoc;
        this.DirectoryName=unzipDir;
        this.onFileDownload=onFileDownload;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //_progress_dialog.show();
    }

    private void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
        fileOrDirectory.delete();
    }


    @Override
    protected File doInBackground(File... params) {
        File zipFile =new File(StorezipFileLocation);
        File targetDirectory = new File(DirectoryName);
        try {
            if (targetDirectory.exists())
                deleteRecursive(targetDirectory);
            Log.e(TAG, TAG + " Extacting request:" + zipFile.getName());
            Log.e(TAG, TAG + " targetDirectory:" + targetDirectory);

            unZipPackage(zipFile, targetDirectory);

        } catch (Exception e) {
            _error = e.getMessage();
            e.printStackTrace();
        }
        return null;
    }


    /**
     * @param zipFile
     * @param targetDirectory
     * @throws IOException
     * @throws FileNotFoundException
     */
    public void unZipPackage(File zipFile, File targetDirectory) throws IOException,
            FileNotFoundException {
        long total_len = zipFile.length();
        long total_installed_len = 0;

        ZipInputStream zis = null;
        InputStream theFile = new FileInputStream(zipFile);

        //  zis = new ZipInputStream(theFile);
        zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile)));
        try {
            ZipEntry ze = null;
            int count;
            Log.e(TAG, TAG + " Extracting: zip path" + zipFile.getPath());
            byte[] buffer = new byte[8192];
            while ((ze = zis.getNextEntry()) != null) {
                total_installed_len += ze.getCompressedSize();
                String file_name = ze.getName();
                Log.e(TAG, TAG + " Extracting:" + file_name);
                int percent = (int) (total_installed_len * 100 / total_len);
               // onProgressUpdate(percent);
                File file = new File(targetDirectory, file_name);
                if (file.exists())
                    file.delete();
                File dir = ze.isDirectory() ? file : file.getParentFile();
                if (!dir.isDirectory() && !dir.mkdirs())
                    throw new FileNotFoundException("Failed to ensure directory: " + dir.getAbsolutePath());
                if (ze.isDirectory())
                    continue;
                FileOutputStream fout = new FileOutputStream(file);
                try {
                    while ((count = zis.read(buffer)) != -1)
                        fout.write(buffer, 0, count);
                } finally {
                    fout.close();
                }

                // if time should be restored as well
                long time = ze.getTime();
                if (time > 0)
                    file.setLastModified(time);
            }
        } finally {
            zis.close();
        }
    }

//    @Override
//    protected void onProgressUpdate(Integer... values) {
//        super.onProgressUpdate(values);
//        int val = values[values.length - 1];
//        _progress_dialog.setProgress(val);
//    }

    @Override
    protected void onPostExecute(File file) {
        super.onPostExecute(file);
        onFileDownload.downloadDone(true);
    }

    private IOnExtractListener onExtractListener;

    public void setOnExtractListener(IOnExtractListener onExtractListener) {
        this.onExtractListener = onExtractListener;
    }

    public interface IOnExtractListener {
        public void onExtracted(File file);

        public void onError(String message);
    }


}
