package in.grasshoppermedia.klapp.services;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by katrina on 04/05/16.
 */
public class DownLoadFile {

    private int download_success = 0;
    private int download_error = 1;
    private int already_exists = 2;

    AsyncTask task;
    public DownLoadFile(String urlString, String name,String file_loc,OnFileDownload onFileDownload) {

        task = new AsyncTask<Object,Integer,Integer>()
        {
            String name;
            String urlString;
//            String type;
            String file_loc = "";
            OnFileDownload onFileDownload;
            @Override
            protected Integer doInBackground(Object... params) {
                urlString = (String)params[0];
                name = (String)params[1];
                file_loc = (String)params[2]; // 0 for ads and 1 for token
                onFileDownload = (OnFileDownload)params[3];

                String path = "";
//                if(type.equalsIgnoreCase("0")) {
//                    file_loc = Utils.makeFolder("Klappads/"+name) + "/";
//                } else if(type.equalsIgnoreCase("1")){
//                    file_loc = Utils.makeFolder("Klapptoken") + "/";
//                }

                Log.d("ankit","file_loc:::"+file_loc);
                path = file_loc + name;
                try {
                    File f = new File(path);
                    if (f.exists()) {
                        return already_exists;
                    } else {
                        int count;
                        URL url = new URL(urlString);
                        URLConnection connection = url.openConnection();
                        connection.connect();
                        int lengthOfFile = connection.getContentLength();
                        long total = 0;
                        InputStream input = new BufferedInputStream(url.openStream());
                        OutputStream output = new FileOutputStream(f);
                        byte data[] = new byte[1024];
                        while ((count = input.read(data)) != -1) {
                            total += count;
                            output.write(data, 0, count);
                            publishProgress((int) (total / 1024), lengthOfFile / 1024);
                        }

                        output.flush();
                        output.close();
                        input.close();
                        if(total==lengthOfFile) {
                            return download_success;
                        } else {
                            return download_error;
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    onFileDownload.onFileDownloadError(e.toString());
                    return download_error;
                }
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
                onFileDownload.onFileDownloadProgress(values[0]);
            }

            @Override
            protected void onPostExecute(Integer response) {
                super.onPostExecute(response);
                if(response==download_success) {
                    onFileDownload.onFileDownloadSuccess(name,file_loc);
                }else if(response==already_exists) {
                    onFileDownload.onFileAlreadyExists(name,file_loc);
                }else{
                    onFileDownload.onFileDownloadError("Error downloading the file");
                }

            }
        }.execute(urlString,name,file_loc,onFileDownload);
    }
}
