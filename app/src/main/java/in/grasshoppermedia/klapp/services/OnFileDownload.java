package in.grasshoppermedia.klapp.services;

/**
 * Created by katrina on 04/05/16.
 */
public interface OnFileDownload {

    public void onFileDownloadSuccess(String name,String path);
    public void onFileAlreadyExists(String name,String path);
    public void onFileDownloadProgress(int progress);
    public void onFileDownloadError(String errorMessage);
}
