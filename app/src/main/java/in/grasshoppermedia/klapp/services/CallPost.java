package in.grasshoppermedia.klapp.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.NameValuePair;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;


/**
 * Created by katrina on 01/02/17.
 */

public class CallPost extends AsyncTask<Void, String, String> {

    OnPostCall OnPostCall;
    Context context;
    String urlStr;

    private ProgressDialog progressDialog;
    boolean shouldShowProgress = false;
    List<NameValuePair> listParams;

    public interface OnPostCall {
        public void OnPostCall(String response);

    }

    public CallPost(Context context, String urlStr,List<NameValuePair> listParams,boolean shouldShowProgress,
                    OnPostCall OnPostCall) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.urlStr = urlStr;
        this.OnPostCall = OnPostCall;
        this.listParams = listParams;
        this.shouldShowProgress = shouldShowProgress;

    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        if(shouldShowProgress)
            progressDialog = ProgressDialog.show(context,"Loading ..","Please wait");

    }

    @Override
    protected String doInBackground(Void... params) {
        // TODO Auto-generated method stub
        try{
            return getData(listParams);
        } catch (Exception ex){
            ex.printStackTrace();
            return "";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if(shouldShowProgress)
            progressDialog.dismiss();
        OnPostCall.OnPostCall(result);
    }

    private String getData(List<NameValuePair> params) throws Exception{
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);

//        List<NameValuePair> params = new ArrayList<NameValuePair>();
//        params.add(new BasicNameValuePair("firstParam", paramValue1));
//        params.add(new BasicNameValuePair("secondParam", paramValue2));
//        params.add(new BasicNameValuePair("thirdParam", paramValue3));

        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));
        writer.write(getQuery(params));
        writer.flush();
        writer.close();
        os.close();

        conn.connect();

        InputStream is = conn.getInputStream();

        BufferedReader reader = new BufferedReader(new InputStreamReader(
                is, "UTF-8"));

        String data = null;
        String webPage = "";
        while ((data = reader.readLine()) != null) {
            webPage += data + "\n";
        }
        return webPage;

    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

}
