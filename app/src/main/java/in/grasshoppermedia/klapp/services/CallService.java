package in.grasshoppermedia.klapp.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class CallService extends AsyncTask<Void, String, String> {

	OnServicecall OnServicecall;
	Context context;
	String urlStr;

	String mehtod = "";
    String JsonData = "";
	private ProgressDialog progressDialog;
    boolean shouldShowProgress = false;

	public interface OnServicecall {
		public void onServicecall(String response);

	}

	public CallService(Context context, String urlStr, String mehtod,String JsonData,boolean shouldShowProgress,
			OnServicecall OnServicecall) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.urlStr = urlStr;
		this.OnServicecall = OnServicecall;
		this.mehtod = mehtod;
        this.JsonData = JsonData;
        this.shouldShowProgress = shouldShowProgress;

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
        if(shouldShowProgress)
        progressDialog = ProgressDialog.show(context,"Loading ..","Please wait");

	}

	@Override
	protected String doInBackground(Void... params) {
		// TODO Auto-generated method stub

//        if(mehtod.equalsIgnoreCase(Utils.GET)) {
//            return getData(mehtod);
//        } else if(mehtod.equalsIgnoreCase(Utils.POST) || mehtod.equalsIgnoreCase(Utils.PUT)){
//            return senddata(mehtod,JsonData);
//        } else
//            return "";

		return getData(mehtod);

	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
        if(shouldShowProgress)
        progressDialog.dismiss();
		OnServicecall.onServicecall(result);
	}

	private String getData(String mehtod) {

		try {
			URL url = new URL(urlStr);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(5000);
			conn.setConnectTimeout(15000);
			conn.setRequestMethod(mehtod);
			conn.setDoInput(true);
			conn.connect();
			InputStream is = conn.getInputStream();

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "UTF-8"));

			String data = null;
			String webPage = "";
			while ((data = reader.readLine()) != null) {
				webPage += data + "\n";
			}
			return webPage;
		} catch (Exception e) {
			e.printStackTrace();
			return e.toString();
		}
	}

//	private String senddata(String method,String jsonData)
//	{
//		try{
//            URL url = new URL(urlStr);
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setConnectTimeout(5000);
//            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//            conn.setDoOutput(true);
//            conn.setDoInput(true);
//            conn.setRequestMethod(method);
//
//            OutputStream os = conn.getOutputStream();
//            os.write(jsonData.getBytes("UTF-8"));
//            os.close();
//
//            InputStream is = conn.getInputStream();
//
//            BufferedReader reader = new BufferedReader(new InputStreamReader(
//                    is, "UTF-8"));
//
//            String data = null;
//            String webPage = "";
//            while ((data = reader.readLine()) != null) {
//                webPage += data + "\n";
//            }
//
//
//            is.close();
//            conn.disconnect();
//
//            return webPage;
//        }catch (Exception ex)
//        {
//            ex.printStackTrace();
//            return "";
//        }
//	}



}
