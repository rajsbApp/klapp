package in.grasshoppermedia.klapp;

import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by dhavalnagar on 03/02/15.
 */
public class DownloadFileAsync extends AsyncTask<Void, String, String> {

    private static final String TAG ="DOWNLOADFILE";
    String fileUrl, StorezipFileLocation, DirectoryName;
    PostDownload onFileDownload;
    public DownloadFileAsync(String url,String storageLoc,String unzipDir,PostDownload onFileDownload) {
        this.fileUrl=url;
        this.StorezipFileLocation=storageLoc;
        this.DirectoryName=unzipDir;
        this.onFileDownload=onFileDownload;
    }
        String result ="";

        @Override
        protected String doInBackground(Void... aurl)
        {
            int count;

            try
            {
                URL url = new URL(fileUrl);
                URLConnection conexion = url.openConnection();
                conexion.connect();
                int lenghtOfFile = conexion.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream());

                OutputStream output = new FileOutputStream(StorezipFileLocation);

                byte data[] = new byte[1024];
                long total = 0;

                while ((count = input.read(data)) != -1)
                {
                    total += count;

                    output.write(data, 0, count);
                }
                output.close();
                input.close();
                result = "true";

            } catch (Exception e) {

                result = "false";
            }
            return null;

        }


    @Override
    protected void onPostExecute(String unused)
    {
        if(result.equalsIgnoreCase("true"))
        {
            try
            {
                unzip();
            } catch (IOException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else
        {

        }
    }


    public void unzip() throws IOException
    {
        new UnZipTask().execute(StorezipFileLocation, DirectoryName);
    }


    private class UnZipTask extends AsyncTask<String, Void, Boolean>
    {
        @SuppressWarnings("rawtypes")
        @Override
        protected Boolean doInBackground(String... params)
        {
            String filePath = params[0];
            String destinationPath = params[1];

            File archive = new File(filePath);
            try
            {
//                ZipFile zipfile = new ZipFile(archive);
//                for (Enumeration e = zipfile.entries(); e.hasMoreElements();)
//                {
//                    ZipEntry entry = (ZipEntry) e.nextElement();
//                    unzipEntry(zipfile, entry, destinationPath);
//                }

                FileOperation.unpackZip(DirectoryName,StorezipFileLocation);
                //                ExtractFile d = new ExtractFile(StorezipFileLocation, DirectoryName);
//                d.unzip();

            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
        onFileDownload.downloadDone(result);

        }


        private void unzipEntry(ZipFile zipfile, ZipEntry entry,String outputDir) throws IOException
        {

            if (entry.isDirectory())
            {
                createDir(new File(outputDir, entry.getName()));
                return;
            }

            File outputFile = new File(outputDir, entry.getName());
            if (!outputFile.getParentFile().exists())
            {
                createDir(outputFile.getParentFile());
            }

            // Log.v("", "Extracting: " + entry);
            BufferedInputStream inputStream = new BufferedInputStream(zipfile.getInputStream(entry));
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));

            try
            {

            }
            finally
            {
                outputStream.flush();
                outputStream.close();
                inputStream.close();
            }
        }

        private void createDir(File dir)
        {
            if (dir.exists())
            {
                return;
            }
            if (!dir.mkdirs())
            {
                throw new RuntimeException("Can not create dir " + dir);
            }
        }
    }

    public  interface PostDownload{
       void downloadDone(boolean b);
    }
}

