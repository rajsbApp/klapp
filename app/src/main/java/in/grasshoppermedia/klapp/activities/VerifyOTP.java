package in.grasshoppermedia.klapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.Utils.Session;
import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.services.CallPost;

public class VerifyOTP extends AppCompatActivity {

    EditText code_et;
    Button confirm_bv;
    String codeString = "";
    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        init();
    }

    private void init(){

        synchronized (this){
            session = Session.getSession(VerifyOTP.this);
        }

        code_et = (EditText)findViewById(R.id.code_et);

        confirm_bv = (Button)findViewById(R.id.confirm_bv);
        confirm_bv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                codeString = code_et.getText().toString().trim();
                if(codeString.equalsIgnoreCase("")){
                    Toast.makeText(VerifyOTP.this,"Please enter otp verify code",Toast.LENGTH_LONG).show();
                } else {
                    otpVerifyTask(codeString);
                }
            }
        });


    }

    private void otpVerifyTask(String code){
        String url = Utils.BASE_URL + Utils.OTP_VERIFY_API;

        Log.d("ankit",url);

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("phone", getIntent().getExtras().getString("phone")));
        params.add(new BasicNameValuePair("otp", code));

        CallPost service = new CallPost(VerifyOTP.this, url,  params, true, new CallPost.OnPostCall() {
            @Override
            public void OnPostCall(String response) {
                try{
                    PARSEJSON(response);
                } catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        service.execute();
    }

    private void PARSEJSON(String response) throws Exception{
        Log.d("ankit",response);
        JSONObject jobj = new JSONObject(response);
        String error = jobj.getString("error");
        if(error.equalsIgnoreCase("false")){

            JSONObject customer = jobj.getJSONObject("customer");
            session.setUserData(customer.toString());

            String id = customer.getString("id");
            session.setUser_id(id);

            Intent intent = new Intent(VerifyOTP.this,HomeActivity.class);
            startActivity(intent);
            finish();
        } else {
            String messages = jobj.getString("messages");
            Toast.makeText(VerifyOTP.this,messages,Toast.LENGTH_LONG).show();
        }
    }
}
