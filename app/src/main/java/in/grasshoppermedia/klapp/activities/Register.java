package in.grasshoppermedia.klapp.activities;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import org.json.JSONObject;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.Utils.Session;
import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.services.CallService;

public class Register extends AppCompatActivity {

    EditText name,email,password,mobile,annoual_income,country,state,city,address_one,address_two;
    RadioButton radio_male,radio_female,radio_others;
    Button button_register;
    Context mContext;
    Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
    }

    private void init(){

        mContext = Register.this;
        session = Session.getSession(mContext);

        name = (EditText)findViewById(R.id.name);
        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);
        mobile = (EditText)findViewById(R.id.mobile);
        annoual_income = (EditText)findViewById(R.id.annoual_income);
        country = (EditText)findViewById(R.id.country);
        state = (EditText)findViewById(R.id.state);
        city = (EditText)findViewById(R.id.city);
        address_one = (EditText)findViewById(R.id.address_one);
        address_two = (EditText)findViewById(R.id.address_two);

        radio_male = (RadioButton)findViewById(R.id.radio_male);
        radio_female = (RadioButton)findViewById(R.id.radio_female);
        radio_others = (RadioButton)findViewById(R.id.radio_others);

        button_register = (Button)findViewById(R.id.button_register);
        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
             String nameStr = name.getText().toString().trim();
             String emailStr = email.getText().toString().trim();
             String passStr = password.getText().toString().trim();
             String mobileStr = mobile.getText().toString().trim();
             String annual_incomeStr = annoual_income.getText().toString().trim();
             String countryStr = country.getText().toString().trim();
             String stateStr = state.getText().toString().trim();
             String cityStr = city.getText().toString().trim();
             String address_oneStr = address_one.getText().toString().trim();
             String address_twoStr = address_two.getText().toString().trim()+"";

                String gender = "";

                if(radio_male.isChecked()){
                    gender = "male";
                } else if(radio_female.isChecked()){
                    gender = "female";
                } else {
                    gender = "other";
                }

                if(nameStr.equalsIgnoreCase(""))
                    Utils.showErrorDialog(mContext, "Please fill name");
                else if(emailStr.equalsIgnoreCase(""))
                    Utils.showErrorDialog(mContext, "Please fill email address");
                else if(!Utils.isValidEmail(emailStr))
                    Utils.showErrorDialog(mContext,"Please fill valid email address");
                else if(passStr.equalsIgnoreCase(""))
                    Utils.showErrorDialog(mContext,"Please fill password");
                else if(mobileStr.equalsIgnoreCase(""))
                    Utils.showErrorDialog(mContext,"Please fill mobile number");
                else if(annual_incomeStr.equalsIgnoreCase(""))
                    Utils.showErrorDialog(mContext,"Please fill annual income");
                else if(countryStr.equalsIgnoreCase(""))
                    Utils.showErrorDialog(mContext,"Please fill your country");
                else if(stateStr.equalsIgnoreCase(""))
                    Utils.showErrorDialog(mContext,"Please fill your state");
                else if(cityStr.equalsIgnoreCase(""))
                    Utils.showErrorDialog(mContext,"Please fill your city");
                else if(address_oneStr.equalsIgnoreCase(""))
                    Utils.showErrorDialog(mContext,"Please fill your address");
                else {
                    try{
                        JSONObject jobj = new JSONObject();
                        jobj.put("user_id",session.getUser_id());
                        jobj.put("name",nameStr);
                        jobj.put("mobile",mobileStr);
                        jobj.put("email",emailStr);
                        jobj.put("password",passStr);
                        jobj.put("gender",gender);
                        jobj.put("income_value",annual_incomeStr);
                        jobj.put("country",countryStr);
                        jobj.put("state",stateStr);
                        jobj.put("city",cityStr);
                        jobj.put("address1",address_oneStr);
                        jobj.put("address2",address_twoStr);
                        String jsonData = jobj.toString();
                        CallService service = new CallService(mContext, Utils.BASE_URL + Utils.REGISTER_API, Utils.POST, jsonData, true, new CallService.OnServicecall() {
                            @Override
                            public void onServicecall(String response) {
//                                if(Utils.isOTPRecieved(mContext))
//                                    Utils.LevelTwoTask(Register.this);
//                                else {
//                                    startActivity(new Intent(mContext,OTP.class));finish();
//                                }
                            }
                        });
                        if (Build.VERSION.SDK_INT >= 11) {
                            // --post GB use serial executor by default --
                            service.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            // --GB uses ThreadPoolExecutor by default--
                            service.execute();
                        }
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }

                }
            }
        });
    }
}
