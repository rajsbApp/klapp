package in.grasshoppermedia.klapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONObject;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.Utils.Session;
import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.services.CallService;

public class LoginPage extends AppCompatActivity {

    Button button_login,button_register;
    EditText email,password;
    Context mContext;
    Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);
        init();
    }

    private void init() {
        mContext = LoginPage.this;
        session = Session.getSession(mContext);
        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);

        button_register = (Button)findViewById(R.id.button_register);
        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginPage.this,Register.class));
            }
        });

        button_login = (Button)findViewById(R.id.button_login);
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String emailString = email.getText().toString().trim();
                String passString = password.getText().toString().trim();

                if(emailString.equalsIgnoreCase("")) {
                    Utils.showErrorDialog(mContext,"Please fill email address");
                } else if(!Utils.isValidEmail(emailString)) {
                    Utils.showErrorDialog(mContext,"Please fill valid email address");
                } else if(passString.equalsIgnoreCase("")) {
                    Utils.showErrorDialog(mContext,"Please fill password");
                } else {
                    try {
                        JSONObject jobj = new JSONObject();
                        jobj.put("email",emailString);
                        jobj.put("password",passString);
                        jobj.put("device_id",session.getDevice_id());
                        jobj.put("device_token",session.getDevice_token());
                        jobj.put("client_id",Utils.CLIENT_ID);
                        jobj.put("app_id", Utils.APP_ID);
                        String jsonData = jobj.toString();
                        CallService service = new CallService(mContext, Utils.BASE_URL + Utils.LOGIN_API, Utils.POST, jsonData,true, new CallService.OnServicecall() {
                            @Override
                            public void onServicecall(String response) {

//                                {
//                                    "error": false,
//                                        "status": 200,
//                                        "response": {
//                                    "user_id": "5719d08ba11627f10d0af0b5",
//                                            "login": true
//                                }
//                                } parse the json and set the user id
                                try {
                                    JSONObject jobj = new JSONObject(response);
                                    String errorMessage = jobj.getString("error");
                                    if(errorMessage.equalsIgnoreCase("false")) {
                                        String status_code = jobj.getString("status");
                                        if (status_code.equalsIgnoreCase("200")) {
                                            JSONObject resJson = jobj.getJSONObject("response");
                                            String login = resJson.getString("login");
                                            if(login.equalsIgnoreCase("true")) {
                                                session.setUser_id(resJson.getString("user_id"));
                                                if(Utils.isOTPRecieved(mContext))
                                                Utils.LevelTwoTask(LoginPage.this);
                                                else {
                                                    startActivity(new Intent(mContext,OTP.class));finish();
                                                }

                                            } else {
                                                Utils.showErrorDialog(mContext,"Login not successfull");
                                            }
                                        } else {
                                            Utils.showErrorDialog(mContext,"Login not successfull");
                                        }
                                    } else {
                                        Utils.showErrorDialog(mContext,"Login not successfull");
                                    }
                                }catch (Exception ex) {
                                    ex.printStackTrace();
                                }

                            }
                        });

                        if (Build.VERSION.SDK_INT >= 11) {
                            // --post GB use serial executor by default --
                            service.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            // --GB uses ThreadPoolExecutor by default--
                            service.execute();
                        }
                    }catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }



//                startActivity(new Intent(LoginPage.this,HomeActivity.class));
//                finish();
            }
        });
    }


}
