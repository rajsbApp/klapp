package in.grasshoppermedia.klapp.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.Utils.Session;
import in.grasshoppermedia.klapp.Utils.Utils;

public class PermissionPage extends AppCompatActivity {

    boolean isNotification=false,isMicrophone=false,isLocation=false,isCalender=false,isReminder=false;

    RelativeLayout permission_right,permission_direction;
    CheckBox mp_check,ls_check,calender_check;
    Context mContext;
    Session session;
    public static final int permsRequestCode = 200;
    Utils utils;

    // according to the new design

    ImageView noti_check_on,noti_check_off,mp_check_on,mp_check_off,ls_check_on,ls_check_off,calender_check_on,calender_check_off
            ,reminder_check_on,reminder_check_off;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission_page);

        init();
    }

    private void init(){

        mContext = PermissionPage.this;
        session = Session.getSession(mContext);
        utils = new Utils();

        noti_check_on = (ImageView)findViewById(R.id.noti_check_on);
        noti_check_on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noti_check_on.setVisibility(View.GONE);
                noti_check_off.setVisibility(View.VISIBLE);
                isNotification = false;
            }
        });

        noti_check_off = (ImageView)findViewById(R.id.noti_check_off);
        noti_check_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noti_check_off.setVisibility(View.GONE);
                noti_check_on.setVisibility(View.VISIBLE);
                isNotification = true;
            }
        });

        // microphone service
        mp_check_on = (ImageView)findViewById(R.id.mp_check_on);
        mp_check_on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp_check_on.setVisibility(View.GONE);
                mp_check_off.setVisibility(View.VISIBLE);
                isMicrophone = false;
            }
        });

        mp_check_off = (ImageView)findViewById(R.id.mp_check_off);
        mp_check_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp_check_off.setVisibility(View.GONE);
                mp_check_on.setVisibility(View.VISIBLE);
                isMicrophone = true;
            }
        });

        // location service
        ls_check_on = (ImageView)findViewById(R.id.ls_check_on);
        ls_check_on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ls_check_on.setVisibility(View.GONE);
                ls_check_off.setVisibility(View.VISIBLE);
                isLocation = false;
            }
        });

        ls_check_off = (ImageView)findViewById(R.id.ls_check_off);
        ls_check_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ls_check_off.setVisibility(View.GONE);
                ls_check_on.setVisibility(View.VISIBLE);
                isLocation = true;
            }
        });

        // calender
        calender_check_on = (ImageView)findViewById(R.id.calender_check_on);
        calender_check_on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calender_check_on.setVisibility(View.GONE);
                calender_check_off.setVisibility(View.VISIBLE);
                isCalender = false;
            }
        });

        calender_check_off = (ImageView)findViewById(R.id.calender_check_off);
        calender_check_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calender_check_off.setVisibility(View.GONE);
                calender_check_on.setVisibility(View.VISIBLE);
                isCalender = true;
            }
        });

        // reminder
        reminder_check_on = (ImageView)findViewById(R.id.reminder_check_on);
        reminder_check_on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reminder_check_on.setVisibility(View.GONE);
                reminder_check_off.setVisibility(View.VISIBLE);
                isReminder = false;
            }
        });

        reminder_check_off = (ImageView)findViewById(R.id.reminder_check_off);
        reminder_check_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reminder_check_off.setVisibility(View.GONE);
                reminder_check_on.setVisibility(View.VISIBLE);
                isReminder = true;
            }
        });

        permission_right = (RelativeLayout)findViewById(R.id.permission_right);
        permission_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionLogic();
            }
        });

        permission_direction = (RelativeLayout)findViewById(R.id.permission_direction);
        permission_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

//    private void init() {
//
//        mContext = PermissionPage.this;
//        session = Session.getSession(mContext);
//        utils = new Utils();
//        //notification checkbox
//        CheckBox noti_check = (CheckBox)findViewById(R.id.noti_check);
//        noti_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                isNotification = isChecked;
//            }
//        });
//
//        //microphone checkbox
//        mp_check = (CheckBox)findViewById(R.id.mp_check);
//        mp_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                isMicrophone = isChecked;
//            }
//        });
//
//        //location services checkbox
//        ls_check = (CheckBox)findViewById(R.id.ls_check);
//        ls_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                isLocation = isChecked;
//            }
//        });
//
//        //calender checkbox
//        calender_check = (CheckBox)findViewById(R.id.calender_check);
//        calender_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                isCalender = isChecked;
//            }
//        });
//
//        //reminder checkbox
//        CheckBox reminder_check = (CheckBox)findViewById(R.id.reminder_check);
//        reminder_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                isReminder = isChecked;
//            }
//        });
//
//        button_continue = (Button)findViewById(R.id.button_continue);
//        button_continue.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                permissionLogic();
//            }
//        });
//    }

    private void permissionLogic(){
        if(isMicrophone && isLocation)
        {
            if(isNotification) {
                session.setShowNotification(Utils.TRUE);
            } else {
                session.setShowNotification(Utils.FALSE);
            }
            if(isReminder) {
                session.setSetReminder(Utils.TRUE);
            } else {
                session.setSetReminder(Utils.FALSE);
            }

            if(isCalender) {
                if(shouldAskPermission()) {
                    String[] perms = { "android.permission.RECORD_AUDIO","android.permission.ACCESS_FINE_LOCATION"
                            ,"android.permission.ACCESS_COARSE_LOCATION","android.permission.WRITE_EXTERNAL_STORAGE"
                            ,"android.permission.READ_PHONE_STATE","android.permission.WRITE_CALENDAR" };
                    givePermissions(perms);
                } else {
                    utils.getDeviceTokenAndId(PermissionPage.this,mContext,session);
                }

            } else {
                if(shouldAskPermission()) {
                    String[] perms = { "android.permission.RECORD_AUDIO","android.permission.ACCESS_FINE_LOCATION"
                            ,"android.permission.ACCESS_COARSE_LOCATION","android.permission.WRITE_EXTERNAL_STORAGE"
                            ,"android.permission.READ_PHONE_STATE" };
                    givePermissions(perms);
                } else {
                    utils.getDeviceTokenAndId(PermissionPage.this,mContext, session);
                }

            }


        } else {
            Utils.showErrorDialog(mContext, "Please check microphone and locaion services");
        }
    }

    // should ask permissions
    private boolean shouldAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    // give permissions
    @SuppressLint("NewApi")
    private void givePermissions(String[] perms) {
        requestPermissions(perms, permsRequestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case permsRequestCode: {
                if ((ContextCompat.checkSelfPermission(getApplicationContext(),
                        android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)
                        &&(ContextCompat.checkSelfPermission(getApplicationContext(),
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                        &&(ContextCompat.checkSelfPermission(getApplicationContext(),
                        android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                        &&(ContextCompat.checkSelfPermission(getApplicationContext(),
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                        &&(ContextCompat.checkSelfPermission(getApplicationContext(),
                        android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)) {
                        // permission is ok do rest of the work
                    utils.getDeviceTokenAndId(PermissionPage.this,mContext,session);
                } else {
                    // permission is not ok show error
                    Utils.showErrorDialog(mContext,"Please check the required permissions");
                }
            }

        }

    }


}
