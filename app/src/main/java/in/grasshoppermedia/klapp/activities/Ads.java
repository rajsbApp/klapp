package in.grasshoppermedia.klapp.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.Utils.Session;
import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.db.DatabaseHelper;
import in.grasshoppermedia.klapp.model.AdModel;
import in.grasshoppermedia.klapp.model.EarnModel;
import in.grasshoppermedia.klapp.model.FeaturesModel;
import in.grasshoppermedia.klapp.model.ScheduleModel;
import in.grasshoppermedia.klapp.model.TransactionModel;
import in.grasshoppermedia.klapp.services.CallPost;

public class Ads extends Activity {
    WebView add_view;
    String code = "";
    ImageView cross_button;
    Context mContext;
    AdModel adModel;
    ScheduleModel scheduleModel;
    FeaturesModel featuresModel;
    DatabaseHelper db;

    // session object
    Session session;
    private EarnModel earnModel;

    @SuppressLint({"JavascriptInterface", "SetJavaScriptEnabled"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads);
        add_view = (WebView) findViewById(R.id.add_view);

        mContext = Ads.this;
        session = Session.getSession(mContext);
        db = DatabaseHelper.getDb(mContext);

        cross_button = (ImageView) findViewById(R.id.cross_button);
        cross_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });


        code = getIntent().getStringExtra("code");

        try {

            getDB();
            scheduleModel = db.getSchedule(code);
            adModel = db.getIneractionByID(scheduleModel.getInteraction_id());

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        add_view.getSettings().setJavaScriptEnabled(true);
        add_view.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        add_view.getSettings().setAllowFileAccess(true);
        add_view.setWebChromeClient(new WebChromeClient());
//
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            try {
                Method m1 = WebSettings.class.getMethod("setDomStorageEnabled", new Class[]{Boolean.TYPE});
                m1.invoke(add_view.getSettings(), Boolean.TRUE);

                Method m2 = WebSettings.class.getMethod("setDatabaseEnabled", new Class[]{Boolean.TYPE});
                m2.invoke(add_view.getSettings(), Boolean.TRUE);

                Method m3 = WebSettings.class.getMethod("setDatabasePath", new Class[]{String.class});
                m3.invoke(add_view.getSettings(), "/data/data/" + getPackageName() + "/databases/");

                Method m4 = WebSettings.class.getMethod("setAppCacheMaxSize", new Class[]{Long.TYPE});
                m4.invoke(add_view.getSettings(), 1024 * 1024 * 8);

                Method m5 = WebSettings.class.getMethod("setAppCachePath", new Class[]{String.class});
                m5.invoke(add_view.getSettings(), "/data/data/" + getPackageName() + "/cache/");

                Method m6 = WebSettings.class.getMethod("setAppCacheEnabled", new Class[]{Boolean.TYPE});
                m6.invoke(add_view.getSettings(), Boolean.TRUE);

            } catch (NoSuchMethodException e) {
                Log.e("ankit", "Reflection fail", e);
            } catch (InvocationTargetException e) {
                Log.e("ankit", "Reflection fail", e);
            } catch (IllegalAccessException e) {
                Log.e("ankit", "Reflection fail", e);
            }
        }

        // fit the width of screen

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            add_view.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
        } else {
            add_view.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        }

        add_view.setWebChromeClient(new WebChromeClient());

        String file = "file://" + Environment.getExternalStorageDirectory()
                + File.separator + "Klapp/" + adModel.getFile_name().substring(0, adModel.getFile_name().length() - 4)
                + "/index.htm";

        Log.d("TAG_URL", file);
        String filedata = null;
//        try {
        add_view.loadUrl("file:///" + Environment.getExternalStorageDirectory().getPath()
                + File.separator + "Klapp/" + adModel.getFile_name().substring(0, adModel.getFile_name().length() - 4)
                + "/index.htm");
//or
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


        add_view.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // TODO Auto-generated method stub
                try {
                    String[] str = url.split("=");
                    String trackId = str[1];
                    // parse feature json from getFeatureJsonFromTrackId method //

                    featuresModel = db.getAdFeatureFromAdID(trackId);
                    Log.d("ScheduleId:", scheduleModel.getSchedule_id());
                    Log.d("Feature_id", featuresModel.getFeaturetype_id());

                     performAction(featuresModel,trackId);

//                    JSONObject jobj = new JSONObject(scheduleModel.getEarn_max());
//                    String max_earn_value = jobj.getString("valueToEarn");
//                    senToDb(max_earn_value, trackId);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return super.shouldOverrideUrlLoading(view, url);
            }

        });


        Utils.isLoad = true;
    }

    private void getDB() {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "/data/data/" + getPackageName() + "/databases/klapp_database.sqlite";
                String backupDBPath = "backupname.sqlite";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        closeCode();
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeCode();
    }

    private void closeCode() {
        finish();
        Utils.isLoad = false;
    }

    private void senToDb( String trackId) throws Exception {

        if (Utils.haveNetworkConnection(getApplicationContext())) {

            String url = Utils.BASE_URL+Utils.SEND_EARNINGS;

            final List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("user_id",  Session.getSession(this).getUser_id()));
            params.add(new BasicNameValuePair("track_id", trackId));
            params.add(new BasicNameValuePair("schedule_id", scheduleModel.getSchedule_id()));
            params.add(new BasicNameValuePair("earning_id", earnModel.getEarning_id()));

            Log.d("ankit", url);
            CallPost service = new CallPost(this, url, params, false, new CallPost.OnPostCall() {
                @Override
                public void OnPostCall(String response) {
                    Log.d("ankit", "interaction response ::::" + response);


                }
            });
            if (Build.VERSION.SDK_INT >= 11) {
                // --post GB use serial executor by default --
                service.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                // --GB uses ThreadPoolExecutor by default--
                service.execute();
            }

        } else {
            TransactionModel model = new TransactionModel();
            model.setTrack_id(trackId);
            model.setMisc_earningId(earnModel.getEarning_id());
            model.setMisc2("" + System.currentTimeMillis());
            model.setUploaded("0");
            db.saveTransactionToDB(model);

        }

    }


    private void performAction(FeaturesModel model, final String trackId) throws Exception {

        Toast.makeText(Ads.this, "do something on click", Toast.LENGTH_LONG).show();

        earnModel = db.getEarningBtTrackId(trackId);

        int id = Integer.parseInt(featuresModel.getFeaturetype_id());
        // int id = Integer.parseInt(jobj.getString("id"));

        //JSONObject Minjobj = new JSONObject(scheduleModel.getEarnModels().g);
        // final String min_earn_value = Minjobj.getString("valueToEarn");


        switch (id) {
            case 1:
                // calling function
            {

                final String number = featuresModel.getNumber();
                makeDialog("Are you sure you want to call on " + number + " to earn more klapps (it can cost you serious money)....?", false, new SetOnDialogButton() {
                    @Override
                    public void OnYesButton() {

                        try {
                            senToDb(trackId);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }


                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        callIntent.setData(Uri.parse("tel:" + number));
                        if (shouldAskPermission()) {
                            if (ActivityCompat.checkSelfPermission(Ads.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                        }
                        startActivity(callIntent);

                    }

                    @Override
                    public void OnNoButton() {

                        Toast.makeText(Ads.this, "You missed earning opportunity", Toast.LENGTH_LONG).show();
                    }
                });

            }
            break;
            case 2:
                // Text Message
            {
                try {


                    final String number = featuresModel.getNumber();
                    makeDialog("Are you sure you want to sms on " + number + " to earn more klapps (it can cost you serious money)....?",false, new SetOnDialogButton() {
                        @Override
                        public void OnYesButton() {

                            try{
                                senToDb(trackId);
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }

                            SmsManager smsManager = SmsManager.getDefault();
                            smsManager.sendTextMessage(number, null, featuresModel.getSms_text(), null, null);
                            Toast.makeText(Ads.this, "Message Sent",
                                    Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void OnNoButton() {

                            Toast.makeText(Ads.this, "You missed earning opportunity", Toast.LENGTH_LONG).show();
                        }
                    });

                } catch (Exception e) {
                    Toast.makeText(Ads.this, "Message not Sent",
                            Toast.LENGTH_LONG).show();
                }
            }
            break;
            case 3:
                // Email
            {


                makeDialog("Are you sure you want to mail on " + featuresModel.getEmail() + " to earn more klapps....?",false, new SetOnDialogButton() {
                    @Override
                    public void OnYesButton() {

                        try{
                            senToDb(trackId);
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }

                        Intent send_intent = new Intent(Intent.ACTION_SENDTO);
                        send_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        send_intent.setData(Uri.parse("mailto:")); // only
                        // email
                        // apps
                        // should handle
                        // this
                        send_intent.putExtra(Intent.EXTRA_EMAIL, new String[]{featuresModel.getEmail()});
                        send_intent.putExtra(Intent.EXTRA_SUBJECT, featuresModel.getMail_text());
                        if (send_intent.resolveActivity(Ads.this
                                .getPackageManager()) != null) {
                            startActivity(send_intent);
                        }
                    }

                    @Override
                    public void OnNoButton() {

                        Toast.makeText(Ads.this, "You missed earning opportunity", Toast.LENGTH_LONG).show();
                    }
                });
            }
            break;
            case 4:
                // Share on social media
            {

                makeDialog("Are you sure you want to share to earn more klapps....?",false, new SetOnDialogButton() {
                    @Override
                    public void OnYesButton() {

                        try{
                            senToDb(trackId);
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }

                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        sendIntent.putExtra(Intent.EXTRA_TEXT,
                                featuresModel.getShare_text());
                        sendIntent.setType("text/plain");

                        try {
                            startActivity(sendIntent);
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(Ads.this, "No application found",
                                    Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void OnNoButton() {

                        Toast.makeText(Ads.this, "You missed earning opportunity", Toast.LENGTH_LONG).show();
                    }
                });
            }
            break;
            case 5:
                // Add reminder
            {
                makeDialog("Are you sure you want to set reminder to earn more klapps....?",false, new SetOnDialogButton() {
                    @Override
                    public void OnYesButton() {

                        try{
                            senToDb(trackId);
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }

                        //addCalender();
                        Toast.makeText(Ads.this, "Reminder has been added for this show", Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void OnNoButton() {

                        Toast.makeText(Ads.this, "You missed earning opportunity", Toast.LENGTH_LONG).show();
                    }
                });


            }
                break;
            case 6:
                // request open a url
            {
                makeDialog("Are you sure you want to open"+" to earn more klapps....?",false, new SetOnDialogButton() {
                    @Override
                    public void OnYesButton() {

                        try{
                            senToDb(trackId);
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }

                        Intent open_web = new Intent(Ads.this,
                                OpenWeb.class);
                        open_web.putExtra("url", featuresModel.getRequest_url());
                        open_web.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(open_web);
                    }

                    @Override
                    public void OnNoButton() {

                        Toast.makeText(Ads.this, "You missed earning opportunity", Toast.LENGTH_LONG).show();
                    }
                });

            }
            break;
            case 7:
                // poll
            {
                try{
                    senToDb(trackId);
                }catch (Exception ex){
                    ex.printStackTrace();
                }
               Toast.makeText(Ads.this,"thank you for you input",Toast.LENGTH_LONG).show();

            }
                break;
            case 8:
                // Calender
            {
                makeDialog("Are you sure you want to set calender event to earn more klapps....?",false, new SetOnDialogButton() {
                    @Override
                    public void OnYesButton() {

                        try{
                            senToDb(trackId);
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                       // addCalender();
                    }

                    @Override
                    public void OnNoButton() {

                        Toast.makeText(Ads.this, "You missed earning opportunity", Toast.LENGTH_LONG).show();
                    }
                });
            }
            break;
//            default:
//                break;
}
finish();        }


        // should ask permissions

    private boolean shouldAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    private void addCalender() {
        long calID = 3;
        long startMillis = 0;
        long endMillis = 0;
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(2016, 3, 10, 19, 30);
        startMillis = beginTime.getTimeInMillis();
        Calendar endTime = Calendar.getInstance();
        endTime.set(2016, 3, 10, 20, 00);
        endMillis = endTime.getTimeInMillis();

        ContentResolver cr = getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.TITLE, "Klapp");
        values.put(CalendarContract.Events.DESCRIPTION, "Watch this show");
        values.put(CalendarContract.Events.EVENT_TIMEZONE,
                CalendarContract.Calendars.CALENDAR_TIME_ZONE);
        values.put(CalendarContract.Events.CALENDAR_ID, calID);
        if (shouldAskPermission()) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
        }

        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
        // get the event ID that is the last element in the Uri
        long eventID = Long.parseLong(uri.getLastPathSegment());
    }

    private void makeDialog(String message, boolean shouldshow, final SetOnDialogButton SetOnDialogButton) {

        if (shouldshow) {
            // custom dialog
            final Dialog dialog = new Dialog(Ads.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialog);

            // set the custom dialog components - text, image and button
            TextView dialog_text = (TextView) dialog.findViewById(R.id.dialog_text);
            dialog_text.setText(message);

            Button dialog_yes = (Button) dialog.findViewById(R.id.dialog_yes);
            // if button is clicked, close the custom dialog
            dialog_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    SetOnDialogButton.OnYesButton();
                }
            });


            Button dialog_no = (Button) dialog.findViewById(R.id.dialog_no);
            // if button is clicked, close the custom dialog
            dialog_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    SetOnDialogButton.OnNoButton();
                }
            });

            dialog.show();
        } else {
            SetOnDialogButton.OnYesButton();
        }

    }

    public interface SetOnDialogButton {

        public void OnYesButton();

        public void OnNoButton();
    }

    private String readFile(String path) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(path));
        try {
            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } finally {
            br.close();
        }
        return sb.toString();
    }
}
