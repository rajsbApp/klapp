package in.grasshoppermedia.klapp.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.ArrayList;
import java.util.List;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.Utils.Session;
import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.adapter.AdListAdapter;
import in.grasshoppermedia.klapp.background_process.DownloadAndSaveAds;
import in.grasshoppermedia.klapp.background_process.LocationUpdater;
import in.grasshoppermedia.klapp.background_process.MusicService;
import in.grasshoppermedia.klapp.customviews.AudioMonitor;
import in.grasshoppermedia.klapp.customviews.WaveformView;
import in.grasshoppermedia.klapp.db.DatabaseHelper;
import in.grasshoppermedia.klapp.homeFragments.AboutUs;
import in.grasshoppermedia.klapp.homeFragments.MyKlapps;
import in.grasshoppermedia.klapp.homeFragments.MyProfile;
import in.grasshoppermedia.klapp.homeFragments.Services;
import in.grasshoppermedia.klapp.homeFragments.Settings;
import in.grasshoppermedia.klapp.model.AdModel;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{
//        , AudioMonitor.OnUpdateListener {

    private WaveformView mWaveformView;

    AudioMonitor monitor;

    private ListView ad_list;
    private AdListAdapter adAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Context mContext;
    DatabaseHelper db;

    ArrayList<AdModel> adModels;

    // slider items
    LinearLayout slider_home,slider_about,slider_services,slider_profile,slider_klapps,slider_settings;

    // if nothing downloaded show this view
    RelativeLayout not_yet;

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient googleApiClient;
    private ImageView top_icon;
    Session session;


    //Your activity will respond to this action String
    public static final String RECEIVE_JSON = "com.your.package.RECEIVE_JSON";

    private BroadcastReceiver bReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(RECEIVE_JSON)) {
                showData();
            }
        }
    };
    LocalBroadcastManager bManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mContext = HomeActivity.this;
        session = Session.getSession(mContext);

        db = DatabaseHelper.getDb(mContext);
//
//        Log.d("ankit",db.getToken().getToken_url());

        adModels = new ArrayList<>();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // wave form
        mWaveformView = (WaveformView) findViewById(R.id.waveform_view);

        // initiate ad list
        addList();

        // initiate the slider
        initSlider();

        // promt gps if not activated
        promtGPS();

        bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(RECEIVE_JSON);
        bManager.registerReceiver(bReceiver, intentFilter);


        // start music service to listen for <code>
       startService(new Intent(HomeActivity.this, MusicService.class));

    }

    private void closeDrawer(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void openDrawer(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.openDrawer(GravityCompat.START);
    }

    private void initSlider(){

        top_icon = (ImageView)findViewById(R.id.top_icon);
        top_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer();
            }
        });

        slider_home = (LinearLayout)findViewById(R.id.slider_home);
        slider_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                popFragments();
            }
        });

        slider_about = (LinearLayout)findViewById(R.id.slider_about);
        slider_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                popFragments();
                addfragment(new AboutUs(), "AboutUs");
            }
        });


        slider_services = (LinearLayout)findViewById(R.id.slider_services);
        slider_services.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                popFragments();
                addfragment(new Services(), "Services");
            }
        });

        slider_profile = (LinearLayout)findViewById(R.id.slider_profile);
        slider_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                popFragments();
                addfragment(new MyProfile(), "MyProfile");
            }
        });

        slider_klapps = (LinearLayout)findViewById(R.id.slider_klapps);
        slider_klapps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                popFragments();
                addfragment(new MyKlapps(), "MyKlapps");
            }
        });

        slider_settings = (LinearLayout)findViewById(R.id.slider_settings);
        slider_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
                popFragments();
                addfragment(new Settings(), "Settings");
            }
        });
    }

    // add fragments
    public void addfragment(Fragment fragment, String tag) {

        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment frag = fragmentManager.findFragmentByTag(tag);

            if (frag == null) {

                FragmentTransaction transaction = fragmentManager
                        .beginTransaction();
                transaction.add(R.id.content_frame, fragment, tag).addToBackStack(tag).commit();

                fragmentManager.executePendingTransactions();
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }


    // popout fragments
    public synchronized void popFragments(){
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }
    // replace fragments
//    public void replacefragment(Fragment fragment, String tag) {
//
//        try {
//            FragmentManager fragmentManager = getSupportFragmentManager();
//            Fragment frag = fragmentManager.findFragmentByTag(tag);
//
//            if (frag == null) {
//
//                FragmentTransaction transaction = fragmentManager
//                        .beginTransaction();
//                transaction.replace(R.id.content_frame, fragment, tag).addToBackStack(tag).commit();
//
////                fragmentManager.executePendingTransactions();
//            }
//        } catch (Exception e) {
//            // TODO: handle exception
//            e.printStackTrace();
//        }
//
//    }

    private void removeAll(){
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            }
        }
    }



    private void addList() {

        not_yet = (RelativeLayout)findViewById(R.id.not_yet);

        ad_list = (ListView) findViewById(R.id.ad_list);
//        ad_list.setHasFixedSize(true);
//        mLayoutManager = new LinearLayoutManager(this);
//        ad_list.setLayoutManager(mLayoutManager);

        adModels = new ArrayList<>();
        adAdapter = new AdListAdapter(HomeActivity.this,0,adModels);
        ad_list.setAdapter(adAdapter);

        ad_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Toast.makeText(HomeActivity.this,"Coming Soon!",Toast.LENGTH_LONG).show();
//                Intent intent = new Intent(HomeActivity.this,InteractionDetail.class);
//                intent.putExtra("model",adModels.get(position));
//                startActivity(intent);
            }
        });

        notifyList();
    }

    private void notifyList(){
//        {
//            ArrayList<ScheduleModel> _models = db.getAllAdsSchedules();
//            if(_models!=null && _models.size()>0) {
//                adsMainModel.addAll(_models);
//                not_yet.setVisibility(View.GONE);
//            } else {
//                not_yet.setVisibility(View.VISIBLE);
//            }
//
//        }
//
//        if(adsMainModel!=null && adsMainModel.size()>0){
//            for (int i =0;i<adsMainModel.size();i++){
//                ScheduleModel scheduleModel = adsMainModel.get(i);
//                AdModel adModel = db.getAdFromAdID(scheduleModel.getAdvertisement_id());
//                ShowAdModel showAdModel = new ShowAdModel();
//                showAdModel.setAddHeading(adModel.getAdvertisement_heading());
//                showAdModel.setAddIcon(adModel.getAdvertisement_icon());
//                showAdModel.setChannelHeading(scheduleModel.getChannel_name());
//                showAdModel.setChannelIcon(scheduleModel.getChannel_image_url());
//                showAdModel.setEarnMax(scheduleModel.getEarn_max());
//                showAdModel.setEarnMin(scheduleModel.getEarn_min());
//                showAdModel.setStartDate(scheduleModel.getStart_date());
//                showAdModel.setStartTime(scheduleModel.getStart_time());
//                showModels.add(showAdModel);
//            }
//        }


    if(Utils.haveNetworkConnection(getApplicationContext())) {

        startService(new Intent(HomeActivity.this, DownloadAndSaveAds.class));
    }else{
        showData();
    }

//        Collections.sort(showModels, new CustomComparator());

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void showData(){
        try{
            //InteractionParser parser = new InteractionParser(KlapPrefs.getString(getApplicationContext(),Utils.INTRACTIONS));
//            adModels.addAll(parser.JSON_PARSER());
            adModels.addAll(db.getAllAddModels());
            adAdapter.notifyDataSetChanged();
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
//        ((AdListAdapter) adAdapter).setOnItemClickListener(new AdListAdapter
//                .MyClickListener() {
//            @Override
//            public void onItemClick(int position, View v) {
////                Toast.makeText(mContext, " Clicked on Item " + position, Toast.LENGTH_LONG).show();
//            }
//        });
//        if (!EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().register(this);
//        }
    }

    @Override
    public void onPause() {
//        EventBus.getDefault().unregister(this);
        super.onPause();
    }



//    @Override
//    public void update(final float amplitude) {
//
//        Log.d("ankit",""+amplitude);
//

//    }

    private void promtGPS(){
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(HomeActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        HomeActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });             }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==1000&&resultCode==-1){
            startService(new Intent(HomeActivity.this, LocationUpdater.class));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bManager.unregisterReceiver(bReceiver);
    }
}
