package in.grasshoppermedia.klapp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.Utils.KlapPrefs;
import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.db.DatabaseHelper;

public class VideoPlay extends Activity {

	VideoView info_video;
	RelativeLayout close_relative;

	Context mContext;
	String from;
	DatabaseHelper db;
	Utils utils;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		setContentView(R.layout.video_play);

		from = getIntent().getStringExtra(Utils.FROM);

		init();
	}

	private void init() {
		mContext = VideoPlay.this;

		db = DatabaseHelper.getDb(mContext);

		utils = new Utils();

		info_video = (VideoView) findViewById(R.id.info_video);

		String path = "android.resource://" + getPackageName() + "/"
				+ R.raw.video;
		info_video.setVideoURI(Uri.parse(path));
		info_video.start();
		info_video.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				// TODO Auto-generated method stub

				closeCode(false);

			}
		});

		close_relative = (RelativeLayout) findViewById(R.id.close_relative);
		close_relative.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				closeCode(true);
			}
		});

	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		closeCode(true);
	}

	private void closeCode(boolean userInput) {
//		if(userInput)
//			Session.getSession(mContext).setShow_video("TRUE");

		if(KlapPrefs.getString(this,Utils.IS_PERMISSION_ALLOWED).equalsIgnoreCase("Y")){
			if(Utils.haveNetworkConnection(mContext)){
				startActivity(new Intent(mContext,PermissionPage.class));
				finish();
			} else {
				Utils.showErrorDialog(mContext,"Intenet is not present");
			}
		} else if(from.equalsIgnoreCase("b")) {
			// check token and download token
			utils.checkNetandDownloadToken(mContext,VideoPlay.this,db);
		} else if(from.equalsIgnoreCase("e")){
            finish();
		}

	}
	// should ask permissions
	private boolean shouldAskPermission() {
		return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
	}
}
