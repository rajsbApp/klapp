package in.grasshoppermedia.klapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.services.CallPost;

public class OTP extends AppCompatActivity {

    EditText number_et;
    Button signin_bv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        init();
    }

    private void init(){
        number_et = (EditText)findViewById(R.id.number_et);
        signin_bv = (Button)findViewById(R.id.signin_bv);
        signin_bv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone_number = number_et.getText().toString().trim();
                if(phone_number.equalsIgnoreCase("")){
                    Toast.makeText(OTP.this,"Please enter valid phone number",Toast.LENGTH_LONG).show();
                } else {
                    doOTP("+91"+phone_number);
                }

            }
        });
    }

    private void doOTP(final String phone_number){
        String url = Utils.BASE_URL + Utils.OTP_API;

        Log.d("ankit",url);

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("application_id", Utils.APP_ID));
        params.add(new BasicNameValuePair("phone", phone_number));
        params.add(new BasicNameValuePair("client_id", Utils.CLIENT_ID));
        params.add(new BasicNameValuePair("device_id", "0"));
        params.add(new BasicNameValuePair("device_token", "0"));
        params.add(new BasicNameValuePair("lat", "0"));
        params.add(new BasicNameValuePair("long", "0"));
        params.add(new BasicNameValuePair("device_type", "android"));

        CallPost service = new CallPost(OTP.this, url,  params, true, new CallPost.OnPostCall() {
            @Override
            public void OnPostCall(String response) {
                try{
                    PARSEJSON(response,phone_number);
                } catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        service.execute();

    }

    private void PARSEJSON(String response,String phone_number) throws Exception{
        JSONObject jobj = new JSONObject(response);
        String error = jobj.getString("error");
        if(error.equalsIgnoreCase("false")){
            Intent intent = new Intent(OTP.this,VerifyOTP.class);
            intent.putExtra("phone",phone_number);
            startActivity(intent);
            finish();
        } else {
            String messages = jobj.getString("messages");
            Toast.makeText(OTP.this,messages,Toast.LENGTH_LONG).show();
        }
    }

}
