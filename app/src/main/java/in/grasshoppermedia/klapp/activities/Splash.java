package in.grasshoppermedia.klapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;

import com.splunk.mint.Mint;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.Utils.KlapPrefs;
import in.grasshoppermedia.klapp.Utils.Session;
import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.db.DatabaseHelper;

public class Splash extends AppCompatActivity{

    Context mContext;

    Session session;
    DatabaseHelper db;
    Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Mint.initAndStartSession(this.getApplication(), "ca83042c");
        mContext = Splash.this;

        utils = new Utils();

        db = DatabaseHelper.getDb(mContext);

        session = Session.getSession(mContext);
        new CountDownTimer(3000,1000){

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                SplashPageLogic();
            }
        }.start();
    }




    private void SplashPageLogic() {
        if(session.getUser_id().equalsIgnoreCase("")){
            // OTP

            KlapPrefs.putString(this,Utils.IS_PERMISSION_ALLOWED,"Y");
            startActivity(new Intent(Splash.this,VideoPlay.class));
            finish();
        } else {
            // Main Activity
            startActivity(new Intent(Splash.this,HomeActivity.class));
            finish();
        }

    }
}
