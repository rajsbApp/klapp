package in.grasshoppermedia.klapp.activities;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import in.grasshoppermedia.klapp.R;

public class OpenWeb extends Activity {

    WebView open_web;
    String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.open_web);
		init();
    }
    private void init() {

        url = getIntent().getExtras().getString("url");
        open_web = (WebView) findViewById(R.id.open_web);
        open_web.loadUrl(url);

    }
}
