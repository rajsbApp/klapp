package in.grasshoppermedia.klapp.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.customviews.CustomText;
import in.grasshoppermedia.klapp.customviews.CustomTextBold;
import in.grasshoppermedia.klapp.homeFragments.EarnFragment;
import in.grasshoppermedia.klapp.model.AdModel;
import in.grasshoppermedia.klapp.model.ChanelModel;
import in.grasshoppermedia.klapp.model.EarnModel;

public class InteractionDetail extends AppCompatActivity {

    AdModel model;
    ViewPager earn_pager;
    CustomTextBold ad_heading;
    CustomText ad_description;
    ImageView ad_background;
    RelativeLayout backLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interaction_detail);

        model = getIntent().getParcelableExtra("model");
        if(model!=null){
            try {
                init();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            finish();
        }

    }

    private void init() throws Exception{
        earn_pager = (ViewPager)findViewById(R.id.earn_pager);
        backLayout= (RelativeLayout) findViewById(R.id.interaction_direction);

        ArrayList<EarnModel> earModels = model.getScheduleModels().get(0).getEarnModels();
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(),earModels,model.getScheduleModels().get(0).getChannelModels().get(0));
        earn_pager.setAdapter(viewPagerAdapter);

        ad_heading = (CustomTextBold)findViewById(R.id.ad_heading);
        ad_heading.setText(model.getHeading());

        ad_background = (ImageView)findViewById(R.id.ad_background);
        try {

            String image_url = model.getImage();

            image_url = image_url.replace(" ", "%20");

            Picasso.with(InteractionDetail.this).load(image_url).fit()
                    .into(ad_background);

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        ad_description = (CustomText)findViewById(R.id.ad_description);
        ad_description.setText(model.getDescription());


        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        ArrayList<EarnModel> mainModel = new ArrayList<>();
        ChanelModel channelModel = new ChanelModel();

        public ViewPagerAdapter(FragmentManager manager, ArrayList<EarnModel> mainModel, ChanelModel channelModel) {
            super(manager);
            this.mainModel = mainModel;
            this.channelModel = channelModel;
        }

        @Override
        public Fragment getItem(int position) {
            return EarnFragment.newInstance(mainModel.get(position),channelModel);
        }

        @Override
        public int getCount() {
            //return mFragmentList.size();
            return mainModel.size();
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }
    }

}
