package in.grasshoppermedia.klapp.db;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.model.AdModel;
import in.grasshoppermedia.klapp.model.ChanelModel;
import in.grasshoppermedia.klapp.model.EarnModel;
import in.grasshoppermedia.klapp.model.FeaturesModel;
import in.grasshoppermedia.klapp.model.ScheduleModel;
import in.grasshoppermedia.klapp.model.TokenModel;
import in.grasshoppermedia.klapp.model.TransactionModel;

import static in.grasshoppermedia.klapp.Utils.Utils.CHANNEL_EXTRA;
import static in.grasshoppermedia.klapp.Utils.Utils.CHANNEL_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.CHANNEL_IMAGE;
import static in.grasshoppermedia.klapp.Utils.Utils.CHANNEL_IMAGE_NAME;
import static in.grasshoppermedia.klapp.Utils.Utils.CHANNEL_MISC;
import static in.grasshoppermedia.klapp.Utils.Utils.CHANNEL_NAME;
import static in.grasshoppermedia.klapp.Utils.Utils.CHANNEL_TABLE;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_AGGREGATE_TO_EARN;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_DISPLAY_TEXT;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_EARNING_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_EARN_BONUS;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_MIN_AGGREGATE_VALUE;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_RESET_AGGREGATE_COUNT;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_SHOW_IN_APP;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_TABLE;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_TOTAL_AGGREGATE_COUNT;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_TOTAL_BONUS;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_TRACK_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_TYPE;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_VALUE_TO_EARN;
import static in.grasshoppermedia.klapp.Utils.Utils.FEATURETYPE_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.FEATURE_DATE;
import static in.grasshoppermedia.klapp.Utils.Utils.FEATURE_EXTRA;
import static in.grasshoppermedia.klapp.Utils.Utils.FEATURE_HEADING;
import static in.grasshoppermedia.klapp.Utils.Utils.FEATURE_MAIL;
import static in.grasshoppermedia.klapp.Utils.Utils.FEATURE_MAIL_TEXT;
import static in.grasshoppermedia.klapp.Utils.Utils.FEATURE_MISC;
import static in.grasshoppermedia.klapp.Utils.Utils.FEATURE_NAME;
import static in.grasshoppermedia.klapp.Utils.Utils.FEATURE_NUMBER;
import static in.grasshoppermedia.klapp.Utils.Utils.FEATURE_OPTIONS;
import static in.grasshoppermedia.klapp.Utils.Utils.FEATURE_REQUEST_URL;
import static in.grasshoppermedia.klapp.Utils.Utils.FEATURE_SHARE_TEXT;
import static in.grasshoppermedia.klapp.Utils.Utils.FEATURE_SMS_TEXT;
import static in.grasshoppermedia.klapp.Utils.Utils.FEATURE_TIME;
import static in.grasshoppermedia.klapp.Utils.Utils.FEATURE_TRACK_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.INTERACTION_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.INTERACTION_TABLE;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_APPLICATION_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_CREATED_AT;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_DESCRIPTION;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_DESCRIPTION_HEADING;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_DESCRIPTION_IMAGE;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_DESCRIPTION_IMAGE_NAME;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_FEATURE_TABLE;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_FILE;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_FILE_NAME;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_HEADING;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_IMAGE;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_IMAGE_NAME;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_INTERACTION_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_IS_PRELOAD;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_TYPE;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_URL;
import static in.grasshoppermedia.klapp.Utils.Utils.INT_VERSION;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_CITY_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_CODE;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_COUNTRY_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_END_DATE;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_END_TIME;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_GENDER;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_INCOME_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_IS_REMINDER;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_IS_REPEAT;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_REMINDER_TEXT;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_REMINDER_TIME;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_REPEAT_TIME;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_REPEAT_TYPE;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_START_DATE;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_START_TIME;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_STATE_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_STATUS;
import static in.grasshoppermedia.klapp.Utils.Utils.SCHEDULE_TABLE;
import static in.grasshoppermedia.klapp.Utils.Utils.TRANSACTION_MISC;
import static in.grasshoppermedia.klapp.Utils.Utils.TRANSACTION_MISC2;
import static in.grasshoppermedia.klapp.Utils.Utils.TRANSACTION_SCHEDULE_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.TRANSACTION_TABLE;
import static in.grasshoppermedia.klapp.Utils.Utils.TRANSACTION_TRACK_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.TRANSACTION_UPLOADED;

/**
 * Created by katrina on 04/05/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static Context mContext;

    public static String DATABASE_NAME = "klapp_database.sqlite";

    public static int DATABASE_VERSION = 1;

    public static String DATABASE_PATH = "";

    private SQLiteDatabase myDataBase;

    @SuppressLint("SdCardPath")
    protected DatabaseHelper(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
        mContext = context;
        DATABASE_PATH = "/data/data/" + mContext.getPackageName()
                + "/databases/";


        try {
            createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        openDataBase();

    }

    private static DatabaseHelper instance = null;

    public static DatabaseHelper getDb(Context context) {
        if (instance == null) {
            instance = new DatabaseHelper(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

    }

    /**
     * This method will create database in application package /databases
     * directory when first time application launched
     **/
    public synchronized void createDataBase() throws IOException {
        boolean mDataBaseExist = checkDataBase();
        if (!mDataBaseExist) {
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException mIOException) {
                mIOException.printStackTrace();
                throw new Error("Error copying database");
            } finally {
                this.close();
            }
        }
    }

    /**
     * This method checks whether database is exists or not
     **/
    private synchronized boolean checkDataBase() {
        try {
            final String mPath = DATABASE_PATH + DATABASE_NAME;
            final File file = new File(mPath);
            if (file.exists())
                return true;
            else
                return false;
        } catch (SQLiteException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * This method will copy database from /assets directory to application
     * package /databases directory
     **/
    private synchronized void copyDataBase() throws IOException {
        try {

            InputStream mInputStream = mContext.getAssets().open(DATABASE_NAME);
            String outFileName = DATABASE_PATH + DATABASE_NAME;
            OutputStream mOutputStream = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = mInputStream.read(buffer)) > 0) {
                mOutputStream.write(buffer, 0, length);
            }
            mOutputStream.flush();
            mOutputStream.close();
            mInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method open database for operations
     **/
    public synchronized boolean openDataBase() throws SQLException {
        String mPath = DATABASE_PATH + DATABASE_NAME;
        myDataBase = SQLiteDatabase.openDatabase(mPath, null,
                SQLiteDatabase.OPEN_READWRITE);
        return myDataBase.isOpen();
    }

    /**
     * This method close database connection and released occupied memory
     **/
    @Override
    public void close() {
        if (myDataBase != null)
            myDataBase.close();

        SQLiteDatabase.releaseMemory();

        super.close();
    }

//    public synchronized boolean saveAdsData(AdModel adModels) {
//
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        try {
//
//            ContentValues insertValues = new ContentValues();
//            insertValues.put(Utils.ADVERTISEMENT_ID,
//                    adModels.getAdvertisement_id());
//            insertValues.put(Utils.ADVERTISEMENT_NAME,
//                    adModels.getAdvertisement_name());
//            insertValues.put(Utils.ADVERTISEMENT_HEADING,
//                    adModels.getAdvertisement_heading());
//            insertValues.put(Utils.ADVERTISEMENT_URL,
//                    adModels.getFile_name());
//            insertValues.put(Utils.ADVERTISEMENT_ICON,
//                    adModels.getAdvertisement_icon());
//            insertValues.put(Utils.ADVERTISEMENT_PRE_LOAD,
//                    ""+adModels.isPre_load());
//            insertValues.put(Utils.ADVERTISEMENT_STATUS,
//                    adModels.getStatus());
//            db.insert(Utils.ADVERTISEMENT_TABLE_NAME, null, insertValues);
//            db.close();
//            return true;
//        } catch (Exception ex){
//            ex.printStackTrace();
//            db.close();
//            return false;
//        }
//
//    }

//    public synchronized ArrayList<AdModel> getAllAds() {
//        ArrayList<AdModel> mainmodel = new ArrayList<AdModel>();
//
//        String selectQuery = "SELECT * FROM " + Utils.ADVERTISEMENT_TABLE_NAME;
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        if (cursor.moveToFirst()) {
//            do {
//
//                // get the data into array,or class variable
//                String advertisement_id = ""+cursor.getString(cursor
//                        .getColumnIndex(Utils.ADVERTISEMENT_ID));
//                String advertisement_name = ""+cursor.getString(cursor
//                        .getColumnIndex(Utils.ADVERTISEMENT_NAME));
//                String advertisement_heading = ""+cursor.getString(cursor
//                        .getColumnIndex(Utils.ADVERTISEMENT_HEADING));
//                String advertisement_url = ""+cursor.getString(cursor
//                        .getColumnIndex(Utils.ADVERTISEMENT_URL));
//                String advertisement_icon = ""+cursor.getString(cursor
//                        .getColumnIndex(Utils.ADVERTISEMENT_PRE_LOAD));
//                String pre_load = ""+cursor.getString(cursor
//                        .getColumnIndex(Utils.ADVERTISEMENT_ICON));
//                String status = ""+cursor.getString(cursor
//                        .getColumnIndex(Utils.ADVERTISEMENT_STATUS));
//
//                AdModel model = new AdModel();
//                model.setAdvertisement_id(advertisement_id);
//                model.setAdvertisement_name(advertisement_name);
//                model.setAdvertisement_heading(advertisement_heading);
//                model.setAdvertisement_url(advertisement_url);
//                model.setAdvertisement_icon(advertisement_icon);
//                if(pre_load.equalsIgnoreCase("true"))
//                    model.setPre_load(true);
//                else
//                    model.setPre_load(true);
//                model.setStatus(status);
//
//                mainmodel.add(model);
//
//            } while (cursor.moveToNext());
//        }
//
//        cursor.close();
//        db.close();
//
//        return mainmodel;
//    }
//
//    public synchronized AdModel getAdFromAdID(String addId){
//        AdModel model = new AdModel();
//        String selectQuery = "SELECT * FROM " + Utils.ADVERTISEMENT_TABLE_NAME+" WHERE "+Utils.ADVERTISEMENT_ID+" = "+"'"+addId+"'";
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        if (cursor.moveToFirst()) {
//
//
//                    // get the data into array,or class variable
//                    String advertisement_id = ""+cursor.getString(cursor
//                            .getColumnIndex(Utils.ADVERTISEMENT_ID));
//                    String advertisement_name = ""+cursor.getString(cursor
//                            .getColumnIndex(Utils.ADVERTISEMENT_NAME));
//                    String advertisement_heading = ""+cursor.getString(cursor
//                            .getColumnIndex(Utils.ADVERTISEMENT_HEADING));
//                    String advertisement_url = ""+cursor.getString(cursor
//                            .getColumnIndex(Utils.ADVERTISEMENT_URL));
//                    String advertisement_icon = ""+cursor.getString(cursor
//                            .getColumnIndex(Utils.ADVERTISEMENT_ICON));
//                    String pre_load = ""+cursor.getString(cursor
//                            .getColumnIndex(Utils.ADVERTISEMENT_PRE_LOAD));
//                    String status = ""+cursor.getString(cursor
//                            .getColumnIndex(Utils.ADVERTISEMENT_STATUS));
//
//                    model.setAdvertisement_id(advertisement_id);
//                    model.setAdvertisement_name(advertisement_name);
//                    model.setAdvertisement_heading(advertisement_heading);
//                    model.setAdvertisement_url(advertisement_url);
//                    model.setAdvertisement_icon(advertisement_icon);
//                    if(pre_load.equalsIgnoreCase("true"))
//                        model.setPre_load(true);
//                    else
//                        model.setPre_load(true);
//                    model.setStatus(status);
//
//        }
//
//        cursor.close();
//        db.close();
//
//        return model;
//    }
//
//    public synchronized boolean isADDPresent(String add_id) {
//        try {
//            String selectQuery = "SELECT  * FROM " + Utils.ADVERTISEMENT_TABLE_NAME + " WHERE "
//                    + Utils.ADVERTISEMENT_ID + "=" + "'"+add_id+"'";
//            SQLiteDatabase db = this.getWritableDatabase();
//            Cursor cursor = db.rawQuery(selectQuery, null);
//            cursor.moveToLast();
//            int count = cursor.getCount();
//
//            cursor.close();
//            db.close();
//
//            if (count == 0)
//                return false;
//            else {
//                return true;
//
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return false;
//        }
//
//
//	}
//
//    // save schedule data
//    public synchronized boolean saveAdsSchedule(ScheduleModel scheduleModels) {
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        try {
//            ContentValues insertValues = new ContentValues();
//            insertValues.put(Utils.AD_SCHEDULE_ADD_ID, scheduleModels.getAdvertisement_id());//1
//            insertValues.put(Utils.AD_SCHEDULE_CHANNEL_ID, scheduleModels.getChannelId());//2
//            insertValues.put(Utils.AD_SCHEDULE_CHANNEL_NAME, scheduleModels.getChannel_name());//3
//            insertValues.put(Utils.AD_SCHEDULE_CHANNEL_IMAGE_URL, scheduleModels.getChannel_image_url());//4
//            insertValues.put(Utils.AD_SCHEDULE_START_DATE, scheduleModels.getStart_date());//5
//            insertValues.put(Utils.AD_SCHEDULE_START_TIME, scheduleModels.getStart_time());//6
//            insertValues.put(Utils.AD_SCHEDULE_END_DATE, scheduleModels.getEnd_date());//7
//            insertValues.put(Utils.AD_SCHEDULE_END_TIME, scheduleModels.getEnd_time());//8
//            insertValues.put(Utils.AD_SCHEDULE_EARN_MAX, scheduleModels.getEarn_max());//9
//            insertValues.put(Utils.AD_SCHEDULE_EARN_MIN, scheduleModels.getEarn_min());//10
//            insertValues.put(Utils.AD_SCHEDULE_CODE, scheduleModels.getCode());//11
//            insertValues.put(Utils.AD_SCHEDULE_REPEAT, scheduleModels.getRepeat());//12
//            insertValues.put(Utils.AD_SCHEDULE_REPEAT_PERIOD, scheduleModels.getRepeatOptions_period());//13
//            insertValues.put(Utils.AD_SCHEDULE_REPEAT_AMOUNT, scheduleModels.getRepeatOptions_amount());//14
//            insertValues.put(Utils.AD_SCHEDULE_REMINDER, scheduleModels.getReminder());//15
//            insertValues.put(Utils.AD_SCHEDULE_REMINDER_TEXT, scheduleModels.getReminderOpitons_text());//16
//            insertValues.put(Utils.AD_SCHEDULE_REMINDER_TIME, scheduleModels.getReminderOpitons_time());//17
//            insertValues.put(Utils.AD_SCHEDULE_LOCATION_STATE, scheduleModels.getLocation_state());//18
//            insertValues.put(Utils.AD_SCHEDULE_LOCATION_CITY, scheduleModels.getLocation_city());//19
//            insertValues.put(Utils.AD_SCHEDULE_INCOME, scheduleModels.getIncome());//20
//            insertValues.put(Utils.AD_SCHEDULE_GENDER, scheduleModels.getGender());//21
//            db.insert(Utils.AD_SCHEDULE_TABLE, null, insertValues);
//            db.close();
//            return true;
//        } catch (Exception ex){
//            ex.printStackTrace();
//            db.close();
//            return false;
//        }
//
//    }
//
//    public synchronized ArrayList<ScheduleModel> getAllAdsSchedules() {
//        ArrayList<ScheduleModel> mainmodel = new ArrayList<ScheduleModel>();
//
//        String selectQuery = "SELECT * FROM " + Utils.AD_SCHEDULE_TABLE;
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        if (cursor.moveToFirst()) {
//            do {
//
//                ScheduleModel model = new ScheduleModel();
//                // get the data into array,or class variable
//                model.setAdvertisement_id(""+cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_ADD_ID)));
//                model.setChannelId("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_CHANNEL_ID)));
//                model.setChannel_name("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_CHANNEL_NAME)));
//                model.setChannel_image_url("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_CHANNEL_IMAGE_URL)));
//                model.setStart_date("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_START_DATE)));
//                model.setStart_time("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_START_TIME)));
//                model.setEnd_date("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_END_DATE)));
//                model.setEnd_time("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_END_TIME)));
//                model.setEarn_max("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_EARN_MAX)));
//                model.setEarn_min("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_EARN_MIN)));
//                model.setCode("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_CODE)));
//                model.setRepeat("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_REPEAT)));
//                model.setRepeatOptions_period("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_REPEAT_PERIOD)));
//                model.setRepeatOptions_amount("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_REPEAT_AMOUNT)));
//                model.setReminder("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_REMINDER)));
//                model.setReminderOpitons_text("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_REMINDER_TEXT)));
//                model.setReminderOpitons_time("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_REMINDER_TIME)));
//                model.setLocation_state("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_LOCATION_STATE)));
//                model.setLocation_city("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_LOCATION_CITY)));
//                model.setIncome("" + cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_INCOME)));
//                model.setGender(""+cursor.getString(cursor
//                        .getColumnIndex(Utils.AD_SCHEDULE_GENDER)));
//
//
//                mainmodel.add(model);
//
//            } while (cursor.moveToNext());
//        }
//
//        cursor.close();
//        db.close();
//
//        return mainmodel;
//    }
//
//    public synchronized ScheduleModel getFromCode(String code){
//        String selectQuery = "SELECT * FROM " + Utils.AD_SCHEDULE_TABLE+" WHERE "+Utils.AD_SCHEDULE_CODE +"="+"'"+code+"'";
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        ScheduleModel model = new ScheduleModel();
//        if (cursor.moveToFirst()) {
//
//            // get the data into array,or class variable
//            model.setAdvertisement_id(""+cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_ADD_ID)));
//            model.setChannelId("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_CHANNEL_ID)));
//            model.setChannel_name("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_CHANNEL_NAME)));
//            model.setChannel_image_url("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_CHANNEL_IMAGE_URL)));
//            model.setStart_date("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_START_DATE)));
//            model.setStart_time("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_START_TIME)));
//            model.setEnd_date("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_END_DATE)));
//            model.setEnd_time("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_END_TIME)));
//            model.setEarn_max("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_EARN_MAX)));
//            model.setEarn_min("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_EARN_MIN)));
//            model.setCode("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_CODE)));
//            model.setRepeat("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_REPEAT)));
//            model.setRepeatOptions_period("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_REPEAT_PERIOD)));
//            model.setRepeatOptions_amount("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_REPEAT_AMOUNT)));
//            model.setReminder("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_REMINDER)));
//            model.setReminderOpitons_text("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_REMINDER_TEXT)));
//            model.setReminderOpitons_time("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_REMINDER_TIME)));
//            model.setLocation_state("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_LOCATION_STATE)));
//            model.setLocation_city("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_LOCATION_CITY)));
//            model.setIncome("" + cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_INCOME)));
//            model.setGender(""+cursor.getString(cursor
//                    .getColumnIndex(Utils.AD_SCHEDULE_GENDER)));
//        }
//
//        cursor.close();
//        db.close();
//        return model;
//    }

    public synchronized boolean isADDSchedulePresent(String code) {
        try {
            String selectQuery = "SELECT * FROM " + Utils.SCHEDULE_TABLE + " WHERE " + Utils.AD_SCHEDULE_CODE + "=" + "'" + code + "'";
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
           // cursor.moveToLast();
            int count = cursor.getCount();

            cursor.close();
            db.close();

            if (count == 0)
                return false;
            else {
                return true;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }


    }

    // save add features
    public synchronized boolean saveFeaturesData(String intractionId, FeaturesModel model) {

        SQLiteDatabase db = this.getWritableDatabase();

        try {


            ContentValues insertValues = new ContentValues();

            insertValues.put(Utils.FEATURE_TRACK_ID, model.getTrack_id());
            insertValues.put(FEATURETYPE_ID, model.getFeaturetype_id());
            insertValues.put(Utils.INTERACTION_ID, model.getInteraction_id());
            insertValues.put(FEATURE_NAME, model.getFeature_name());
            insertValues.put(FEATURE_NUMBER, model.getNumber());
            insertValues.put(FEATURE_SMS_TEXT, model.getSms_text());
            insertValues.put(FEATURE_MAIL_TEXT, model.getMail_text());
            insertValues.put(FEATURE_MAIL, model.getEmail());
            insertValues.put(FEATURE_SHARE_TEXT, model.getShare_text());
            insertValues.put(FEATURE_TIME, model.getTime());
            insertValues.put(FEATURE_REQUEST_URL, model.getRequest_url());
            insertValues.put(FEATURE_OPTIONS, model.getOptions());
            insertValues.put(FEATURE_HEADING, model.getHeading());
            insertValues.put(FEATURE_DATE, model.getDate());
            insertValues.put(FEATURE_MISC, model.getMisc());
            insertValues.put(FEATURE_EXTRA, model.getExtra());

            if (isFeatureExists(intractionId, model.getTrack_id()) > 0) {
                db.update(Utils.INT_FEATURE_TABLE, insertValues, INT_INTERACTION_ID + " = ? AND " + FEATURE_TRACK_ID + "=?",
                        new String[]{String.valueOf(intractionId), String.valueOf(model.getTrack_id())});
            } else {
                db.insert(Utils.INT_FEATURE_TABLE, null, insertValues);
            }

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            db.close();
            return false;
        }

    }

    private int isFeatureExists(String intractionId, String track_id) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(INT_FEATURE_TABLE, new String[]{Utils.INT_INTERACTION_ID}, Utils.INT_INTERACTION_ID + "=? AND " + Utils.FEATURE_TRACK_ID + "=?",
                new String[]{String.valueOf(intractionId), String.valueOf(track_id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        return cursor.getCount();

    }

    // get features
    public synchronized FeaturesModel getAdFeatureFromAdID(String trackId) {

        String selectQuery = "SELECT * FROM " + Utils.INT_FEATURE_TABLE + " WHERE " + Utils.EARNING_TRACK_ID + " = " + "'" + trackId + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        FeaturesModel featuresModel = new FeaturesModel();


        if (cursor.moveToFirst()) {
            do {

                featuresModel.setTrack_id(cursor.getString(cursor.getColumnIndex(Utils.FEATURE_TRACK_ID)));
                featuresModel.setFeaturetype_id(cursor.getString(cursor.getColumnIndex(FEATURETYPE_ID)));
                featuresModel.setInteraction_id(cursor.getString(cursor.getColumnIndex(Utils.INTERACTION_ID)));
                featuresModel.setFeature_name(cursor.getString(cursor.getColumnIndex(FEATURE_NAME)));
                featuresModel.setNumber(cursor.getString(cursor.getColumnIndex(FEATURE_NUMBER)));
                featuresModel.setSms_text(cursor.getString(cursor.getColumnIndex(FEATURE_SMS_TEXT)));
                featuresModel.setMail_text(cursor.getString(cursor.getColumnIndex(FEATURE_MAIL_TEXT)));
                featuresModel.setEmail(cursor.getString(cursor.getColumnIndex(FEATURE_MAIL)));
                featuresModel.setShare_text(cursor.getString(cursor.getColumnIndex(FEATURE_SHARE_TEXT)));
                featuresModel.setTime(cursor.getString(cursor.getColumnIndex(FEATURE_TIME)));
                featuresModel.setRequest_url(cursor.getString(cursor.getColumnIndex(FEATURE_REQUEST_URL)));
                featuresModel.setOptions(cursor.getString(cursor.getColumnIndex(FEATURE_OPTIONS)));
                featuresModel.setHeading(cursor.getString(cursor.getColumnIndex(FEATURE_HEADING)));
                featuresModel.setDate(cursor.getString(cursor.getColumnIndex(FEATURE_DATE)));
                featuresModel.setMisc(cursor.getString(cursor.getColumnIndex(FEATURE_MISC)));
                featuresModel.setExtra(cursor.getString(cursor.getColumnIndex(FEATURE_EXTRA)));


            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return featuresModel;
    }

    // save the token
    public synchronized boolean saveToken(TokenModel model) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues insertValues = new ContentValues();
            insertValues.put(Utils.TOKEN_TOKEN_ID,
                    model.getToken_id());//1
            insertValues.put(Utils.TOKEN_NAME,
                    model.getToken_name());//2
            insertValues.put(Utils.TOKEN_URL,
                    model.getToken_url());//3
            insertValues.put(Utils.TOKEN_EXPIRY_DATE,
                    model.getToken_expire_date());//4
            insertValues.put(Utils.TOKEN_START_DATE,
                    model.getToken_start_date());//5
            insertValues.put(Utils.TOKEN_VERSION,
                    model.getVersion());//6
            insertValues.put(Utils.TOKEN_STATUS,
                    model.getStatus());//7
            db.insert(Utils.TOKEN_TABLE_NAME, null, insertValues);
            db.close();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            db.close();
            return false;
        }

    }

    // get the token
    public synchronized TokenModel getToken() {

        String selectQuery = "SELECT * FROM " + Utils.TOKEN_TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        TokenModel model = new TokenModel();
        if (cursor.moveToFirst()) {

            String TOKEN_TOKEN_ID = "" + cursor.getString(cursor
                    .getColumnIndex(Utils.TOKEN_TOKEN_ID));
            String TOKEN_NAME = "" + cursor.getString(cursor
                    .getColumnIndex(Utils.TOKEN_NAME));
            String TOKEN_URL = "" + cursor.getString(cursor
                    .getColumnIndex(Utils.TOKEN_URL));
            String TOKEN_EXPIRY_DATE = "" + cursor.getString(cursor
                    .getColumnIndex(Utils.TOKEN_EXPIRY_DATE));
            String TOKEN_START_DATE = "" + cursor.getString(cursor
                    .getColumnIndex(Utils.TOKEN_START_DATE));
            String TOKEN_VERSION = "" + cursor.getString(cursor
                    .getColumnIndex(Utils.TOKEN_VERSION));
            String TOKEN_STATUS = "" + cursor.getString(cursor
                    .getColumnIndex(Utils.TOKEN_STATUS));

            model.setToken_id(TOKEN_TOKEN_ID);
            model.setToken_name(TOKEN_NAME);
            model.setToken_url(TOKEN_URL);
            model.setToken_expire_date(TOKEN_EXPIRY_DATE);
            model.setToken_start_date(TOKEN_START_DATE);
            model.setVersion(TOKEN_VERSION);
            model.setStatus(TOKEN_STATUS);
        }

        cursor.close();
        db.close();

        return model;
    }

    // delete token
    public boolean deleteToken(String token_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        boolean result = db.delete(Utils.TOKEN_TABLE_NAME, Utils.TOKEN_TOKEN_ID + "=" + token_id, null) > 0;
        db.close();
        return result;
    }



    // delete transaction
    public boolean deleteTransaction(String transaction_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        boolean result = db.delete(Utils.TRANSACTION_TABLE, "id" + "=" + transaction_id, null) > 0;
        db.close();
        return result;
    }
//    db.execSQL("delete from "+ TABLE_NAME);

    public void cleanAllTables() throws Exception {
        SQLiteDatabase db = this.getWritableDatabase();
        // table one TOKEN_TABLE_NAME
        db.execSQL("delete from " + Utils.TOKEN_TABLE_NAME);
        // table two ADVERTISEMENT_TABLE_NAME
        db.execSQL("delete from " + Utils.ADVERTISEMENT_TABLE_NAME);
        // table three AD_FEATURE_TABLE
        db.execSQL("delete from " + Utils.INT_FEATURE_TABLE);
        // table four AD_SCHEDULE_TABLE
        db.execSQL("delete from " + Utils.AD_SCHEDULE_TABLE);

        try {
            // table six BALANCE_TABLE
            db.execSQL("delete from " + Utils.BALANCE_TABLE);
            // table five TRANSACTION_TABLE
            db.execSQL("delete from " + Utils.TRANSACTION_TABLE);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        db.close();
    }

    public long saveIntractionDetails(AdModel adModel) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        long id = 0;
        values.put(INT_INTERACTION_ID, adModel.getInteraction_id());
        values.put(INT_APPLICATION_ID, adModel.getApplication_id());
        values.put(INT_IS_PRELOAD, adModel.getIs_preload());
        values.put(INT_HEADING, adModel.getHeading());
        values.put(INT_FILE, adModel.getFile());
        values.put(INT_FILE_NAME, adModel.getFile_name());
        values.put(INT_IMAGE, adModel.getImage());
        values.put(INT_IMAGE_NAME, adModel.getImage_name());
        values.put(INT_TYPE, adModel.getType());
        values.put(INT_DESCRIPTION_HEADING, adModel.getDescription_heading());
        values.put(INT_DESCRIPTION, adModel.getDescription());
        values.put(INT_DESCRIPTION_IMAGE, adModel.getDescription_image());
        values.put(INT_DESCRIPTION_IMAGE_NAME, adModel.getDescription_image_name());
        values.put(INT_URL, adModel.getUrl());
        values.put(INT_VERSION, adModel.getVersion());
        values.put(INT_CREATED_AT, adModel.getCreated_at());
        if (isExist(INT_INTERACTION_ID, Utils.INTERACTION_TABLE, adModel.getInteraction_id()) > 0) {
            id = db.update(Utils.INTERACTION_TABLE, values, INT_INTERACTION_ID + " = ?",
                    new String[]{String.valueOf(adModel.getInteraction_id())});
        } else {
            id = db.insert(Utils.INTERACTION_TABLE, null, values);
        }

//            saveFeaturesData(""+id, adModel.getFeaturesModels());
//            saveScheduleData(""+id, adModel.getScheduleModels());

        return id;


    }

    public void saveScheduleData(String interaction_id, ScheduleModel model) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Utils.SCHEDULE_ID, model.getSchedule_id());
        values.put(Utils.INT_INTERACTION_ID, interaction_id);
        values.put(SCHEDULE_GENDER, model.getGender());
        values.put(SCHEDULE_STATUS, model.getStatus());
        values.put(SCHEDULE_START_DATE, model.getStart_date());
        values.put(SCHEDULE_START_TIME, model.getStart_time());
        values.put(SCHEDULE_END_DATE, model.getEnd_date());
        values.put(SCHEDULE_END_TIME, model.getEnd_time());
        values.put(SCHEDULE_CODE, model.getCode());
        values.put(SCHEDULE_IS_REPEAT, model.getIs_repeat());
        values.put(SCHEDULE_REPEAT_TIME, model.getRepeat_time());
        values.put(SCHEDULE_REPEAT_TYPE, model.getRepeat_type());
        values.put(SCHEDULE_IS_REMINDER, model.getIs_reminder());
        values.put(SCHEDULE_REMINDER_TEXT, model.getReminder_text());
        values.put(SCHEDULE_REMINDER_TIME, model.getReminder_time());
        values.put(SCHEDULE_COUNTRY_ID, model.getCountry_id());
        values.put(SCHEDULE_STATE_ID, model.getState_id());
        values.put(SCHEDULE_CITY_ID, model.getCity_id());
        values.put(SCHEDULE_INCOME_ID, model.getIncome_id());
        values.put(SCHEDULE_INCOME_ID, model.getIncome_id());

        if (isScheduleExists(interaction_id, model.getSchedule_id()) > 0) {
            db.update(SCHEDULE_TABLE, values, INT_INTERACTION_ID + " = ? AND " + SCHEDULE_ID + "=?",
                    new String[]{String.valueOf(interaction_id), String.valueOf(model.getSchedule_id())});
        } else {
            db.insert(SCHEDULE_TABLE, null, values);
        }

        saveEarnings(model.getEarnModels());
        saveChannels(model.getChannelModels());


    }


    private void saveChannels(ArrayList<ChanelModel> channelModels) {
        SQLiteDatabase db = this.getWritableDatabase();

        for (ChanelModel chanelModel : channelModels) {
            ContentValues values = new ContentValues();

            values.put(CHANNEL_ID, chanelModel.getChannel_id());
            values.put(CHANNEL_NAME, chanelModel.getName());
            values.put(CHANNEL_IMAGE, chanelModel.getImage());
            values.put(CHANNEL_IMAGE_NAME, chanelModel.getImage_name());

            db.insert(CHANNEL_TABLE, null, values);

        }

    }

    private void saveEarnings(ArrayList<EarnModel> earnModels) {
        SQLiteDatabase db = this.getWritableDatabase();

        for (EarnModel earnModel : earnModels) {
            ContentValues values = new ContentValues();
            values.put(EARNING_EARNING_ID, earnModel.getEarning_id());
            values.put(EARNING_TYPE, earnModel.getType());
            values.put(EARNING_TRACK_ID, earnModel.getTrack_id());
            values.put(EARNING_DISPLAY_TEXT, earnModel.getDisplay_text());
            values.put(EARNING_VALUE_TO_EARN, earnModel.getValue_to_earn());
            values.put(EARNING_SHOW_IN_APP, earnModel.getShow_in_app());
            values.put(EARNING_AGGREGATE_TO_EARN, earnModel.getAggregate_to_earn());
            values.put(EARNING_MIN_AGGREGATE_VALUE, earnModel.getMin_aggregate_value());
            values.put(EARNING_RESET_AGGREGATE_COUNT, earnModel.getReset_aggregate_count());
            values.put(EARNING_EARN_BONUS, earnModel.getEarn_bonus());
            values.put(EARNING_TOTAL_AGGREGATE_COUNT, earnModel.getTotal_aggregate_count());
            values.put(EARNING_TOTAL_BONUS, earnModel.getTotal_bonus());


            db.insert("earnings", null, values);


        }


    }

    private int isScheduleExists(String interaction_id, String schedule_id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(SCHEDULE_TABLE, new String[]{Utils.INT_INTERACTION_ID}, Utils.INT_INTERACTION_ID + "=? AND " + Utils.SCHEDULE_ID + "=?",
                new String[]{String.valueOf(interaction_id), String.valueOf(schedule_id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        return cursor.getCount();
    }


    private int isExist(String interaction_id, String tableName, String value) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(tableName, new String[]{interaction_id}, interaction_id + "=?",
                new String[]{String.valueOf(value)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        return cursor.getCount();
    }

    public ScheduleModel getSchedule(String code) {
        String selectQuery = "SELECT schedule_id,interaction_id FROM " + Utils.SCHEDULE_TABLE + " WHERE " + Utils.AD_SCHEDULE_CODE + "=" + "'" + code + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ScheduleModel scheduleModel = new ScheduleModel();
        cursor.moveToLast();
        int count = cursor.getCount();
        if (count > 0) {
            scheduleModel.setInteraction_id(cursor.getString(cursor.getColumnIndex("interaction_id")));
            scheduleModel.setSchedule_id(cursor.getString(cursor.getColumnIndex("schedule_id")));
        }
        return scheduleModel;
    }

    public AdModel getIneractionByID(String interaction_id) {

        String selectQuery = "SELECT file_name,interaction_id FROM " + Utils.INTERACTION_TABLE + " WHERE " + Utils.INT_INTERACTION_ID + "=" + "'" + interaction_id + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        AdModel adModel = new AdModel();
        cursor.moveToLast();
        int count = cursor.getCount();
        if (count > 0) {
            adModel.setInteraction_id(cursor.getString(cursor.getColumnIndex("interaction_id")));
            adModel.setFile_name(cursor.getString(cursor.getColumnIndex("file_name")));
        }

        return adModel;
    }


    public ArrayList<AdModel> getAllAddModels() {
        ArrayList<AdModel> adModels = new ArrayList<>();
        String query = "SELECT * from " + INTERACTION_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                AdModel model = new AdModel();
                model.setInteraction_id(cursor.getString(cursor.getColumnIndex(INTERACTION_ID)));
                model.setApplication_id(cursor.getString(cursor.getColumnIndex(INT_APPLICATION_ID)));
                model.setIs_preload(cursor.getString(cursor.getColumnIndex(INT_IS_PRELOAD)));
                model.setHeading(cursor.getString(cursor.getColumnIndex(INT_HEADING)));
                model.setFile(cursor.getString(cursor.getColumnIndex(INT_FILE)));
                model.setFile_name(cursor.getString(cursor.getColumnIndex(INT_FILE_NAME)));
                model.setImage(cursor.getString(cursor.getColumnIndex(INT_IMAGE)));
                model.setImage_name(cursor.getString(cursor.getColumnIndex(INT_IMAGE_NAME)));
                model.setType(cursor.getString(cursor.getColumnIndex(INT_TYPE)));
                model.setDescription_heading(cursor.getString(cursor.getColumnIndex(INT_DESCRIPTION_HEADING)));
                model.setDescription(cursor.getString(cursor.getColumnIndex(INT_DESCRIPTION)));
                model.setDescription_image(cursor.getString(cursor.getColumnIndex(INT_DESCRIPTION_IMAGE)));
                model.setDescription_image_name(cursor.getString(cursor.getColumnIndex(INT_DESCRIPTION_IMAGE_NAME)));
                model.setVersion(cursor.getString(cursor.getColumnIndex(INT_VERSION)));
                model.setCreated_at(cursor.getString(cursor.getColumnIndex(INT_CREATED_AT)));

                model.setFeaturesModels(getAllFeatures(cursor.getString(cursor.getColumnIndex(INTERACTION_ID))));
                model.setScheduleModels(getAllSchedule(cursor.getString(cursor.getColumnIndex(INTERACTION_ID))));
                adModels.add(model);
            } while (cursor.moveToNext());
        }
        return adModels;
    }


    private ArrayList<ChanelModel> getAllChannel(String string) {
        ArrayList<ChanelModel> chanelModels = new ArrayList<>();
        String query = "SELECT * from " + CHANNEL_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                ChanelModel model = new ChanelModel();
                model.setChannel_id(cursor.getString(cursor.getColumnIndex(CHANNEL_ID)));
                model.setName(cursor.getString(cursor.getColumnIndex(CHANNEL_NAME)));
                model.setImage(cursor.getString(cursor.getColumnIndex(CHANNEL_IMAGE)));
                model.setImage_name(cursor.getString(cursor.getColumnIndex(CHANNEL_IMAGE_NAME)));
                model.setMisc(cursor.getString(cursor.getColumnIndex(CHANNEL_MISC)));
                model.setExtra(cursor.getString(cursor.getColumnIndex(CHANNEL_EXTRA)));

                chanelModels.add(model);
            } while (cursor.moveToNext());


        }
        return chanelModels;
    }


    private ArrayList<FeaturesModel> getAllFeatures(String string) {
        ArrayList<FeaturesModel> featuresModels = new ArrayList<>();
        String query = "SELECT * from " + INT_FEATURE_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int count = cursor.getCount();
        if (count > 0) {
            if (cursor.moveToFirst()) {
                do {
                    FeaturesModel model = new FeaturesModel();
                    model.setTrack_id(cursor.getString(cursor.getColumnIndex(FEATURE_TRACK_ID)));
                    model.setFeaturetype_id(cursor.getString(cursor.getColumnIndex(FEATURETYPE_ID)));
                    model.setInteraction_id(cursor.getString(cursor.getColumnIndex(INTERACTION_ID)));
                    model.setFeature_name(cursor.getString(cursor.getColumnIndex(FEATURE_NAME)));
                    model.setNumber(cursor.getString(cursor.getColumnIndex(FEATURE_NUMBER)));
                    model.setSms_text(cursor.getString(cursor.getColumnIndex(FEATURE_SMS_TEXT)));
                    model.setMail_text(cursor.getString(cursor.getColumnIndex(FEATURE_MAIL_TEXT)));
                    model.setEmail(cursor.getString(cursor.getColumnIndex(FEATURE_MAIL)));
                    model.setShare_text(cursor.getString(cursor.getColumnIndex(FEATURE_SHARE_TEXT)));
                    model.setTime(cursor.getString(cursor.getColumnIndex(FEATURE_TIME)));
                    model.setRequest_url(cursor.getString(cursor.getColumnIndex(FEATURE_REQUEST_URL)));
                    model.setOptions(cursor.getString(cursor.getColumnIndex(FEATURE_OPTIONS)));
                    model.setHeading(cursor.getString(cursor.getColumnIndex(FEATURE_HEADING)));
                    model.setDate(cursor.getString(cursor.getColumnIndex(FEATURE_DATE)));
                    model.setMisc(cursor.getString(cursor.getColumnIndex(FEATURE_MISC)));
                    model.setExtra(cursor.getString(cursor.getColumnIndex(FEATURE_EXTRA)));
                    featuresModels.add(model);


                } while (cursor.moveToNext());
            }

        }
        return featuresModels;
    }


    private ArrayList<ScheduleModel> getAllSchedule(String intractionId) {
        {
            ArrayList<ScheduleModel> scheduleModels = new ArrayList<>();
            String query = "SELECT * from " + SCHEDULE_TABLE + " WHERE " + INT_INTERACTION_ID + "=" + intractionId;
            SQLiteDatabase db = this.getWritableDatabase();
            ScheduleModel model = new ScheduleModel();
            Cursor cursor = db.rawQuery(query, null);


            int count = cursor.getCount();
            if (count > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        model.setSchedule_id(cursor.getString(cursor.getColumnIndex(SCHEDULE_ID)));
                        model.setInteraction_id(cursor.getString(cursor.getColumnIndex(INT_INTERACTION_ID)));
                        model.setGender(cursor.getString(cursor.getColumnIndex(SCHEDULE_GENDER)));
                        model.setStatus(cursor.getString(cursor.getColumnIndex(SCHEDULE_STATUS)));
                        model.setStart_date(cursor.getString(cursor.getColumnIndex(SCHEDULE_START_DATE)));
                        model.setStart_time(cursor.getString(cursor.getColumnIndex(SCHEDULE_START_TIME)));
                        model.setEnd_date(cursor.getString(cursor.getColumnIndex(SCHEDULE_END_DATE)));
                        model.setEnd_time(cursor.getString(cursor.getColumnIndex(SCHEDULE_END_TIME)));
                        model.setCode(cursor.getString(cursor.getColumnIndex(SCHEDULE_CODE)));
                        model.setIs_repeat(cursor.getString(cursor.getColumnIndex(SCHEDULE_IS_REPEAT)));
                        model.setRepeat_time(cursor.getString(cursor.getColumnIndex(SCHEDULE_REPEAT_TIME)));
                        model.setRepeat_type(cursor.getString(cursor.getColumnIndex(SCHEDULE_REPEAT_TYPE)));
                        model.setIs_reminder(cursor.getString(cursor.getColumnIndex(SCHEDULE_IS_REMINDER)));
                        model.setReminder_text(cursor.getString(cursor.getColumnIndex(SCHEDULE_REMINDER_TEXT)));
                        model.setReminder_time(cursor.getString(cursor.getColumnIndex(SCHEDULE_REMINDER_TIME)));
                        model.setCountry_id(cursor.getString(cursor.getColumnIndex(SCHEDULE_COUNTRY_ID)));
                        model.setState_id(cursor.getString(cursor.getColumnIndex(SCHEDULE_STATE_ID)));
                        model.setCity_id(cursor.getString(cursor.getColumnIndex(SCHEDULE_CITY_ID)));
                        model.setIncome_id(cursor.getString(cursor.getColumnIndex(SCHEDULE_INCOME_ID)));
                        model.setChannelModels(getAllChannel(cursor.getString(cursor.getColumnIndex(SCHEDULE_ID))));
                        model.setEarnModels(getAllEarnings(intractionId));
                        scheduleModels.add(model);
                    } while (cursor.moveToNext());
                }
            }
            return scheduleModels;
        }
    }

    private ArrayList<EarnModel> getAllEarnings(String intractionId) {

        ArrayList<EarnModel> earnModels = new ArrayList<>();
        String query = "SELECT * from " + EARNING_TABLE;
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);


        if (cursor.moveToFirst()) {
            do {
                EarnModel earnModel = new EarnModel();
                earnModel.setId(cursor.getInt(cursor.getColumnIndex("id")));
                earnModel.setEarning_id(cursor.getString(cursor.getColumnIndex(EARNING_EARNING_ID)));
                earnModel.setType(cursor.getString(cursor.getColumnIndex(EARNING_TYPE)));
                earnModel.setTrack_id(cursor.getString(cursor.getColumnIndex(EARNING_TRACK_ID)));
                earnModel.setDisplay_text(cursor.getString(cursor.getColumnIndex(EARNING_DISPLAY_TEXT)));
                earnModel.setValue_to_earn(cursor.getString(cursor.getColumnIndex(EARNING_VALUE_TO_EARN)));
                earnModel.setShow_in_app(cursor.getString(cursor.getColumnIndex(EARNING_SHOW_IN_APP)));
                earnModel.setAggregate_to_earn(cursor.getString(cursor.getColumnIndex(EARNING_AGGREGATE_TO_EARN)));
                earnModel.setMin_aggregate_value(cursor.getString(cursor.getColumnIndex(EARNING_MIN_AGGREGATE_VALUE)));
                earnModel.setReset_aggregate_count(cursor.getString(cursor.getColumnIndex(EARNING_RESET_AGGREGATE_COUNT)));
                earnModel.setEarn_bonus(cursor.getString(cursor.getColumnIndex(EARNING_EARN_BONUS)));
                earnModel.setTotal_bonus(cursor.getString(cursor.getColumnIndex(EARNING_TOTAL_AGGREGATE_COUNT)));
                earnModel.setEarn_bonus(cursor.getString(cursor.getColumnIndex(EARNING_TOTAL_BONUS)));
                earnModels.add(earnModel);

            } while (cursor.moveToNext());


        }


        return earnModels;
    }

    public AdModel getIntractionVersion(String interaction_id) {

        String selectQuery = "SELECT version,interaction_id FROM " + Utils.INTERACTION_TABLE + " WHERE " + Utils.INT_INTERACTION_ID + "=" + "'" + interaction_id + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        AdModel adModel = new AdModel();
        cursor.moveToLast();
        int count = cursor.getCount();
        if (count > 0) {
            //adModel.setInteraction_id(cursor.getString(cursor.getColumnIndex("interaction_id")));
            adModel.setVersion(cursor.getString(cursor.getColumnIndex("version")));
        }

        return adModel;
    }

    public EarnModel getEarningBtTrackId(String trackId) {
        String selectQuery = "SELECT value_to_earn,earning_id FROM " + Utils.EARNING_TABLE + " WHERE " + Utils.EARNING_TRACK_ID + "=" + "'" + trackId + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        EarnModel earnModel = new EarnModel();
        cursor.moveToLast();
        int count = cursor.getCount();
        if (count > 0) {
            //adModel.setInteraction_id(cursor.getString(cursor.getColumnIndex("interaction_id")));
             earnModel.setValue_to_earn(cursor.getString(cursor.getColumnIndex("value_to_earn")));
             earnModel.setEarning_id(cursor.getString(cursor.getColumnIndex("earning_id")));
        }

        return earnModel;


    }

    public void saveTransactionToDB(TransactionModel model) {
       SQLiteDatabase db = getWritableDatabase();
try {
    ContentValues values = new ContentValues();

    values.put(TRANSACTION_TRACK_ID, model.getTrack_id());
    values.put(TRANSACTION_SCHEDULE_ID, model.getSchedule_id());
    values.put(TRANSACTION_MISC, model.getMisc_earningId());
    values.put(TRANSACTION_MISC2, model.getMisc2());
    values.put(TRANSACTION_UPLOADED, model.getUploaded());

    db.insert(TRANSACTION_TABLE, null, values);
}catch(Exception e){
    e.printStackTrace();
}

    }
}

