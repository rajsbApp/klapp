package in.grasshoppermedia.klapp.Events;

/**
 * Created by katrina on 10/05/16.
 */
public class NewdAdEvent {

    String adName = "";

    public String getAdName() {
        return adName;
    }

    public NewdAdEvent(String adName) {
        this.adName = adName;
    }

}
