package in.grasshoppermedia.klapp.homeFragments;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.Utils.Session;
import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.activities.HomeActivity;
import in.grasshoppermedia.klapp.adapter.MyKlappsAdapter;
import in.grasshoppermedia.klapp.customviews.CustomTextBold;
import in.grasshoppermedia.klapp.model.MyKlappsModel;
import in.grasshoppermedia.klapp.services.CallService;

public class MyKlapps extends Fragment {

    ListView myklapp_list;
    ArrayList<MyKlappsModel> mainModel;
    MyKlappsAdapter adapter;
    Session session;
    CustomTextBold myklapps_value;
    RelativeLayout my_klapps_direction;

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_my_klapps, container, false);
        init(view);
        return view;
    }

    private void init(View view){

        session = Session.getSession(getActivity());

        mainModel = new ArrayList<>();

        adapter = new MyKlappsAdapter(getActivity(),0,mainModel);

        myklapps_value = (CustomTextBold)view.findViewById(R.id.myklapps_value);
        my_klapps_direction = (RelativeLayout)view.findViewById(R.id.my_klapps_direction);
        my_klapps_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).popFragments();
            }
        });

        // to get all the klapps
        CallService status_service = new CallService(getActivity(), Utils.BASE_URL + Utils.STATUS + "?user_id=" + session.getUser_id(), Utils.GET, null, false, new CallService.OnServicecall() {
            @Override
            public void onServicecall(String response) {
                try{
                    JSONObject jobj = new JSONObject(response);
                    JSONObject innerJobj = jobj.getJSONObject("response");
                    String total_earn = innerJobj.getString("total_earn");
                    String total_withdraw = innerJobj.getString("total_withdraw");
                    String balance = innerJobj.getString("balance");
                    myklapps_value.setText(balance);

                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });

        if (Build.VERSION.SDK_INT >= 11) {
            // --post GB use serial executor by default --
            status_service.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            // --GB uses ThreadPoolExecutor by default--
            status_service.execute();
        }

        getList();

        myklapp_list = (ListView)view.findViewById(R.id.myklapp_list);
        myklapp_list.setAdapter(adapter);

    }

    private void getList(){

        CallService service = new CallService(getActivity(), Utils.BASE_URL + Utils.HISTORY_API+"?user_id="+session.getUser_id(), Utils.GET, null, false, new CallService.OnServicecall() {
            @Override
            public void onServicecall(String response) {
                try {
                    JSONPARSOR(response);
                }catch (Exception ex){
                    ex.printStackTrace();
                }

            }
        });

        if (Build.VERSION.SDK_INT >= 11) {
            // --post GB use serial executor by default --
            service.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            // --GB uses ThreadPoolExecutor by default--
            service.execute();
        }
    }

    private void JSONPARSOR(String response) throws  Exception{
        JSONObject jobj = new JSONObject(response);
        JSONArray jarr = jobj.getJSONArray("response");
        for (int i=0;i<jarr.length();i++){
            JSONObject innerJob = jarr.getJSONObject(i);

            String advertisement_heading = ""+innerJob.getString("advertisement_heading");
            String advertisement_id = ""+innerJob.getString("advertisement_id");
            String advertisement_image = ""+innerJob.getString("advertisement_image");
            String advertisement_name = ""+innerJob.getString("advertisement_name");
            String earned = ""+innerJob.getString("earned");
            String time = ""+innerJob.getString("time");
            String transaction_id = ""+innerJob.getString("transaction_id");
            String winner_status = ""+innerJob.getString("winner_status");

            MyKlappsModel model = new MyKlappsModel();
            model.setAdvertisement_heading(advertisement_heading);
            model.setAdvertisement_id(advertisement_id);
            model.setAdvertisement_image(advertisement_image);
            model.setAdvertisement_name(advertisement_name);
            model.setEarned(earned);
            model.setTime(time);
            model.setTransaction_id(transaction_id);
            model.setWinner_status(winner_status);

            mainModel.add(model);
        }

        if(mainModel!=null && mainModel.size()>0){
            adapter.notifyDataSetChanged();
        }
    }

}
