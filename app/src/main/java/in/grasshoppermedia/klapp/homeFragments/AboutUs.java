package in.grasshoppermedia.klapp.homeFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.activities.HomeActivity;

public class AboutUs extends Fragment {

    View view;
    RelativeLayout about_us_direction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_about_us, container, false);
        init();
        return view;
    }

    private void init(){
        about_us_direction = (RelativeLayout)view.findViewById(R.id.about_us_direction);
        about_us_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).popFragments();
            }
        });
    }
}
