package in.grasshoppermedia.klapp.homeFragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.Utils.Session;
import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.activities.Splash;
import in.grasshoppermedia.klapp.activities.VideoPlay;
import in.grasshoppermedia.klapp.customviews.CustomText;
import in.grasshoppermedia.klapp.db.DatabaseHelper;

public class Settings extends Fragment {

    View view;
    CustomText settings_play_video,settings_logout;
    Session session;
    DatabaseHelper db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_settings, container, false);
        init(view);
        return view;
    }

    private void init(View view){
        settings_play_video = (CustomText)view.findViewById(R.id.settings_play_video);
        settings_play_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), VideoPlay.class);
                intent.putExtra(Utils.FROM,"e");
                getActivity().startActivity(intent);
            }
        });

        settings_logout = (CustomText)view.findViewById(R.id.settings_logout);
        settings_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                synchronized (this){
                    logOut();
                }
                startActivity(new Intent(getActivity(), Splash.class));
                getActivity().finish();
            }
        });
    }

    private void logOut(){
        session = Session.getSession(getActivity());
        session.cleanAll();

        db = DatabaseHelper.getDb(getActivity());
        try{
            db.cleanAllTables();
        } catch (Exception ex){
            ex.printStackTrace();
        }

        // write code to delete all files also
        Utils.deleteRecursive(new File(Environment.getExternalStorageDirectory()
                + File.separator+Utils.root_folder));

    }
}
