package in.grasshoppermedia.klapp.homeFragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.customviews.CustomText;
import in.grasshoppermedia.klapp.customviews.CustomTextBold;
import in.grasshoppermedia.klapp.model.ChanelModel;
import in.grasshoppermedia.klapp.model.EarnModel;

/**
 * Created by katrina on 06/02/17.
 */

public class EarnFragment extends Fragment{
    Context context;
    EarnModel model;
    ChanelModel chanel_model;
    CustomText earn_heading,channel_name;
    CustomTextBold earn_value;
    ImageView chanel_logo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.earn_fragment, container, false);

        model = getArguments().getParcelable("model");
        chanel_model = getArguments().getParcelable("chanelModel");
        if(model!=null && chanel_model!=null){
         //   earnInit(v);
        }

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void earnInit(View v){

        // view by ids
        earn_heading = (CustomText)v.findViewById(R.id.earn_heading);

        chanel_logo = (ImageView)v.findViewById(R.id.chanel_logo);
        try {

            String image_url = chanel_model.getImage();

            image_url = image_url.replace(" ", "%20");

            Picasso.with(context).load(image_url).fit()
                    .into(chanel_logo);

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        channel_name = (CustomText)v.findViewById(R.id.channel_name);
        channel_name.setText(chanel_model.getName());

        earn_value = (CustomTextBold)v.findViewById(R.id.earn_value);
        // ends here

        try{
            JSONObject jobj = new JSONObject("");
            String type = jobj.getString("type");
            if(type.equalsIgnoreCase("1")){
                String earning_id = jobj.getString("earning_id");
                String track_id = jobj.getString("track_id");

                String display_text = jobj.getString("display_text");
                earn_heading.setText(display_text);

                String value_to_earn = jobj.getString("value_to_earn");
                earn_value.setText(value_to_earn);

                String aggregate_to_earn = jobj.getString("aggregate_to_earn");
                String min_aggregate_value = jobj.getString("min_aggregate_value");
                String reset_aggregate_count = jobj.getString("reset_aggregate_count");
                String earn_bonus = jobj.getString("earn_bonus");
                String total_aggregate_count = jobj.getString("total_aggregate_count");
                String total_bonus = jobj.getString("total_bonus");

            } else if(type.equalsIgnoreCase("2")){
                String earning_id = jobj.getString("earning_id");
                String track_id = jobj.getString("track_id");

                String display_text = jobj.getString("display_text");
                earn_heading.setText(display_text);

                String value_to_earn = jobj.getString("value_to_earn");
                earn_value.setText(value_to_earn);

                String notification_type = jobj.getString("notification_type");
                String notification_text = jobj.getString("notification_text");
            }


        } catch (Exception ex){
            ex.printStackTrace();
        }



    }

    public static EarnFragment newInstance(EarnModel model,ChanelModel chanel_model) {
        EarnFragment f = new EarnFragment();
        Bundle b = new Bundle();
     //   b.putParcelable("model", model);
        b.putParcelable("chanelModel",chanel_model);
        f.setArguments(b);
        return f;
    }
}
