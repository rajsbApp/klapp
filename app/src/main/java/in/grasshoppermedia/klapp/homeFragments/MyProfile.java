package in.grasshoppermedia.klapp.homeFragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.Utils.Session;
import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.activities.HomeActivity;
import in.grasshoppermedia.klapp.customviews.CustomText;
import in.grasshoppermedia.klapp.model.ProfileModel;

public class MyProfile extends Fragment {

    RelativeLayout my_profile_edit,my_profile_direction;
    CustomText my_profile_name,my_profile_number,my_profile_email,my_profile_gender,my_profile_income,my_profile_address;

    View view;
    Session session;
    String userData = "";
    Utils utils;
    ProfileModel model;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_my_profile, container, false);
        init();
        return view;
    }

    private void init(){

        session = Session.getSession(getActivity());
        userData = session.getUserData();
        Log.d("ankit","userData::::"+userData);
        utils = new Utils();
        my_profile_edit = (RelativeLayout)view.findViewById(R.id.my_profile_edit);
        my_profile_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).popFragments();
                ((HomeActivity) getActivity()).addfragment(new EditProfile(), "EditProfile");

            }
        });

        my_profile_name = (CustomText)view.findViewById(R.id.my_profile_name);
        my_profile_number = (CustomText)view.findViewById(R.id.my_profile_number);
        my_profile_email = (CustomText)view.findViewById(R.id.my_profile_email);
        my_profile_gender = (CustomText)view.findViewById(R.id.my_profile_gender);
        my_profile_income = (CustomText)view.findViewById(R.id.my_profile_income);
        my_profile_address = (CustomText)view.findViewById(R.id.my_profile_address);

        my_profile_direction = (RelativeLayout)view.findViewById(R.id.my_profile_direction);
        my_profile_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).popFragments();
            }
        });

//        setUserData();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void getUserdata(){
//        String url = Utils.BASE_URL+"user/"+session.getUser_id()+"/update_profile";
//        CallPost postService = new CallPost(context,url,)
    }

//    private void setUserData(){
//        Log.d("ankit","m here::::"+userData);
//        if(userData!=null && !userData.equalsIgnoreCase("")){
//            Log.d("ankit","m here::::2");
//            try{
//                model = utils.parseProfiledata(userData);
//                if(model!=null){
//                    my_profile_name.setText(""+model.getName());
//                    my_profile_number.setText(""+session.getPhoneNumber());
//                    my_profile_email.setText(""+model.getEmail());
//                    my_profile_gender.setText(""+model.getGender());
//                    my_profile_income.setText(""+model.getIncome_value());
//                    my_profile_address.setText(""+model.getAddress1());
//                }
//            } catch (Exception ex){
//                ex.printStackTrace();
//            }
//
//        }
//    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(AutoregistrationModel model){
//        setUserData();
//        Toast.makeText(getActivity(),"m here",Toast.LENGTH_LONG).show();
//    }
}
