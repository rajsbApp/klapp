package in.grasshoppermedia.klapp.homeFragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.Utils.Session;
import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.activities.HomeActivity;
import in.grasshoppermedia.klapp.customviews.CustomText;
import in.grasshoppermedia.klapp.model.ProfileModel;
import in.grasshoppermedia.klapp.services.CallPost;

public class EditProfile extends Fragment {

    View view;
    RelativeLayout profile_camera,profile_save;
    ImageView edit_profile_top;
    private static final int CAMERA_REQUEST = 1;
    private static final int SELECT_PICTURE = 2;
    private String selectedImagePath;
    public static EditText edit_profile_date;
    Session session;
    EditText    edit_profile_name,//1
                edit_profile_gender,//2
                edit_profile_mobile,//3
                edit_profile_email,//4
                edit_profile_income,//5
                edit_profile_address_one,//6
                edit_profile_address_two,//7
                edit_profile_city,//8
                edit_profile_state,//9
                edit_profile_country,//10
                edit_profile_pincode;//11

    String userData = "",userProfileString = "";
    ProfileModel profile_model;
    Utils utils;
    RelativeLayout edit_profile_direction;
    Context context;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_edit_profile, container, false);
        init();
        return view;
    }

    private void init(){

        // get session
        session = Session.getSession(getActivity());
        utils = new Utils();
        userData = session.getUserData();

        edit_profile_direction = (RelativeLayout)view.findViewById(R.id.edit_profile_direction);
        edit_profile_direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).popFragments();
            }
        });

        profile_camera = (RelativeLayout)view.findViewById(R.id.profile_camera);
        profile_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeDialog();
            }
        });

        profile_save = (RelativeLayout)view.findViewById(R.id.profile_save);
        profile_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edit_profile_nameStr = edit_profile_name.getText().toString().trim();
                String edit_profile_genderStr = edit_profile_gender.getText().toString().trim();
                String edit_profile_mobileStr = edit_profile_mobile.getText().toString().trim();
                String edit_profile_emailStr = edit_profile_email.getText().toString().trim();
                String edit_profile_incomeStr = edit_profile_income.getText().toString().trim();
                String edit_profile_address_oneStr = edit_profile_address_one.getText().toString().trim();
                String edit_profile_address_twoStr = edit_profile_address_two.getText().toString().trim();
                String edit_profile_cityStr = edit_profile_city.getText().toString().trim();
                String edit_profile_stateStr = edit_profile_state.getText().toString().trim();
                String edit_profile_countryStr = edit_profile_country.getText().toString().trim();
                String edit_profile_pincodeStr = edit_profile_pincode.getText().toString().trim();

                if(checkEmpty(edit_profile_nameStr)){
                    showError("Please enter your name");
                } else if(checkEmpty(edit_profile_genderStr)){
                    showError("Please enter your gender");
                } else if(checkEmpty(edit_profile_mobileStr)){
                    showError("Please enter your mobile number");
                } else if(checkEmpty(edit_profile_emailStr)){
                    showError("Please enter your email id");
                } else if(checkEmpty(edit_profile_incomeStr)){
                    showError("Please enter your income");
                } else if(checkEmpty(edit_profile_address_oneStr)){
                    showError("Please enter your address");
                } else if(checkEmpty(edit_profile_address_twoStr)){
                    showError("Please enter your full address");
                } else if(checkEmpty(edit_profile_cityStr)){
                    showError("Please enter your city");
                } else if(checkEmpty(edit_profile_stateStr)){
                    showError("Please enter your state");
                } else if(checkEmpty(edit_profile_countryStr)){
                    showError("Please enter your country");
                } else if(checkEmpty(edit_profile_pincodeStr)){
                    showError("Please enter your pincode");
                } else {
                    try {
                        ProfileModel model = new ProfileModel();
                        model.setUser_id(session.getUser_id());
                        model.setName("" + edit_profile_nameStr);
                        model.setEmail("" + edit_profile_emailStr);
                        model.setPassword("");
                        model.setGender("" + edit_profile_genderStr);
//                        model.setIncome_value("" + edit_profile_incomeStr);
                        model.setIncome_value("");
                        model.setCountry("" + edit_profile_countryStr);
                        model.setState("" + edit_profile_stateStr);
                        model.setCity(""+edit_profile_cityStr);
                        model.setAddress1("" + edit_profile_address_oneStr);
                        model.setAddress2("" + edit_profile_address_twoStr);
                        updateProfile(model);
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }

                }
            }
        });

        // all the profile widgets
        edit_profile_top = (ImageView)view.findViewById(R.id.edit_profile_top);
        edit_profile_date= (EditText)view.findViewById(R.id.edit_profile_date);
        edit_profile_date.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    showDatePickerDialog();
                }
            }
        });

        edit_profile_name = (EditText)view.findViewById(R.id.edit_profile_name);
        edit_profile_gender = (EditText)view.findViewById(R.id.edit_profile_gender);
        edit_profile_gender.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    makeDialog("Set your gender", true, new SetOnDialogButton() {
                        @Override
                        public void OnYesButton() {
                            edit_profile_gender.setText("Male");
                        }

                        @Override
                        public void OnNoButton() {
                            edit_profile_gender.setText("Female");
                        }
                    });
                }
            }
        });

        edit_profile_mobile = (EditText)view.findViewById(R.id.edit_profile_mobile);
        edit_profile_mobile.setText(session.getPhoneNumber());
        edit_profile_email = (EditText)view.findViewById(R.id.edit_profile_email);
        edit_profile_income = (EditText)view.findViewById(R.id.edit_profile_income);
        edit_profile_income.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {

                    makeIncomeDialog(new SetOnIncomeDialog() {
                        @Override
                        public void Onincome_below() {
                            edit_profile_income.setText("< 300000");
                        }

                        @Override
                        public void Onincome_middle() {
                            edit_profile_income.setText("300000 - 1000000");
                        }

                        @Override
                        public void Onincome_bottom() {
                            edit_profile_income.setText("> 1000000");
                        }
                    });

                }
            }
        });

        edit_profile_address_one = (EditText)view.findViewById(R.id.edit_profile_address_one);
        edit_profile_address_two = (EditText)view.findViewById(R.id.edit_profile_address_two);
        edit_profile_city = (EditText)view.findViewById(R.id.edit_profile_city);
        edit_profile_state = (EditText)view.findViewById(R.id.edit_profile_state);
        edit_profile_country = (EditText)view.findViewById(R.id.edit_profile_country);
        edit_profile_pincode = (EditText)view.findViewById(R.id.edit_profile_pincode);

        setUserData();
    }

    private void setUserData(){
        if(userData!=null && !userData.equalsIgnoreCase("")){
            try{
                profile_model = utils.parseProfiledata(userData);
                if(profile_model!=null){
                    edit_profile_name.setText(""+profile_model.getName());
                    edit_profile_email.setText(""+profile_model.getEmail());
                    edit_profile_gender.setText(""+profile_model.getGender());
                    edit_profile_income.setText(""+profile_model.getIncome_value());
                    edit_profile_address_one.setText(""+profile_model.getAddress1());
                }
            } catch (Exception ex){
                ex.printStackTrace();
            }

        }
    }

    private void makeDialog(){
        // custom dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_dialog);

        CustomText dialog_camera = (CustomText)dialog.findViewById(R.id.dialog_camera);
        dialog_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                imageFromCamera();
            }
        });

        CustomText dialog_card = (CustomText)dialog.findViewById(R.id.dialog_card);
        dialog_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                imageFromCard();
            }
        });

        CustomText dialog_view = (CustomText)dialog.findViewById(R.id.dialog_view);
        dialog_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private void imageFromCamera(){
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    private void imageFromCard(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(intent, "Select Picture"),
                SELECT_PICTURE);
    }

    @SuppressLint("NewApi")
    public  String getRealPathFromURI_API19(Context context, Uri uri){
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = { MediaStore.Images.Media.DATA };

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{ id }, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }


    @SuppressLint("NewApi")
    public  String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if(cursor != null){
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }

    public  String getRealPathFromURI_BelowAPI11(Context context, Uri contentUri){
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        int column_index
                = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST && resultCode == getActivity().RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            edit_profile_top.setImageBitmap(photo);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();

        userProfileString = Base64.encodeToString(byteArray, Base64.DEFAULT);

        } else if(requestCode == SELECT_PICTURE && resultCode == getActivity().RESULT_OK){
            Uri selectedImageUri = data.getData();
            if (Build.VERSION.SDK_INT < 11)
                selectedImagePath = getRealPathFromURI_BelowAPI11(getActivity(), data.getData());

                // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19)
                selectedImagePath = getRealPathFromURI_API11to18(getActivity(), data.getData());

                // SDK > 19 (Android 4.4)
            else
                selectedImagePath = getRealPathFromURI_API19(getActivity(), data.getData());

            try{
                byte[] byteArray = loadFile(new File(selectedImagePath));
                userProfileString = Base64.encodeToString(byteArray, Base64.DEFAULT);
            } catch (Exception ex){
                ex.printStackTrace();
            }

            edit_profile_top.setImageURI(selectedImageUri);
            edit_profile_top.setRotation(getCameraPhotoOrientation(getActivity(),selectedImageUri,selectedImagePath));
        }
    }
    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int)length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }

        is.close();
        return bytes;
    }
    public int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath){
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile = new File(imagePath);

            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            String year1 = String.valueOf(year);
            String month1 = String.valueOf(month + 1);
            String day1 = String.valueOf(day);


            edit_profile_date.setText(day1 + "/" + month1 + "/" + year1);
        }
    }

    public void showDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getActivity().getFragmentManager(), "datePicker");
    }

    private void makeDialog(String message,boolean shouldshow,final SetOnDialogButton SetOnDialogButton){

        if(shouldshow) {
            // custom dialog
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_dialog);

            // set the custom dialog components - text, image and button
            TextView dialog_text = (TextView) dialog.findViewById(R.id.dialog_text);
            dialog_text.setText(message);


            Button dialog_yes = (Button) dialog.findViewById(R.id.dialog_yes);
            dialog_yes.setText("Male");
            // if button is clicked, close the custom dialog
            dialog_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    SetOnDialogButton.OnYesButton();
                }
            });


            Button dialog_no = (Button) dialog.findViewById(R.id.dialog_no);
            dialog_no.setText("Female");
            // if button is clicked, close the custom dialog
            dialog_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    SetOnDialogButton.OnNoButton();
                }
            });

            dialog.show();
        } else {
            SetOnDialogButton.OnYesButton();
        }

    }

    public interface SetOnDialogButton {

        public void OnYesButton();
        public void OnNoButton();
    }

    public interface SetOnIncomeDialog {

        public void Onincome_below();
        public void Onincome_middle();
        public void Onincome_bottom();
    }

    private void makeIncomeDialog(final SetOnIncomeDialog SetOnIncomeDialog){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.income_dialog);

        CustomText income_below = (CustomText)dialog.findViewById(R.id.income_below);
        income_below.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SetOnIncomeDialog.Onincome_below();
            }
        });

        CustomText income_middle = (CustomText)dialog.findViewById(R.id.income_middle);
        income_middle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SetOnIncomeDialog.Onincome_middle();
            }
        });

        CustomText income_bottom = (CustomText)dialog.findViewById(R.id.income_bottom);
        income_bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SetOnIncomeDialog.Onincome_bottom();
            }
        });

        dialog.show();
    }

    public void updateProfile(ProfileModel model) throws Exception{

        if(model!=null) {

            String url = Utils.BASE_URL + "user/"+session.getUser_id()+"/update_profile";

            Log.d("ankit",url);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("client_id", Utils.CLIENT_ID));
            Log.d("ankit","client_id:::"+Utils.CLIENT_ID);

            params.add(new BasicNameValuePair("city_id", model.getCity()));
            Log.d("ankit","city_id:::"+model.getCity());

            params.add(new BasicNameValuePair("state_id", model.getState()));
            Log.d("ankit","state_id:::"+model.getState());

            params.add(new BasicNameValuePair("country_id", model.getCountry()));
            Log.d("ankit","country_id:::"+model.getCountry());

            params.add(new BasicNameValuePair("lat", Utils.LATITUDE));
            Log.d("ankit","lat:::"+Utils.LATITUDE);

            params.add(new BasicNameValuePair("long", Utils.LONGITUDE));
            Log.d("ankit","long:::"+Utils.LONGITUDE);

            params.add(new BasicNameValuePair("email", model.getEmail()));
            Log.d("ankit","email:::"+model.getEmail());

            params.add(new BasicNameValuePair("dob", model.getDob()));
            Log.d("ankit","dob:::"+model.getDob());

            params.add(new BasicNameValuePair("user_image", userProfileString));
            Log.d("ankit","user_image:::"+userProfileString);

            params.add(new BasicNameValuePair("income", model.getIncome_value()));
            Log.d("ankit","income:::"+model.getIncome_value());

            params.add(new BasicNameValuePair("address", model.getAddress1()+"\n"+model.getAddress2()));
            Log.d("ankit","address:::"+model.getAddress1()+"\n"+model.getAddress2());

            CallPost service = new CallPost(context, url,  params, true, new CallPost.OnPostCall() {
                @Override
                public void OnPostCall(String response) {
                    try{
                        Log.d("ankit","::::"+response);

                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            });

            service.execute();

        } else {
         showError("Something went wrong please try again later");
        }

    }

    private void PARSE_JSON_DATA(String response) throws Exception{
        JSONObject jobj = new JSONObject(response);
        JSONObject dataJson = jobj.getJSONObject("data");
        String id = dataJson.getString("id");
        String application_id = dataJson.getString("application_id");
        String client_id = dataJson.getString("client_id");
        String city_id = dataJson.getString("city_id");
        String state_id = dataJson.getString("state_id");
        String country_id = dataJson.getString("country_id");
        String is_registered = dataJson.getString("is_registered");
        String is_active = dataJson.getString("is_active");
        String email = dataJson.getString("email");
        String name = dataJson.getString("name");
        String gender = dataJson.getString("gender");
        String dob = dataJson.getString("dob");
        String phone = dataJson.getString("phone");
        String income = dataJson.getString("income");
        String address = dataJson.getString("address");
        String user_image = dataJson.getString("user_image");
        String device_id = dataJson.getString("device_id");
        String device_type = dataJson.getString("device_type");
        String device_token = dataJson.getString("device_token");
        String lat = dataJson.getString("lat");
        String longs = dataJson.getString("long");
        String points = dataJson.getString("points");
        String status = dataJson.getString("status");
        String is_verified = dataJson.getString("is_verified");
        String otp = dataJson.getString("otp");
        String otp_created_at = dataJson.getString("otp_created_at");
        String created_at = dataJson.getString("created_at");
        String updated_at = dataJson.getString("updated_at");

    }

    private void showError(String message){
        Utils.showErrorDialog(getActivity(),message);
    }

    private boolean checkEmpty(String str){
        if(str.equalsIgnoreCase(""))
            return true;
        else
            return  false;
    }

}
