package in.grasshoppermedia.klapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by katrina on 02/02/17.
 */

public class ChanelModel implements Parcelable {
    private String channel_id = "";
    private String name = "";
    private String image = "";
    private String image_name = "";
    private String misc = "";

    public String getMisc() {
        return misc;
    }

    public void setMisc(String misc) {
        this.misc = misc;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    private String extra = "";

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public ChanelModel(){

    }

    protected ChanelModel(Parcel in) {
        channel_id = in.readString();
        name = in.readString();
        image = in.readString();
        image_name = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(channel_id);
        dest.writeString(name);
        dest.writeString(image);
        dest.writeString(image_name);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ChanelModel> CREATOR = new Parcelable.Creator<ChanelModel>() {
        @Override
        public ChanelModel createFromParcel(Parcel in) {
            return new ChanelModel(in);
        }

        @Override
        public ChanelModel[] newArray(int size) {
            return new ChanelModel[size];
        }
    };
}
