package in.grasshoppermedia.klapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by katrina on 09/05/16.
 */
public class AdModel implements Parcelable {
    private String interaction_id = "";
    private String application_id = "";
    private String is_preload = "";
    private String heading = "";
    private String file = "";
    private String file_name = "";
    private String image = "";
    private String image_name = "";
    private String type = "";
    private String description_heading = "";
    private String description_image = "";
    private String description_image_name = "";
    private String description = "";
    private String url = "";
    private String version = "";
    private String created_at = "";

    public AdModel(){

    }


    ArrayList<FeaturesModel> featuresModels = new ArrayList<>();
    ArrayList<ScheduleModel> scheduleModels = new ArrayList<>();

    public String getInteraction_id() {
        return interaction_id;
    }

    public void setInteraction_id(String interaction_id) {
        this.interaction_id = interaction_id;
    }

    public String getApplication_id() {
        return application_id;
    }

    public void setApplication_id(String application_id) {
        this.application_id = application_id;
    }

    public String getIs_preload() {
        return is_preload;
    }

    public void setIs_preload(String is_preload) {
        this.is_preload = is_preload;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription_heading() {
        return description_heading;
    }

    public void setDescription_heading(String description_heading) {
        this.description_heading = description_heading;
    }

    public String getDescription_image() {
        return description_image;
    }

    public void setDescription_image(String description_image) {
        this.description_image = description_image;
    }

    public String getDescription_image_name() {
        return description_image_name;
    }

    public void setDescription_image_name(String description_image_name) {
        this.description_image_name = description_image_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public ArrayList<FeaturesModel> getFeaturesModels() {
        return featuresModels;
    }

    public void setFeaturesModels(ArrayList<FeaturesModel> featuresModels) {
        this.featuresModels = featuresModels;
    }

    public ArrayList<ScheduleModel> getScheduleModels() {
        return scheduleModels;
    }

    public void setScheduleModels(ArrayList<ScheduleModel> scheduleModels) {
        this.scheduleModels = scheduleModels;
    }

    protected AdModel(Parcel in) {
        interaction_id = in.readString();
        application_id = in.readString();
        is_preload = in.readString();
        heading = in.readString();
        file = in.readString();
        file_name = in.readString();
        image = in.readString();
        image_name = in.readString();
        type = in.readString();
        description_heading = in.readString();
        description_image = in.readString();
        description_image_name = in.readString();
        description = in.readString();
        url = in.readString();
        version = in.readString();
        created_at = in.readString();
        if (in.readByte() == 0x01) {
            featuresModels = new ArrayList<FeaturesModel>();
            in.readList(featuresModels, FeaturesModel.class.getClassLoader());
        } else {
            featuresModels = null;
        }
        if (in.readByte() == 0x01) {
            scheduleModels = new ArrayList<ScheduleModel>();
            in.readList(scheduleModels, ScheduleModel.class.getClassLoader());
        } else {
            scheduleModels = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(interaction_id);
        dest.writeString(application_id);
        dest.writeString(is_preload);
        dest.writeString(heading);
        dest.writeString(file);
        dest.writeString(file_name);
        dest.writeString(image);
        dest.writeString(image_name);
        dest.writeString(type);
        dest.writeString(description_heading);
        dest.writeString(description_image);
        dest.writeString(description_image_name);
        dest.writeString(description);
        dest.writeString(url);
        dest.writeString(version);
        dest.writeString(created_at);
        if (featuresModels == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(featuresModels);
        }
        if (scheduleModels == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(scheduleModels);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AdModel> CREATOR = new Parcelable.Creator<AdModel>() {
        @Override
        public AdModel createFromParcel(Parcel in) {
            return new AdModel(in);
        }

        @Override
        public AdModel[] newArray(int size) {
            return new AdModel[size];
        }
    };
}