package in.grasshoppermedia.klapp.model;

/**
 * Created by katrina on 02/02/17.
 */

public class FeaturesModel {
    String track_id;
    String featuretype_id;
    String interaction_id ;
    String feature_name ;
    String number ;

    public String getTrack_id() {
        return track_id;
    }

    public void setTrack_id(String track_id) {
        this.track_id = track_id;
    }

    public String getFeaturetype_id() {
        return featuretype_id;
    }

    public void setFeaturetype_id(String featuretype_id) {
        this.featuretype_id = featuretype_id;
    }

    public String getInteraction_id() {
        return interaction_id;
    }

    public void setInteraction_id(String interaction_id) {
        this.interaction_id = interaction_id;
    }

    public String getFeature_name() {
        return feature_name;
    }

    public void setFeature_name(String feature_name) {
        this.feature_name = feature_name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSms_text() {
        return sms_text;
    }

    public void setSms_text(String sms_text) {
        this.sms_text = sms_text;
    }

    public String getMail_text() {
        return mail_text;
    }

    public void setMail_text(String mail_text) {
        this.mail_text = mail_text;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getShare_text() {
        return share_text;
    }

    public void setShare_text(String share_text) {
        this.share_text = share_text;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRequest_url() {
        return request_url;
    }

    public void setRequest_url(String request_url) {
        this.request_url = request_url;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMisc() {
        return misc;
    }

    public void setMisc(String misc) {
        this.misc = misc;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    String sms_text ;
    String mail_text ;
    String email ;
    String share_text;
    String  time;
    String request_url;
    String options;
    String heading;
    String date ;
    String  misc ;
    String extra;
}
