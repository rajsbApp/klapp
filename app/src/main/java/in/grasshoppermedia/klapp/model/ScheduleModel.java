package in.grasshoppermedia.klapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by katrina on 09/05/16.
 */
public class ScheduleModel implements Parcelable {
    public String getInteraction_id() {
        return interaction_id;
    }

    public void setInteraction_id(String interaction_id) {
        this.interaction_id = interaction_id;
    }

    private String interaction_id = "";
    private String schedule_id = "";
    private String gender = "";
    private String status = "";
    private String start_date = "";
    private String start_time = "";
    private String end_date = "";
    private String end_time = "";
    private String channel_id = "";
    private String code = "";
    private String is_repeat = "";
    private String repeat_time = "";
    private String repeat_type = "";
    private String is_reminder = "";
    private String reminder_text = "";
    private String reminder_time = "";
    private String country_id = "";
    private String state_id = "";
    private String city_id = "";
    private String income_id = "";

    ArrayList<EarnModel> earnModels = new ArrayList<>();
    ArrayList<ChanelModel> channelModels = new ArrayList<>();

    public ArrayList<Draw> getDraws() {
        return draws;
    }

    public void setDraws(ArrayList<Draw> draws) {
        this.draws = draws;
    }

    ArrayList<Draw> draws = new ArrayList<>();

    public ScheduleModel(){

    }

    public String getSchedule_id() {
        return schedule_id;
    }

    public void setSchedule_id(String schedule_id) {
        this.schedule_id = schedule_id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIs_repeat() {
        return is_repeat;
    }

    public void setIs_repeat(String is_repeat) {
        this.is_repeat = is_repeat;
    }

    public String getRepeat_time() {
        return repeat_time;
    }

    public void setRepeat_time(String repeat_time) {
        this.repeat_time = repeat_time;
    }

    public String getRepeat_type() {
        return repeat_type;
    }

    public void setRepeat_type(String repeat_type) {
        this.repeat_type = repeat_type;
    }

    public String getIs_reminder() {
        return is_reminder;
    }

    public void setIs_reminder(String is_reminder) {
        this.is_reminder = is_reminder;
    }

    public String getReminder_text() {
        return reminder_text;
    }

    public void setReminder_text(String reminder_text) {
        this.reminder_text = reminder_text;
    }

    public String getReminder_time() {
        return reminder_time;
    }

    public void setReminder_time(String reminder_time) {
        this.reminder_time = reminder_time;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getIncome_id() {
        return income_id;
    }

    public void setIncome_id(String income_id) {
        this.income_id = income_id;
    }

    public ArrayList<EarnModel> getEarnModels() {
        return earnModels;
    }

    public void setEarnModels(ArrayList<EarnModel> earnModels) {
        this.earnModels = earnModels;
    }

    public ArrayList<ChanelModel> getChannelModels() {
        return channelModels;
    }

    public void setChannelModels(ArrayList<ChanelModel> channelModels) {
        this.channelModels = channelModels;
    }

    protected ScheduleModel(Parcel in) {
        schedule_id = in.readString();
        gender = in.readString();
        status = in.readString();
        start_date = in.readString();
        start_time = in.readString();
        end_date = in.readString();
        end_time = in.readString();
        channel_id = in.readString();
        code = in.readString();
        is_repeat = in.readString();
        repeat_time = in.readString();
        repeat_type = in.readString();
        is_reminder = in.readString();
        reminder_text = in.readString();
        reminder_time = in.readString();
        country_id = in.readString();
        state_id = in.readString();
        city_id = in.readString();
        interaction_id=in.readString();
        income_id = in.readString();
        if (in.readByte() == 0x01) {
            earnModels = new ArrayList<EarnModel>();
            in.readList(earnModels, EarnModel.class.getClassLoader());
        } else {
            earnModels = null;
        }
        if (in.readByte() == 0x01) {
            channelModels = new ArrayList<ChanelModel>();
            in.readList(channelModels, ChanelModel.class.getClassLoader());
        } else {
            channelModels = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(schedule_id);
        dest.writeString(gender);
        dest.writeString(status);
        dest.writeString(start_date);
        dest.writeString(start_time);
        dest.writeString(end_date);
        dest.writeString(end_time);
        dest.writeString(channel_id);
        dest.writeString(code);
        dest.writeString(is_repeat);
        dest.writeString(repeat_time);
        dest.writeString(repeat_type);
        dest.writeString(is_reminder);
        dest.writeString(reminder_text);
        dest.writeString(reminder_time);
        dest.writeString(country_id);
        dest.writeString(state_id);
        dest.writeString(city_id);
        dest.writeString(income_id);
        if (earnModels == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(earnModels);
        }
        if (channelModels == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(channelModels);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ScheduleModel> CREATOR = new Parcelable.Creator<ScheduleModel>() {
        @Override
        public ScheduleModel createFromParcel(Parcel in) {
            return new ScheduleModel(in);
        }

        @Override
        public ScheduleModel[] newArray(int size) {
            return new ScheduleModel[size];
        }
    };
}