package in.grasshoppermedia.klapp.model;

/**
 * Created by katrina on 10/08/16.
 */
public class MyKlappsModel {
//            "transaction_id": "57a9f20dfdca22fb46f7bd2f",
//            "time": "2016-08-09T15:09:01.535Z",
//            "earned": 6,
//            "advertisement_id": "57a2ccc3f18fc53653b4db7c",
//            "winner_status": "waiting",
//            "advertisement_name": "Shila Dixit",
//            "advertisement_heading": "Shila dixit",
//            "advertisement_image": "http://104.199.170.65/files/ads/images/8f1cfaab1343fa09e2075ee217bfa7e4.png"

    private String advertisement_heading = "";
    private String advertisement_id = "";
    private String advertisement_image = "";
    private String advertisement_name = "";
    private String earned = "";
    private String time = "";
    private String transaction_id = "";
    private String winner_status = "";

    public String getWinner_status() {
        return winner_status;
    }

    public void setWinner_status(String winner_status) {
        this.winner_status = winner_status;
    }

    public String getAdvertisement_heading() {
        return advertisement_heading;
    }

    public void setAdvertisement_heading(String advertisement_heading) {
        this.advertisement_heading = advertisement_heading;
    }

    public String getAdvertisement_id() {
        return advertisement_id;
    }

    public void setAdvertisement_id(String advertisement_id) {
        this.advertisement_id = advertisement_id;
    }

    public String getAdvertisement_image() {
        return advertisement_image;
    }

    public void setAdvertisement_image(String advertisement_image) {
        this.advertisement_image = advertisement_image;
    }

    public String getAdvertisement_name() {
        return advertisement_name;
    }

    public void setAdvertisement_name(String advertisement_name) {
        this.advertisement_name = advertisement_name;
    }

    public String getEarned() {
        return earned;
    }

    public void setEarned(String earned) {
        this.earned = earned;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }
}
