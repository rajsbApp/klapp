package in.grasshoppermedia.klapp.model;

/**
 * Created by katrina on 03/06/16.
 */

public class TransactionModel {
    public String getTrack_id() {
        return track_id;
    }

    public void setTrack_id(String track_id) {
        this.track_id = track_id;
    }

    public String getTransaction_date_and_time() {
        return transaction_date_and_time;
    }

    public void setTransaction_date_and_time(String transaction_date_and_time) {
        this.transaction_date_and_time = transaction_date_and_time;
    }

    public String getSchedule_id() {
        return schedule_id;
    }

    public void setSchedule_id(String schedule_id) {
        this.schedule_id = schedule_id;
    }

    public String getUploaded() {
        return uploaded;
    }

    public void setUploaded(String uploaded) {
        this.uploaded = uploaded;
    }

    public String getNumber_of_times() {
        return number_of_times;
    }

    public void setNumber_of_times(String number_of_times) {
        this.number_of_times = number_of_times;
    }

    public String getMisc_earningId() {
        return misc_earningId;
    }

    public void setMisc_earningId(String misc_earningId) {
        this.misc_earningId = misc_earningId;
    }

    public String getMisc2() {
        return misc2;
    }

    public void setMisc2(String misc2) {
        this.misc2 = misc2;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    private String track_id = "";//1
    private String transaction_date_and_time = "";//2
    private String schedule_id = "";//3
    private String uploaded = "";//4
    private String number_of_times = "";//5
    private String misc_earningId = "";//6
    private String misc2 = "";//7
    private String extra = "";//8


   }
