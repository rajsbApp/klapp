package in.grasshoppermedia.klapp.model;

/**
 * Created by katrina on 25/07/16.
 */
public class AudioEvent {

    private String audioOn = "";
    private int confidence;

    public AudioEvent(int confidence){
        setConfidence(confidence);
    }

    public int getConfidence() {
        return confidence;
    }

    public void setConfidence(int confidence) {
        this.confidence = confidence;
    }

    public String getAudioOn() {
        return audioOn;
    }

    public void setAudioOn(String audioOn) {
        this.audioOn = audioOn;
    }
}
