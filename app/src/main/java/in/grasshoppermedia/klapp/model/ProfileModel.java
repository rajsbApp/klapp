package in.grasshoppermedia.klapp.model;

/**
 * Created by katrina on 01/09/16.
 */
public class ProfileModel {
    private String user_image = ""; // 0..
    private String user_id = "";//1..
    private String name = "";//2..
    private String email = "";//3..
    private String password = "";//4..
    private String gender = "";//5..
    private String income_value = "";//6..
    private String country = "";//7..
    private String state = "";//8..
    private String city = "";//9..
    private String address1 = "";//10
    private String address2 = "";//11
    private String dob = "";// 12..

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIncome_value() {
        return income_value;
    }

    public void setIncome_value(String income_value) {
        this.income_value = income_value;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }
}
