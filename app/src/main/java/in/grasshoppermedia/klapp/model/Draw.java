package in.grasshoppermedia.klapp.model;

/**
 * Created by rbpatel on 6/23/2017.
 */

public class Draw {
    public String getDraw_id() {
        return draw_id;
    }

    public void setDraw_id(String draw_id) {
        this.draw_id = draw_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String draw_id;
    private String title;
    private String photo;
    private String status;
    private String description;
}
