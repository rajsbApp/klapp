package in.grasshoppermedia.klapp.model;

/**
 * Created by katrina on 04/05/16.
 */
public class TokenModel {

    private String token_id = "";
    private String token_name= "";
    private String token_url = "";
    private String token_expire_date = "";
    private String token_start_date = "";
    private String version = "";
    private String status = "";

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken_name() {
        return token_name;
    }

    public void setToken_name(String token_name) {
        this.token_name = token_name;
    }

    public String getToken_url() {
        return token_url;
    }

    public void setToken_url(String token_url) {
        this.token_url = token_url;
    }

    public String getToken_expire_date() {
        return token_expire_date;
    }

    public void setToken_expire_date(String token_expire_date) {
        this.token_expire_date = token_expire_date;
    }

    public String getToken_start_date() {
        return token_start_date;
    }

    public void setToken_start_date(String token_start_date) {
        this.token_start_date = token_start_date;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
