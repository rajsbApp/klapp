package in.grasshoppermedia.klapp.model;

/**
 * Created by katrina on 18/05/16.
 */
public class EarnModel {
    private  int id;
    private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEarning_id() {
        return earning_id;
    }

    public void setEarning_id(String earning_id) {
        this.earning_id = earning_id;
    }

    public String getTrack_id() {
        return track_id;
    }

    public void setTrack_id(String track_id) {
        this.track_id = track_id;
    }

    public String getDisplay_text() {
        return display_text;
    }

    public void setDisplay_text(String display_text) {
        this.display_text = display_text;
    }

    public String getValue_to_earn() {
        return value_to_earn;
    }

    public void setValue_to_earn(String value_to_earn) {
        this.value_to_earn = value_to_earn;
    }

    public String getShow_in_app() {
        return show_in_app;
    }

    public void setShow_in_app(String show_in_app) {
        this.show_in_app = show_in_app;
    }

    public String getAggregate_to_earn() {
        return aggregate_to_earn;
    }

    public void setAggregate_to_earn(String aggregate_to_earn) {
        this.aggregate_to_earn = aggregate_to_earn;
    }

    public String getMin_aggregate_value() {
        return min_aggregate_value;
    }

    public void setMin_aggregate_value(String min_aggregate_value) {
        this.min_aggregate_value = min_aggregate_value;
    }

    public String getReset_aggregate_count() {
        return reset_aggregate_count;
    }

    public void setReset_aggregate_count(String reset_aggregate_count) {
        this.reset_aggregate_count = reset_aggregate_count;
    }

    public String getEarn_bonus() {
        return earn_bonus;
    }

    public void setEarn_bonus(String earn_bonus) {
        this.earn_bonus = earn_bonus;
    }

    public String getTotal_aggregate_count() {
        return total_aggregate_count;
    }

    public void setTotal_aggregate_count(String total_aggregate_count) {
        this.total_aggregate_count = total_aggregate_count;
    }

    public String getTotal_bonus() {
        return total_bonus;
    }

    public void setTotal_bonus(String total_bonus) {
        this.total_bonus = total_bonus;
    }

    private String earning_id;
    private String track_id;
    private String display_text;
    private String value_to_earn;
    private String show_in_app;
    private String aggregate_to_earn;
    private String min_aggregate_value;
    private String reset_aggregate_count;
    private String earn_bonus;
    private String total_aggregate_count;
    private String total_bonus;

}
