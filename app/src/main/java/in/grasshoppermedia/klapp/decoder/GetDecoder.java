package in.grasshoppermedia.klapp.decoder;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.intrasonics.Decoder;
import com.intrasonics.DecoderConfidence;
import com.intrasonics.DecoderListener;
import com.intrasonics.ITokenUpdateListener;
import com.intrasonics.triggerengine.Codeword;
import com.intrasonics.triggerengine.ITriggerEngine;
import com.intrasonics.triggerengine.ITriggerEngineListener;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.activities.Ads;
import in.grasshoppermedia.klapp.db.DatabaseHelper;
import in.grasshoppermedia.klapp.model.AudioEvent;

public class GetDecoder implements DecoderListener, ITriggerEngineListener,
		ITokenUpdateListener {

	private Decoder mDecoder = null;

	private Context context;

	private final int msgTypeMessage = 0;
	private final int msgTypeCodeword = 1;
	private final int msgTypeConfidence = 2;
	private final int msgTypeFailed = 3;
	private final int msgTokenUpdated = 4;

    DatabaseHelper db;
	private ITriggerEngine triggerEngine = null;

	public GetDecoder(Context context,DatabaseHelper db) {

		this.context = context;

        this.db = db;

		// Get a reference to the Intrasonics Decoder
		mDecoder = Decoder.getInstance();

		// Pass the application context into the decoder
		mDecoder.setApplicationContext(context);

		// Register as a decoder listener
		mDecoder.registerListener(this);

		try {
			// Read in a licence token and pass it to the decoder

//			 String token = readTokenFromFile();
			String token = readTokenFromFile("557.json");
			mDecoder.setToken(token);
		} catch (Exception ex) {
			uiUpdateHandler.sendMessage(uiUpdateHandler.obtainMessage(
					msgTypeFailed, ex.getMessage()));
			Log.d("ankit", "error" + ex);
			ex.printStackTrace();
		}

		// Connect to the Trigger Engine
		triggerEngine = (ITriggerEngine) mDecoder.connectEngineListener(this,
				ITriggerEngine.class);

		if (triggerEngine == null) {
			uiUpdateHandler.sendMessage(uiUpdateHandler.obtainMessage(
					msgTypeFailed, "Failed to connect to Trigger Engine"));
		}

	}

	public void startDecoding() {
		// Start the decoder
		mDecoder.startDecoding();

	}

	public boolean isRunning(){
		return mDecoder.isListening();
	}

	public void stopDecoding() {

		// Unregister this object as the listener for the Trigger Engine
		if (triggerEngine != null) {
			mDecoder.disconnectFromEngine(this, triggerEngine);
		}

		// Stop the decoder.
		mDecoder.stopDecoding();

		// Unregister as a decoder listener
		mDecoder.unRegisterListener(this);
		mDecoder = null;

	}

	@Override
	public void onTokenUpdated(String arg0, Date arg1, Date arg2, long arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCodewordReceived(Codeword codeword) {
		// TODO Auto-generated method stub

		uiUpdateHandler.sendMessage(uiUpdateHandler.obtainMessage(
				msgTypeCodeword, codeword));

	}

	@Override
	public void onDecoderConfidenceChanged(DecoderConfidence confidence) {
		// TODO Auto-generated method stub
//		AudioEvent audioEvent = ;
//		audioEvent.setConfidence(confidence.swigValue());
		EventBus.getDefault().post(new AudioEvent(confidence.swigValue()));
	}

	@Override
	public void onDecoderFailed(String error, Date time) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDecoderIdle() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onDecoderStartedListening() {
		// TODO Auto-generated method stub

	}

	private Handler uiUpdateHandler = new Handler() {
		public void handleMessage(Message msg) {
			String newText = "";

			switch (msg.what) {
			case msgTypeMessage: {

				break;
			}
			case msgTypeCodeword: {
				if(!Utils.isLoad) {
					Codeword codeword = (Codeword) msg.obj;
					Log.d("ankit", "codeword ::::" + (int) codeword.codeword);
                    if(db.isADDSchedulePresent(""+(int) codeword.codeword)) {
                        Intent intent = new Intent(context, Ads.class);
                        intent.putExtra("code", "" + (int) codeword.codeword);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }

				}

				break;
			}
			case msgTypeConfidence: {
				break;
			}
			case msgTypeFailed: {
				Log.d("ankit", newText);
				break;
			}
			case msgTokenUpdated: {
				Log.d("ankit", newText);
				break;
			}
			default:
				break;
			}
		}
	};


	@Override
	public void onDecoderResetComplete() {
		// TODO Auto-generated method stub

	}

	public String readTokenFromFile(String fileName) throws IOException {
		String token = "empty";

		try {
			InputStream iS = context.getResources().getAssets().open(fileName);

			byte[] buffer = new byte[iS.available()];
			iS.read(buffer);

			ByteArrayOutputStream oS = new ByteArrayOutputStream();

			oS.write(buffer);
			token = oS.toString();
			oS.close();
			iS.close();
		} catch (IOException e) {
			Log.d("ankit", e.toString());
		}
		return token;
	}
}
