package in.grasshoppermedia.klapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.Utils.Utils;
import in.grasshoppermedia.klapp.model.AdModel;

/**
 * Created by katrina on 09/05/16.
 */
public class AdListAdapter extends ArrayAdapter<AdModel> {
    private static String LOG_TAG = "klapp";
    Context context;

    public AdListAdapter(Context context, int resource, ArrayList<AdModel> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    private static class ViewHolder {
        TextView promotext,earn_min,earn_min_value,earn_max,earn_max_value,start_time,start_date;
        ImageView ad_image,chanel_icon;
        LinearLayout top_linear;
        RelativeLayout first_earn_lin;
        LinearLayout second_earn_lin;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        AdModel model = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder holder; // view lookup cache stored in tag
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.ad_item,
                    parent, false);

            holder.promotext = (TextView)convertView.findViewById(R.id.promotext);
            holder.earn_min = (TextView)convertView.findViewById(R.id.earn_min);
            holder.earn_min_value = (TextView)convertView.findViewById(R.id.earn_min_value);
            holder.earn_max = (TextView)convertView.findViewById(R.id.earn_max);
            holder.earn_max_value = (TextView)convertView.findViewById(R.id.earn_max_value);
            holder.start_time = (TextView)convertView.findViewById(R.id.start_time);
//            start_date = (TextView)convertView.findViewById(R.id.start_date);
            holder.ad_image = (ImageView)convertView.findViewById(R.id.ad_image);
            holder.chanel_icon = (ImageView)convertView.findViewById(R.id.chanel_icon);

            holder.top_linear = (LinearLayout) convertView.findViewById(R.id.top_linear);

            holder.first_earn_lin = (RelativeLayout)convertView.findViewById(R.id.first_earn_lin);
            holder.second_earn_lin = (LinearLayout) convertView.findViewById(R.id.second_earn_lin);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.top_linear.requestLayout();
        holder.top_linear.getLayoutParams().height = (int)(Utils.getScreenHeight(context)*0.67)/2;

//        Log.d("ankit",model.getImage());
//        Log.d("ankit",model.getImage_name());




        holder.promotext.setText(model.getHeading());
        holder.start_time.setText(model.getScheduleModels().get(0).getStart_time());

        // for first earning
        {
            try{
              //  JSONObject jobj = new JSONObject(model.getScheduleModels().get(0).getEarnModels().get(0).getJsonData());

             //   String display_text = jobj.getString("display_text");
                holder.earn_min.setText(model.getScheduleModels().get(0).getEarnModels().get(0).getDisplay_text());

               // String value_to_earn = jobj.getString("value_to_earn");
                holder.earn_min_value.setText(model.getScheduleModels().get(0).getEarnModels().get(0).getValue_to_earn());

            } catch (Exception ex){
                ex.printStackTrace();
                holder.first_earn_lin.setVisibility(View.GONE);
            }
        }

        // for second earning
        {
            try{
             //   JSONObject jobj = new JSONObject(model.getScheduleModels().get(0).getEarnModels().get(1).getJsonData());

                //String display_text = jobj.getString("display_text");
                holder.earn_max.setText(model.getScheduleModels().get(0).getEarnModels().get(1).getDisplay_text());

                ///String value_to_earn = jobj.getString("value_to_earn");
                holder.earn_max_value.setText(model.getScheduleModels().get(0).getEarnModels().get(1).getValue_to_earn());

            } catch (Exception ex){
                ex.printStackTrace();
                holder.second_earn_lin.setVisibility(View.GONE);
            }
        }

        try {

            String image_url = model.getScheduleModels().get(0).getChannelModels().get(0).getImage();
            image_url = image_url.replace(" ", "%20");

            Picasso.with(context).load(image_url).fit()
                    .into(holder.chanel_icon);

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

        try {

            String image_url = model.getImage();
            image_url = image_url.replace(" ", "%20");

            Picasso.with(context).load(image_url).fit()
                    .into(holder.ad_image);

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
//
//        holder.start_time.setText(model.getStartTime());


        // Return the completed view to render on screen
        return convertView;
    }

}