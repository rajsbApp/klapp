package in.grasshoppermedia.klapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import in.grasshoppermedia.klapp.R;
import in.grasshoppermedia.klapp.customviews.CustomText;
import in.grasshoppermedia.klapp.customviews.CustomTextBold;
import in.grasshoppermedia.klapp.model.MyKlappsModel;

/**
 * Created by katrina on 10/08/16.
 */
public class MyKlappsAdapter extends ArrayAdapter<MyKlappsModel>{

    ArrayList<MyKlappsModel> objects;
    Context context;

    public MyKlappsAdapter(Context context, int resource, ArrayList<MyKlappsModel> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    private static class ViewHolder {

        CustomTextBold ad_name,myklapps_value;
        CustomText ad_time;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        MyKlappsModel model = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.myklapp_list_item,
                    parent, false);

            viewHolder.ad_name = (CustomTextBold)convertView.findViewById(R.id.ad_name);
            viewHolder.ad_time = (CustomText)convertView.findViewById(R.id.ad_time);
            viewHolder.myklapps_value = (CustomTextBold)convertView.findViewById(R.id.myklapps_value);


            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        viewHolder.ad_name.setText(model.getAdvertisement_heading());
        viewHolder.ad_time.setText(model.getTime());
        viewHolder.myklapps_value.setText(model.getEarned());

        // Return the completed view to render on screen
        return convertView;
    }
}
