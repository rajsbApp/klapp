package in.grasshoppermedia.klapp.Utils;

import android.content.Context;

/**
 * Created by katrina on 04/05/16.
 */
public class Session {
    Context context;

    private static Session instance = null;

    public static Session getSession(Context context) {
        if(instance == null) {
            instance = new Session(context);
        }
        return instance;
    }

    public String getPhoneNumber() {
        return Utils.getShared(context, Utils.PHONE_NUMBER, "");
    }

    public void setPhoneNumber(String phoneNumber) {
        Utils.setShared(context, Utils.PHONE_NUMBER, phoneNumber);
    }

    public String getLatitude() {
        return Utils.getShared(context, Utils.LATITUDE, "");
    }

    // lat setter
    public void setLatitude(String latitude) {
        Utils.setShared(context, Utils.LATITUDE, latitude);
    }

    public String getLongitude() {
        return Utils.getShared(context, Utils.LONGITUDE, "");
    }

    // long setter
    public void setLongitude(String longitude) {
        Utils.setShared(context, Utils.LONGITUDE, longitude);
    }

    public String getUserData() {
        return Utils.getShared(context, Utils.USER_DATA, "");
    }

    // user data setter
    public void setUserData(String userData) {
        Utils.setShared(context, Utils.USER_DATA, userData);
    }

    public String getDevice_id() {
        return Utils.getShared(context, Utils.DEVICE_ID, "");
    }

    // device id
    public void setDevice_id(String device_id) {
        Utils.setShared(context, Utils.DEVICE_ID, device_id);
    }

    public String getDevice_token() {
        return Utils.getShared(context, Utils.DEVICE_TOKEN, "");
    }

    // device token
    public void setDevice_token(String device_token) {
        Utils.setShared(context, Utils.DEVICE_TOKEN, device_token);
    }

    public String getShowNotification() {
        return Utils.getShared(context, Utils.NOTIFICATION, "");
    }

    // should show notification
    public void setShowNotification(String showNotification) {
        Utils.setShared(context, Utils.NOTIFICATION, showNotification);
    }

    public String getSetReminder() {
        return Utils.getShared(context, Utils.REMINDER, "");
    }

    // should set reminder
    public void setSetReminder(String setReminder) {
        Utils.setShared(context, Utils.REMINDER, setReminder);
    }

    protected Session(Context context) {
        this.context = context;
    }

    public String getOTP() {
        return Utils.getShared(context, Utils.OTP, "");
    }

    // set OTP
    public void setOTP(String OTP) {
        Utils.setShared(context, Utils.OTP, OTP);
    }

    public String getShow_video() {
        return Utils.getShared(context, Utils.show_video, "");
    }

    // should show video
    public void setShow_video(String show_video) {
        Utils.setShared(context, Utils.show_video, show_video);
    }

    public String getIsPlaying() {
        return Utils.getShared(context, Utils.is_playing_string, "");
    }

    // set is playing
    public void setIsPlaying(String isPlaying) {
        Utils.setShared(context, Utils.is_playing_string, isPlaying);
    }

    public String getUser_id() {
        return Utils.getShared(context, Utils.user_id_string, "");
    }

    // user id
    public void setUser_id(String user_id) {
        Utils.setShared(context, Utils.user_id_string, user_id);
    }

    public void cleanAll(){
        setLatitude("");
        setLongitude("");
        setUserData("");
        setDevice_id("");
        setDevice_token("");
        setShowNotification("");
        setSetReminder("");
        setOTP("");
        setShow_video("");
        setIsPlaying("");
        setUser_id("");
    }
}
