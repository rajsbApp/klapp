package in.grasshoppermedia.klapp.Utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import in.grasshoppermedia.klapp.activities.HomeActivity;
import in.grasshoppermedia.klapp.activities.VideoPlay;
import in.grasshoppermedia.klapp.background_process.DownloadAndSaveAds;
import in.grasshoppermedia.klapp.background_process.RestartService;
import in.grasshoppermedia.klapp.db.DatabaseHelper;
import in.grasshoppermedia.klapp.model.ProfileModel;
import in.grasshoppermedia.klapp.model.TokenModel;
import in.grasshoppermedia.klapp.services.CallService;
import in.grasshoppermedia.klapp.services.DownLoadFile;
import in.grasshoppermedia.klapp.services.OnFileDownload;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * Created by katrina on 04/05/16.
 */
public class Utils {

    public static final String INTRACTIONS ="intractiondata" ;
    public static final String AD_PARAM_APPLICATION ="application_id" ;
    public static final File ROOT_DIR = Environment.getExternalStorageDirectory();
    public static final String IS_PERMISSION_ALLOWED = "given";
    public static String SEND_EARNINGS="user/earn";
    GoogleCloudMessaging gcm;
    private String device_token = "";
    private String emei_id = "";

    public static int WATCHDOGTIMER = 10000;

    // for invisible folder
     //public static String root_folder = ".klapp";

    // for visible folder
    public static String root_folder = "klapp";

    public static String user_id_string = "user_id";
    public static String PHONE_NUMBER = "phone_number";
    public static String USER_DATA = "user_data";
    public static String is_playing_string = "is_playing";
    public static String show_video = "show_video";
    public static String OTP = "OTP";
    public static String NOTIFICATION = "notification";
    public static String REMINDER = "reminder";
    public static String DEVICE_ID = "device_id";
    public static String DEVICE_TOKEN = "device_token";
    public static String PLATEFORM = "android";
    public static String LATITUDE = "lat";
    public static String LONGITUDE = "Long";

    public static String FROM = "from";

    public static String TRUE = "true";
    public static String FALSE = "false";

    public static String CLIENT_ID = "2";
    public static String APP_ID = "34";

    public static String InternetError = "No Internet Present";

    public static String POST = "POST";
    public static String GET = "GET";
    public static String PUT = "PUT";

    public static String GCM_KEY = "582444993088";

    public static String BASE_URL = "https://klapp.tv/klapp/public/api/";

    // otp api
    public static String OTP_API = "user/register";

    // otp verify api
    public static String OTP_VERIFY_API = "user/otpverify"; //

    // otp api
    public static String GET_INTERACTIONS = "get_interactions";

    // register data
    public static String REGISTER_API = "user/complete-reg";


    // register data
    public static String UPDATE_PROFILE = "user/complete-reg";

    // auto register data
    public static String AUTO_REGISTER = "user/register";

    // login data
    public static String LOGIN_API = "user/login";

    // klapp history
    public static String HISTORY_API = "history";

    // get klapps
    public static String STATUS = "user/status";

    // token data
    public static String TOKEN_API = "token";
    public static String TOKEN_PARAM_CLIENT_ID = "client_id";
    public static String TOKEN_PARAM_APP_ID = "app_id";

    public static String TOKEN_TABLE_NAME = "token"; // table name
    public static String TOKEN_TOKEN_ID = "token_id"; //1
    public static String TOKEN_NAME = "token_name";   //2
    public static String TOKEN_URL = "token_url";     //3
    public static String TOKEN_EXPIRY_DATE = "token_expire_date"; //4
    public static String TOKEN_START_DATE = "token_start_date"; //5
    public static String TOKEN_VERSION = "version"; //6
    public static String TOKEN_STATUS = "status";   //7 download status


    public static boolean isLoad = false;

    /**
     * INtraction Data Key
     */


    public static String INTERACTION_TABLE="interactions";
    public static String INT_INTERACTION_ID="interaction_id";
    public static String INT_APPLICATION_ID= "application_id";
    public static String INT_IS_PRELOAD="is_preload";
    public static String INT_HEADING="heading";
    public static String INT_FILE="file";
    public static String INT_FILE_NAME="file_name";
    public static String INT_IMAGE="image";
    public static String INT_IMAGE_NAME="image_name";
    public static String INT_TYPE="type";
    public static String INT_DESCRIPTION_HEADING="description_heading";
    public static String INT_DESCRIPTION_IMAGE="description_image";
    public static String INT_DESCRIPTION_IMAGE_NAME="description_image_name";
    public static String INT_DESCRIPTION="description";
    public static String INT_URL ="url";
    public static String INT_VERSION="version";
    public static String INT_CREATED_AT="created_at";
    public static String INT_WATCHED="watched";
    public static String INT_MISC="misc";
    public static String INT_SHOW="show";



    // advertise data
    public static String AD_API = "get_interactions";
    public static String AD_PARAM_USERID = "user_id";

    public static String ADVERTISEMENT_TABLE_NAME = "advertisement"; // table name
    public static String ADVERTISEMENT_ID = "advertisement_id"; //1
    public static String ADVERTISEMENT_NAME = "advertisement_name"; //2
    public static String ADVERTISEMENT_HEADING = "advertisement_heading"; //3
    public static String ADVERTISEMENT_URL = "advertisement_url"; //4
    public static String ADVERTISEMENT_ICON = "advertisement_icon"; //5
    public static String ADVERTISEMENT_PRE_LOAD = "pre_load"; //6
    public static String ADVERTISEMENT_STATUS = "status"; //7 download status

    // advertisement_features data
    public static String INT_FEATURE_TABLE = "features"; // table name
    public static String FEATURE_TRACK_ID = "track_id";//1
    public static String FEATURETYPE_ID="featuretype_id" ;
    public static String INTERACTION_ID="interaction_id" ;
    public static String FEATURE_NAME="feature_name" ;
    public static String FEATURE_NUMBER="number" ;
    public static String FEATURE_SMS_TEXT="number";
    public static String FEATURE_MAIL_TEXT ="mail_text";
    public static String FEATURE_MAIL="email" ;
    public static String FEATURE_SHARE_TEXT="share_text" ;
    public static String FEATURE_TIME="time" ;
    public static String FEATURE_REQUEST_URL="request_url" ;
    public static String FEATURE_OPTIONS="options" ;
    public static String FEATURE_HEADING="heading" ;
    public static String FEATURE_DATE="date";
    public static String FEATURE_MISC="misc";
    public static String FEATURE_EXTRA="extra" ;

    // advertisement_scheduling data
    public static String SCHEDULE_TABLE="schedule";
    public static String SCHEDULE_ID="schedule_id";
    public static String SCHEDULE_GENDER= "gender";
    public static String SCHEDULE_STATUS="status";
    public static String SCHEDULE_START_DATE="start_date";
    public static String SCHEDULE_START_TIME="start_time";
    public static String SCHEDULE_END_DATE="end_date";
    public static String SCHEDULE_END_TIME="end_time";
    public static String SCHEDULE_CHANNEL_ID="channel_id";
    public static String SCHEDULE_CODE="code";
    public static String SCHEDULE_IS_REPEAT="is_repeat";
    public static String SCHEDULE_REPEAT_TIME="repeat_time";
    public static String SCHEDULE_REPEAT_TYPE="repeat_type";
    public static String SCHEDULE_IS_REMINDER="is_reminder";
    public static String SCHEDULE_REMINDER_TEXT="reminder_text";
    public static String SCHEDULE_REMINDER_TIME="reminder_time";
    public static String SCHEDULE_COUNTRY_ID="country_id";
    public static String SCHEDULE_STATE_ID="state_id";
    public static String SCHEDULE_CITY_ID="city_id";
    public static String SCHEDULE_INCOME_ID="income_id";


    public static String AD_SCHEDULE_TABLE = "advertisement_scheduling"; // table name
    public static String AD_SCHEDULE_ADD_ID = "advertisement_id"; //1
    public static String AD_SCHEDULE_CHANNEL_ID = "channelId"; //2
    public static String AD_SCHEDULE_CHANNEL_NAME = "channel_name"; //3
    public static String AD_SCHEDULE_CHANNEL_IMAGE_URL = "channel_image_url"; //4
    public static String AD_SCHEDULE_START_DATE = "start_date"; //5
    public static String AD_SCHEDULE_START_TIME = "start_time"; //6
    public static String AD_SCHEDULE_END_DATE = "end_date"; //7
    public static String AD_SCHEDULE_END_TIME = "end_time"; //8
    public static String AD_SCHEDULE_EARN_MAX = "earn_max"; //9
    public static String AD_SCHEDULE_EARN_MIN = "earn_min"; //10
    public static String AD_SCHEDULE_CODE = "code"; //11
    public static String AD_SCHEDULE_REPEAT = "repeat"; //12
    public static String AD_SCHEDULE_REPEAT_PERIOD = "repeatOptions_period"; //13
    public static String AD_SCHEDULE_REPEAT_AMOUNT = "repeatOptions_amount"; //14
    public static String AD_SCHEDULE_REMINDER = "reminder"; //15
    public static String AD_SCHEDULE_REMINDER_TEXT = "reminderOpitons_text"; //16
    public static String AD_SCHEDULE_REMINDER_TIME = "reminderOpitons_time"; //17
    public static String AD_SCHEDULE_LOCATION_STATE = "location_state"; //18
    public static String AD_SCHEDULE_LOCATION_CITY = "location_city"; //19
    public static String AD_SCHEDULE_INCOME = "income"; //20
    public static String AD_SCHEDULE_GENDER = "gender"; //21

    // transaction earning api
    public static String EARNING_TABLE = "earnings";
    public static String EARNING_TYPE="type";
    public static String EARNING_EARNING_ID="earning_id";
    public static String EARNING_TRACK_ID="track_id";
    public static String EARNING_DISPLAY_TEXT="display_text";
    public static String EARNING_VALUE_TO_EARN ="value_to_earn";
    public static String EARNING_SHOW_IN_APP="show_in_app";
    public static String EARNING_AGGREGATE_TO_EARN="aggregate_to_earn";
    public static String EARNING_MIN_AGGREGATE_VALUE="min_aggregate_value";
    public static String EARNING_RESET_AGGREGATE_COUNT="reset_aggregate_count";
    public static String EARNING_EARN_BONUS="earn_bonus";
    public static String EARNING_TOTAL_AGGREGATE_COUNT="total_aggregate_count";
    public static String EARNING_TOTAL_BONUS= "total_bonus";


    //Channels

    public static String CHANNEL_TABLE="channel";
    public static String CHANNEL_ID="channel_id";
    public static String CHANNEL_NAME="name";
    public static String CHANNEL_IMAGE="image";
    public static String CHANNEL_IMAGE_NAME="image_name";
    public static String CHANNEL_MISC="misc";
    public static String CHANNEL_EXTRA="extra";

    // transaction data
    public static String TRANSACTION_TABLE = "transactions"; // transaction table name
    public static String TRANSACTION_TRACK_ID = "track_id"; //1
    public static String TRANSACTION_SCHEDULE_ID= "schedule_id"; //2
    public static String TRANSACTION_EARNED = "uploaded"; //3
    public static String TRANSACTION_UPLOADED= "times"; //4
    public static String TRANSACTION_MISC = "misc"; //5
    public static String TRANSACTION_MISC2 = "misc2"; // 6
    public static String TRANSACTION_EXTRA = "extra"; // 6

    // balance_amount data
    public static String BALANCE_TABLE = "balance_amount"; // table name
    public static String BALANCE_AMOUNT = "amount"; //1

    // live event
    public static String LIVE_EVENT_TABLE = "for_live_event"; // table name
    public static String LIVE_EVENT_ADVERT_ID = "advert_id"; //1
    public static String LIVE_EVENT_TRACK_ID = "track_id"; //2
    public static String LIVE_EVENT_PROCESSED = "processed"; //3
    public static String LIVE_EVENT_MISC = "misc"; //4

    public static boolean haveNetworkConnection(Context context) {
        try {
            if (context != null) {
                ConnectivityManager connectivityManager =
                        (ConnectivityManager) context
                                .getSystemService(CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                return networkInfo != null && networkInfo.isConnected();
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    // shared methods
     static void setShared(Context context, String name, String value) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(name, value);
        editor.commit();
    }

     static String getShared(Context context, String name,
                                   String defaultValue) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        return prefs.getString(name, defaultValue);

    }

    public static void LevelTwoTask(Activity mActivity) {

        /** Following things to be done here
         * 1. Start Location update (background process)
         * 2. Start Download and save ad process (background process)
         * 3. Start send transaction process (background process)
         * 4. Start watchdog process (background process)
         * 5. Start MainScreen  (main process)
          */

//        boolean isLocAlive = Utils.isMyServiceRunning(LocationUpdater.class, mActivity);
//
//        try {
//                if (!isLocAlive)
//                    mActivity.startService(new Intent(mActivity, LocationUpdater.class));
//
//        } catch (Exception e) {
//            // TODO: handle exception
//            e.printStackTrace();
//        }

        boolean isDASA_Alive = Utils.isMyServiceRunning(DownloadAndSaveAds.class, mActivity);

        try {
            if (!isDASA_Alive)
                mActivity.startService(new Intent(mActivity, DownloadAndSaveAds.class));

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }


        // do it afterward
//        boolean isST_Alive = Utils.isMyServiceRunning(SendTransaction.class, mActivity);
//
//        try {
//            if (!isST_Alive)
//                mActivity.startService(new Intent(mActivity, SendTransaction.class));
//
//        } catch (Exception e) {
//            // TODO: handle exception
//            e.printStackTrace();
//        }

//        mActivity.startService(new Intent(mActivity,LocationUpdater.class));

//        mActivity.startService(new Intent(mActivity,DownloadAndSaveAds.class));

//        mActivity.startService(new Intent(mActivity,SendTransaction.class));

//        mActivity.startService(new Intent(mActivity,WatchDog.class));
        mActivity.startActivity(new Intent(mActivity, HomeActivity.class));
        mActivity.finish();
    }

    // show error dialog on splash
    public static void showErrorDialog(Context mContext,String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }

    public static String makeFolder(String foldername) {

        String path = Environment.getExternalStorageDirectory()
                + File.separator + foldername;
        File folder = new File(path);
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        return path;
    }

    public static String makeRootFolder() {

        String path = Environment.getExternalStorageDirectory()
                + File.separator + Utils.root_folder;
        File folder = new File(path);
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }

        return path;
    }
    // check for user is already registered
    public static boolean isRegisteredUser(Context mContext) {
        if(Session.getSession(mContext).getUser_id().equals(""))
            return false;
        else
            return true;
    }

    // check otp done or not
    public static boolean isOTPRecieved(Context mContext) {
        if(Session.getSession(mContext).getOTP().equals(Utils.TRUE))
            return true;
        else
            return false;
    }

    // check to show video or not
    public static boolean shouldShowVideo(Context mContext) {
        if(Session.getSession(mContext).getShow_video().equals(""))
            return true;
        else
            return false;
    }
    // check that token exists and is valid ?
    public static boolean isTokenValid(Context mContext,DatabaseHelper db) {
        //**TODO to be done later**//
        TokenModel model = db.getToken();
        if(model!=null){
            if(!model.getToken_id().equalsIgnoreCase("")){
//                return true;
            }
        }
        return true;
    }
    // get emie id of the phone
    public static String getEMIE_ID(Context context) throws Exception {

        TelephonyManager telephonyManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    // is valid email ?
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    // delete a folder
   public static void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    public static void sendToVideoActivity(Context context,String from){
        Intent intent = new Intent(context, VideoPlay.class);
        intent.putExtra(FROM,from);
        context.startActivity(intent);
    }
    public static void sentToOtp(Context context,String from){
        Intent intent = new Intent(context, in.grasshoppermedia.klapp.activities.OTP.class);
        intent.putExtra(FROM,from);
        context.startActivity(intent);
    }

    @SuppressWarnings("deprecation")
    public static int getScreenHeight(Context context) {
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        Display d = wm.getDefaultDisplay();

        return (int) d.getHeight();
    }

    private void downloadToken(final Context mContext,final Activity mActivity,final DatabaseHelper db)
    {
        // get file name from url
        String url =BASE_URL+TOKEN_API+"?"+TOKEN_PARAM_CLIENT_ID+"="+CLIENT_ID +"&"+TOKEN_PARAM_APP_ID+"="+APP_ID;
        Log.d("ankit",url);
        CallService service = new CallService(mContext, url, GET,"",true, new CallService.OnServicecall() {
            @Override
            public void onServicecall(String response) {
                try {
                    JSONObject tokenJson = new JSONObject(response);
                    String errorMessage = tokenJson.getString("error");
                    if(errorMessage.equalsIgnoreCase("false")) {
                        String status_code = tokenJson.getString("status");
                        if(status_code.equalsIgnoreCase("200")) {
                            JSONObject tokenData = tokenJson.getJSONObject("response");
                            final TokenModel model = new TokenModel();
                            if(tokenData.has("token_name"))
                                model.setToken_name(tokenData.getString("token_name"));
                            if(tokenData.has("token_url"))
                                model.setToken_url(tokenData.getString("token_url"));
                            if(tokenData.has("token_expire_date"))
                                model.setToken_expire_date(tokenData.getString("token_expire_date"));
                            if(tokenData.has("token_start_date"))
                                model.setToken_start_date(tokenData.getString("token_start_date"));
                            if(tokenData.has("version"))
                                model.setVersion(tokenData.getString("version"));

//                            Date date = new Date(location.getTime());
//                            DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());

                            if(model.getToken_name().equalsIgnoreCase("") && model.getToken_url().equalsIgnoreCase("")) {
                                Utils.showErrorDialog(mContext, "Error downloading the file");
                            } else {
                                new DownLoadFile(model.getToken_url(), model.getToken_name(), Utils.makeFolder("Klapptoken") + "/", new OnFileDownload() {
                                    @Override
                                    public void onFileDownloadSuccess(String name, String path) {
                                        db.saveToken(model);
                                        LevelTwoTask(mActivity);
                                    }

                                    @Override
                                    public void onFileAlreadyExists(String name, String path) {

                                    }

                                    @Override
                                    public void onFileDownloadProgress(int progress) {

                                    }

                                    @Override
                                    public void onFileDownloadError(String errorMessage) {
                                        checkNetandDownloadToken(mContext,mActivity,db);
                                    }
                                });
                            }
                        } else {
                            Utils.showErrorDialog(mContext, "Error downloading the file");
                        }
                    } else
                    {
                        Utils.showErrorDialog(mContext, "Error downloading the file");
                    }
                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
        });


        if (Build.VERSION.SDK_INT >= 11) {
            // --post GB use serial executor by default --
            service.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            // --GB uses ThreadPoolExecutor by default--
            service.execute();
        }
    }

    public void checkNetandDownloadToken(final Context mContext,final Activity activity,final DatabaseHelper db){
        if(Utils.isTokenValid(mContext, db)) {
            // close splash here and start level 2 task
            // ** TODO here lies the next part ** //
            Utils.LevelTwoTask(activity);
        } else {
            // check for internet connection
            if(haveNetworkConnection(mContext)) {
                // download the token
                downloadToken(mContext,activity,db);
            }else {
                // show some error and be on this page only
                Utils.showErrorDialog(mContext, Utils.InternetError);
                new CountDownTimer(120000,1000){
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        checkNetandDownloadToken(mContext,activity,db);
                    }
                }.start();

            }
        }

    }

    public void getDeviceTokenAndId(Activity activity,Context mContext,Session session) {
        if (device_token.equalsIgnoreCase(""))
            new GetRegistrationId(activity,mContext,session).execute();
        else {
            // send gcm key and emei to my server
            synchronized (this) {
                try {
                    emei_id = "" + Utils.getEMIE_ID(mContext);

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            session.setDevice_id(emei_id);
            session.setDevice_token(device_token);
            KlapPrefs.putString(mContext,Utils.IS_PERMISSION_ALLOWED,"N");
            sentToOtp(mContext, "c");
            activity.finish();
        }
    }

    private class GetRegistrationId extends AsyncTask<Void, Void, Void> {

        Activity activity;
        Context context;
        Session session;
        ProgressDialog pd;

        public GetRegistrationId(Activity activity,Context context,Session session){
            this.activity = activity;
            this.context = context;
            this.session = session;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pd = ProgressDialog.show(context,"Loading..","Please wait");
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub

            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(context);
                }
                device_token = gcm.register(Utils.GCM_KEY);

            } catch (IOException ex) {
                ex.printStackTrace();
                device_token = "";

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            pd.dismiss();
            getDeviceTokenAndId(activity,context,session);
        }

    }

//    public static synchronized void sendTransactionToServer(Context context,EarnModel earnModel) throws Exception {
//        JSONObject jobj = new JSONObject();
//        jobj.put("user_id", earnModel.getUser_id());
//        jobj.put("latitude", earnModel.getLatitude());
//        jobj.put("longitude", earnModel.getLongitude());
//        jobj.put("advertise_id", earnModel.getAdvertise_id());
//        jobj.put("action_view", earnModel.getAction_view());
//        jobj.put("earn", earnModel.getEarn());
//        jobj.put("track_id", earnModel.getTrack_id());
//        String jsonData = jobj.toString();
//        CallService service = new CallService(context, Utils.BASE_URL + Utils.EARNING_API, Utils.PUT, jsonData, false,
//                new CallService.OnServicecall() {
//                    @Override
//                    public void onServicecall(String response) {
//                        Log.d("ankit",":::"+response);
//                    }
//                });
//        if (Build.VERSION.SDK_INT >= 11) {
//            // --post GB use serial executor by default --
//            service.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//        } else {
//            // --GB uses ThreadPoolExecutor by default--
//            service.execute();
//        }
//    }

    public static void setServiceWatchdogTimer(Context context, boolean set,
                                               int timeout) {
        Intent intent;
        PendingIntent alarmIntent;
        intent = new Intent(); // forms and creates appropriate Intent and pass
        // it to AlarmManager
        intent.setAction("in.grasshoppermedia.klapp.RestartService");
        intent.setClass(context, RestartService.class);
        alarmIntent = PendingIntent.getBroadcast(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);
        if (set)
            am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                    + timeout, alarmIntent);
        else
            am.cancel(alarmIntent);
    }

    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {

                return true;
            }
        }

        return false;
    }
//    public static void addfragment(FragmentActivity fActivity,Fragment fragment, int id,String tag) {
//
//        // id = R.id.content_frame
//
//        try {
//            FragmentManager fragmentManager = fActivity.getSupportFragmentManager();
//            Fragment frag = fragmentManager.findFragmentByTag(tag);
//
//            if (frag == null) {
//
//                FragmentTransaction transaction = fragmentManager
//                        .beginTransaction();
//                transaction.add(id, fragment, tag)
//                        .addToBackStack(null).commit();
//
//                fragmentManager.executePendingTransactions();
//            }
//        } catch (Exception e) {
//            // TODO: handle exception
//            e.printStackTrace();
//        }
//
//    }
    public ProfileModel parseProfiledata(String userdata) throws Exception{
        ProfileModel model = new ProfileModel();
        JSONObject jobj = new JSONObject(userdata);
        model.setUser_id(jobj.getString("_id"));
        model.setName(jobj.getString("name"));
        model.setEmail(jobj.getString("email"));
        model.setGender(jobj.getString("gender"));
        model.setIncome_value(jobj.getString("income"));
        JSONObject countryJson = jobj.getJSONObject("country");
        model.setCountry(countryJson.getString("name"));
        model.setState(countryJson.getString("state"));
        model.setCity(countryJson.getString("city"));
        model.setAddress1(countryJson.getString("address1"));
        model.setAddress2(countryJson.getString("address2"));
        return  model;
    }
}
