package in.grasshoppermedia.klapp.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by rbpatel on 4/25/2017.
 */

public class KlapPrefs {
    private static SharedPreferences sharedPreferences=null;
    private static final String KLAP_SECURITY_PREFS="Klap_SECURITY";


    public static SharedPreferences getInstance(Context context){
        if(sharedPreferences==null){
            sharedPreferences= context.getSharedPreferences(KLAP_SECURITY_PREFS, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }


    public static String getString(Context context, String key){
        getInstance(context);
        return sharedPreferences.getString(key,"");
    }

    public static void putString(Context context, String key, String value){
        getInstance(context);
        sharedPreferences.edit().putString(key,value).commit();
    }


    public static boolean getBoolean(Context context, String key){
        getInstance(context);
        return sharedPreferences.getBoolean(key,false);
    }

    public static void putBoolean(Context context, String key, boolean value){
        getInstance(context);
        sharedPreferences.edit().putBoolean(key,value).commit();
    }


    public static void clear(Context context) {
        getInstance(context);
        sharedPreferences.edit().clear().commit();
    }
}
