package in.grasshoppermedia.klapp.Utils;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import in.grasshoppermedia.klapp.model.AutoregistrationModel;
import in.grasshoppermedia.klapp.services.CallService;

/**
 * Created by katrina on 26/05/16.
 */
public class AutoRegistration implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private String latitude = "";
    private String longitude = "";

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;
    Context context;
    Session session;
    AutoregistrationModel model;

    public AutoRegistration(Context context,String number){
        this.context = context;

        session = new Session(context);

        model = new AutoregistrationModel();
        model.setDevice_id(session.getDevice_id());
        model.setDevice_token(session.getDevice_token());
        model.setClient_id(Utils.CLIENT_ID);
        model.setApp_id(Utils.APP_ID);
        model.setMobile(number);
        model.setPlatform(Utils.PLATEFORM);

        buildGoogleApiClient();
        mGoogleApiClient.connect();
    }



    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {

            latitude = ""+String.valueOf(mLastLocation.getLatitude());
            longitude = ""+String.valueOf(mLastLocation.getLongitude());

        }
        model.setLatitude(latitude);
        model.setLongitude(longitude);

        try{
            doAutoRegistration();
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

        model.setLatitude("");
        model.setLongitude("");

        try{
            doAutoRegistration();
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        model.setLatitude("");
        model.setLongitude("");

        try{
            doAutoRegistration();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private void doAutoRegistration() throws  Exception{

        String urlStr =  Utils.BASE_URL + Utils.AUTO_REGISTER;
        JSONObject jobj = new JSONObject();
        jobj.put("device_id",""+model.getDevice_id());
        jobj.put("device_token",""+model.getDevice_token());
        jobj.put("client_id",""+model.getClient_id());
        jobj.put("app_id",""+model.getApp_id());
        jobj.put("latitude",""+model.getLatitude());
        jobj.put("longitude", "" + model.getLongitude());
        jobj.put("mobile", "" + model.getMobile());
        jobj.put("platform", "" + model.getPlatform());

        String jsonData = jobj.toString();

        CallService service = new CallService(context, urlStr, Utils.PUT, jsonData, true, new CallService.OnServicecall() {
            @Override
            public void onServicecall(String response) {

                Log.d("ankit",""+response);
                try {
                    JSONObject jobj = new JSONObject(response);
                    String errorMessage = jobj.getString("error");
                    if(errorMessage.equalsIgnoreCase("false")) {
                        String status_code = jobj.getString("status");
                        if (status_code.equalsIgnoreCase("200")) {
                            JSONObject resJson = jobj.getJSONObject("response");
                            JSONObject user_id = null;

                            try{
                                user_id = resJson.getJSONObject("user_id");
                            } catch (Exception ex){
                                ex.printStackTrace();
                            }

                            if(user_id==null){
                                // not a json
                                session.setUser_id(resJson.getString("user_id"));
                            } else {
                                // is a json
                                session.setUser_id(user_id.getString("_id"));
                                session.setUserData(user_id.toString());
                            }
                            session.setPhoneNumber(model.getMobile());
//                            ((OTP)context).OTP_Done();
                            // send event
                            EventBus.getDefault().post(new AutoregistrationModel());

                        } else {
                            Utils.showErrorDialog(context,"OTP not successfull");
                        }
                    } else {
                        Utils.showErrorDialog(context,"OTP not successfull");
                    }
                }catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });

        if (Build.VERSION.SDK_INT >= 11) {
            // --post GB use serial executor by default --
            service.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            // --GB uses ThreadPoolExecutor by default--
            service.execute();
        }
    }
}
