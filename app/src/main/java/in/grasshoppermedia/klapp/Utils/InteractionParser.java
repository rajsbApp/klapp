package in.grasshoppermedia.klapp.Utils;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import in.grasshoppermedia.klapp.model.AdModel;
import in.grasshoppermedia.klapp.model.ChanelModel;
import in.grasshoppermedia.klapp.model.EarnModel;
import in.grasshoppermedia.klapp.model.FeaturesModel;
import in.grasshoppermedia.klapp.model.ScheduleModel;

import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_AGGREGATE_TO_EARN;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_DISPLAY_TEXT;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_EARNING_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_EARN_BONUS;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_MIN_AGGREGATE_VALUE;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_RESET_AGGREGATE_COUNT;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_SHOW_IN_APP;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_TOTAL_AGGREGATE_COUNT;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_TOTAL_BONUS;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_TRACK_ID;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_TYPE;
import static in.grasshoppermedia.klapp.Utils.Utils.EARNING_VALUE_TO_EARN;

/**
 * Created by katrina on 02/02/17.
 */

public class InteractionParser {

    String response = "";
    ArrayList<AdModel> adModels;

    public InteractionParser(String response) {
        this.response = response;
        adModels = new ArrayList<>();
    }

    public ArrayList<AdModel> JSON_PARSER() throws Exception {
        if(TextUtils.isEmpty(response)){
            return null ;
        }
        JSONObject jobj = new JSONObject(response);
        String error = jobj.optString("error");
        if (error.equalsIgnoreCase("false")) {

            JSONArray jarr = jobj.getJSONArray("data");
            for (int i = 0; i < jarr.length(); i++) {
                AdModel adModel = new AdModel();
                JSONObject inner_job = jarr.getJSONObject(i);

                // model objects
                String interaction_id = inner_job.optString("interaction_id");
                adModel.setInteraction_id(interaction_id);

                String application_id = inner_job.optString("application_id");
                adModel.setApplication_id(application_id);

                String is_preload = inner_job.optString("is_preload");
                adModel.setIs_preload(is_preload);

                String heading = inner_job.optString("heading");
                adModel.setHeading(heading);

                String file = inner_job.optString("file");
                adModel.setFile(file);

                String file_name = inner_job.optString("file_name");
                adModel.setFile_name(file_name);

                String image = inner_job.optString("image");
                adModel.setImage(image);

                String image_name = inner_job.optString("image_name");
                adModel.setImage_name(image_name);

                String type = inner_job.optString("type");
                adModel.setType(type);

                String description_heading = inner_job.optString("description_heading");
                adModel.setDescription_heading(description_heading);

                String description_image = inner_job.optString("description_image");
                adModel.setDescription_image(description_image);

                String description_image_name = inner_job.optString("description_image_name");
                adModel.setDescription_image_name(description_image_name);

                String description = inner_job.optString("description");
                adModel.setDescription(description);

                String url = inner_job.optString("url");
                adModel.setUrl(url);

                String version = inner_job.optString("version");
                adModel.setVersion(version);

                String created_at = inner_job.optString("created_at");
                adModel.setCreated_at(created_at);

                JSONArray features = inner_job.getJSONArray("features");
                ArrayList<FeaturesModel> featureModels = new ArrayList<>();
                for (int j = 0; j < features.length(); j++) {
                    JSONObject jsonObject = features.getJSONObject(j);
                    FeaturesModel model = new FeaturesModel();

                    if (jsonObject.has(Utils.FEATURE_TRACK_ID))
                        model.setTrack_id(jsonObject.optString(Utils.FEATURE_TRACK_ID));
                    if (jsonObject.has(Utils.INT_INTERACTION_ID))
                        model.setInteraction_id(jsonObject.optString(Utils.INT_INTERACTION_ID));
                    if (jsonObject.has(Utils.FEATURE_DATE))
                        model.setDate(jsonObject.optString(Utils.FEATURE_DATE));
                    if (jsonObject.has(Utils.FEATURE_MAIL))
                        model.setEmail(jsonObject.optString(Utils.FEATURE_MAIL));
                    if (jsonObject.has(Utils.FEATURE_MAIL_TEXT))
                        model.setMail_text(jsonObject.optString(Utils.FEATURE_MAIL_TEXT));
                    if (jsonObject.has(Utils.FEATURE_EXTRA))
                        model.setExtra(jsonObject.optString(Utils.FEATURE_EXTRA));
                    if (jsonObject.has(Utils.FEATURE_NAME))
                        model.setFeature_name(jsonObject.optString(Utils.FEATURE_NAME));
                    if (jsonObject.has(Utils.FEATURETYPE_ID))
                        model.setFeaturetype_id(jsonObject.optString(Utils.FEATURETYPE_ID));
                    if (jsonObject.has(Utils.FEATURE_HEADING))
                        model.setHeading(jsonObject.optString(Utils.FEATURE_HEADING));
                    if (jsonObject.has(Utils.FEATURE_OPTIONS))
                        model.setOptions(jsonObject.optString(Utils.FEATURE_OPTIONS));
                    if (jsonObject.has(Utils.FEATURE_REQUEST_URL))
                        model.setRequest_url(jsonObject.optString(Utils.FEATURE_REQUEST_URL));
                    if (jsonObject.has(Utils.FEATURE_TIME))
                        model.setTime(jsonObject.optString(Utils.FEATURE_TIME));
                    if (jsonObject.has(Utils.FEATURE_SMS_TEXT))
                        model.setSms_text(jsonObject.optString(Utils.FEATURE_SMS_TEXT));
                    if (jsonObject.has(Utils.FEATURE_MISC))
                        model.setMisc(jsonObject.optString(Utils.FEATURE_MISC));
                    if (jsonObject.has(Utils.FEATURE_SHARE_TEXT))
                        model.setShare_text(jsonObject.optString(Utils.FEATURE_SHARE_TEXT));
                    if (jsonObject.has(Utils.FEATURE_NUMBER))
                        model.setNumber(jsonObject.optString(Utils.FEATURE_NUMBER));

                    featureModels.add(model);

                }
                adModel.setFeaturesModels(featureModels);

                JSONArray schedulesArray = inner_job.getJSONArray("schedules");
                ArrayList<ScheduleModel> scheduleModels = new ArrayList<>();
                for (int k = 0; k < schedulesArray.length(); k++) {
                    ScheduleModel model = new ScheduleModel();
                    JSONObject scheduleJson = schedulesArray.getJSONObject(k);

                    String schedule_id = scheduleJson.optString("schedule_id");
                    model.setSchedule_id(schedule_id);

                    String gender = scheduleJson.optString("gender");
                    model.setGender(gender);

                    String status = scheduleJson.optString("status");
                    model.setStatus(status);

                    String start_date = scheduleJson.optString("start_date");
                    model.setStart_date(start_date);

                    String start_time = scheduleJson.optString("start_time");
                    model.setStart_time(start_time);

                    String end_date = scheduleJson.optString("end_date");
                    model.setEnd_date(end_date);

                    String end_time = scheduleJson.optString("end_time");
                    model.setEnd_time(end_time);

                    String channel_id = scheduleJson.optString("channel_id");
                    model.setChannel_id(channel_id);

                    String code = scheduleJson.optString("code");
                    model.setCode(code);

                    String is_repeat = scheduleJson.optString("is_repeat");
                    model.setIs_repeat(is_repeat);

                    String repeat_time = scheduleJson.optString("repeat_time");
                    model.setRepeat_time(repeat_time);

                    String repeat_type = scheduleJson.optString("repeat_type");
                    model.setRepeat_type(repeat_type);

                    String is_reminder = scheduleJson.optString("is_reminder");
                    model.setIs_reminder(is_reminder);

                    String reminder_text = scheduleJson.optString("reminder_text");
                    model.setReminder_text(reminder_text);

                    String reminder_time = scheduleJson.optString("reminder_time");
                    model.setReminder_time(reminder_time);

                    String country_id = scheduleJson.optString("country_id");
                    model.setCountry_id(country_id);

                    String state_id = scheduleJson.optString("state_id");
                    model.setState_id(state_id);

                    String city_id = scheduleJson.optString("city_id");
                    model.setCity_id(city_id);

                    String income_id = scheduleJson.optString("income_id");
                    model.setIncome_id(income_id);

                    JSONArray earningsJarr = scheduleJson.getJSONArray("earnings");
                    ArrayList<EarnModel> earnModels = new ArrayList<>();
                    for (int m = 0; m < earningsJarr.length(); m++) {
                        JSONObject obj=earningsJarr.getJSONObject(m);
                        EarnModel earnModel = new EarnModel();

                        earnModel.setEarning_id(obj.optString(EARNING_EARNING_ID));
                        earnModel.setType(obj.optString(EARNING_TYPE));
                        earnModel.setTrack_id(obj.optString(EARNING_TRACK_ID));
                        earnModel.setDisplay_text(obj.optString(EARNING_DISPLAY_TEXT));
                        earnModel.setValue_to_earn(obj.optString(EARNING_VALUE_TO_EARN));
                        earnModel.setShow_in_app(obj.optString(EARNING_SHOW_IN_APP));
                        earnModel.setAggregate_to_earn(obj.optString(EARNING_AGGREGATE_TO_EARN));
                        earnModel.setMin_aggregate_value(obj.optString(EARNING_MIN_AGGREGATE_VALUE));
                        earnModel.setReset_aggregate_count(obj.optString(EARNING_RESET_AGGREGATE_COUNT));
                        earnModel.setEarn_bonus(obj.optString(EARNING_EARN_BONUS));
                        earnModel.setTotal_bonus(obj.optString(EARNING_TOTAL_AGGREGATE_COUNT));
                        earnModel.setEarn_bonus(obj.optString(EARNING_TOTAL_BONUS));
                        earnModels.add(earnModel);
                    }

                    model.setEarnModels(earnModels);

                    JSONArray channelJar = scheduleJson.getJSONArray("channel");
                    ArrayList<ChanelModel> chanelModels = new ArrayList<>();
                    for (int n = 0; n < channelJar.length(); n++) {
                        JSONObject channelJob = channelJar.getJSONObject(n);
                        ChanelModel chanelModel = new ChanelModel();

                        String _id = channelJob.optString("channel_id");
                        chanelModel.setChannel_id(_id);

                        String name = channelJob.optString("name");
                        chanelModel.setName(name);

                        String chanel_image = channelJob.optString("image");
                        chanelModel.setImage(chanel_image);

                        String chanel_image_name = channelJob.optString("image_name");
                        chanelModel.setImage_name(chanel_image_name);

                        chanelModels.add(chanelModel);
                    }
                    model.setChannelModels(chanelModels);

                    scheduleModels.add(model);
                }

                adModel.setScheduleModels(scheduleModels);
                adModels.add(adModel);
            }

            return adModels;
        } else
            return null;
    }

}
