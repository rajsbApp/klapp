var classintrasonics_1_1_i_buffered_audio_processor =
[
    [ "~IBufferedAudioProcessor", "classintrasonics_1_1_i_buffered_audio_processor.html#a1c86c0ec119f207125025e886a2d4d42", null ],
    [ "Configure", "classintrasonics_1_1_i_buffered_audio_processor.html#abe27b0e06e4c5842fcb74e2106e4942b", null ],
    [ "GetVersion", "classintrasonics_1_1_i_buffered_audio_processor.html#afb5237c5ad273995fd6bb276b6202915", null ],
    [ "Initialise", "classintrasonics_1_1_i_buffered_audio_processor.html#a0e1259d63a4c624e49dcf04dd820e414", null ],
    [ "ProcessBuffer", "classintrasonics_1_1_i_buffered_audio_processor.html#a8ac2c912d1f371bacc39875c63ad60b2", null ],
    [ "Start", "classintrasonics_1_1_i_buffered_audio_processor.html#a5b74be60c086dec5c6063cc5b2e68df5", null ],
    [ "Stop", "classintrasonics_1_1_i_buffered_audio_processor.html#af269df9bb607f15d14b1e8d2717f62ef", null ]
];